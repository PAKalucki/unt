package kalucki.przemyslaw.unt.Adapter;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import kalucki.przemyslaw.unt.Adapter.ListAdapter.ListAdapter;
import kalucki.przemyslaw.unt.Adapter.ListAdapter.ListItem;

import static junit.framework.Assert.assertEquals;

/**
 * Created by prka on 23.10.17.
 */

public class ListAdapterTest
{
	ListAdapter adapter;

	@Before
	public void setUp()
	{
		List<ListItem> list = new ArrayList<>();

		for(int i = 0; i < 20; i++)
		{
			list.add(new ListItem(null,"test123"));
		}

		adapter = new ListAdapter(list);
	}

	@Test
	public void fillColumn0_withIdealAmountOfValues_checkValuesSet()
	{
		List<ListItem> newValues = new ArrayList<>();

		for(int i = 0; i < 11; i++)
		{
			newValues.add(new ListItem(null, "new"));
		}

		adapter.fillColumn(0, 2, newValues);

		List<ListItem> result = adapter.getListItems();

		assertEquals("new", result.get(0).text);
		assertEquals("test123", result.get(1).text);
		assertEquals("new", result.get(2).text);
		assertEquals("test123", result.get(3).text);
		assertEquals("new", result.get(4).text);
		assertEquals("test123", result.get(5).text);
		assertEquals("new", result.get(6).text);
		assertEquals("test123", result.get(7).text);
		assertEquals("new", result.get(8).text);
		assertEquals("test123", result.get(9).text);
		assertEquals("new", result.get(10).text);
		assertEquals("test123", result.get(11).text);
		assertEquals("new", result.get(12).text);
		assertEquals("test123", result.get(13).text);
		assertEquals("new", result.get(14).text);
		assertEquals("test123", result.get(15).text);
		assertEquals("new", result.get(16).text);
		assertEquals("test123", result.get(17).text);
		assertEquals("new", result.get(18).text);
		assertEquals("test123", result.get(19).text);
	}

	@Test
	public void fillColumn0_withLessValues_checkValuesSet()
	{
		List<ListItem> newValues = new ArrayList<>();

		for(int i = 0; i < 8; i++)
		{
			newValues.add(new ListItem(null,"new"));
		}

		adapter.fillColumn(0, 2, newValues);

		List<ListItem> result = adapter.getListItems();

		assertEquals("new", result.get(0).text);
		assertEquals("test123", result.get(1).text);
		assertEquals("new", result.get(2).text);
		assertEquals("test123", result.get(3).text);
		assertEquals("new", result.get(4).text);
		assertEquals("test123", result.get(5).text);
		assertEquals("new", result.get(6).text);
		assertEquals("test123", result.get(7).text);
		assertEquals("new", result.get(8).text);
		assertEquals("test123", result.get(9).text);
		assertEquals("new", result.get(10).text);
		assertEquals("test123", result.get(11).text);
		assertEquals("new", result.get(12).text);
		assertEquals("test123", result.get(13).text);
		assertEquals("new", result.get(14).text);
		assertEquals("test123", result.get(15).text);
		assertEquals("test123", result.get(16).text);
		assertEquals("test123", result.get(17).text);
		assertEquals("test123", result.get(18).text);
		assertEquals("test123", result.get(19).text);
	}

	@Test (expected = IndexOutOfBoundsException.class)
	public void fillColumn0_withTooManyValuesValues_Throw()
	{
		List<ListItem> newValues = new ArrayList<>();

		for(int i = 0; i < 40; i++)
		{
			newValues.add(new ListItem(null, "new"));
		}

		adapter.fillColumn(0, 2, newValues);
	}
}
