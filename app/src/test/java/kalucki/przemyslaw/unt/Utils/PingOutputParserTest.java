package kalucki.przemyslaw.unt.Utils;

import org.junit.Assert;
import org.junit.Test;

/**
 * prka@mobica-R50:~/workspace/android/UNT$ ping -c 1 -t 1 www.google.com
 * PING www.google.com (172.217.20.164) 56(84) bytes of data.
 * From gateway (192.168.135.161) icmp_seq=1 Time to live exceeded
 * <p>
 * --- www.google.com ping statistics ---
 * 1 packets transmitted, 0 received, +1 errors, 100% packet loss, time 0ms
 * <p>
 * prka@mobica-R50:~/workspace/android/UNT$ ping -c 1 -t 2 www.google.com
 * PING www.google.com (172.217.20.164) 56(84) bytes of data.
 * From 10.0.66.101 icmp_seq=1 Time to live exceeded
 * <p>
 * --- www.google.com ping statistics ---
 * 1 packets transmitted, 0 received, +1 errors, 100% packet loss, time 0ms
 * <p>
 * ping -c 1 -t 3 www.google.com
 * PING www.google.com (172.217.20.164) 56(84) bytes of data.
 * From 91-224-118-65.fttx.telefonserwis.pl (91.224.118.65) icmp_seq=1 Time to live exceeded
 * <p>
 * --- www.google.com ping statistics ---
 * 1 packets transmitted, 0 received, +1 errors, 100% packet loss, time 0ms
 * <p>
 * ping -c 1 -t 4 www.google.com
 * PING www.google.com (172.217.20.164) 56(84) bytes of data.
 * From 91-224-118-241.fttx.telefonserwis.pl (91.224.118.241) icmp_seq=1 Time to live exceeded
 * <p>
 * --- www.google.com ping statistics ---
 * 1 packets transmitted, 0 received, +1 errors, 100% packet loss, time 0ms
 * <p>
 * ping -c 1 -t 5 www.google.com
 * PING www.google.com (172.217.20.164) 56(84) bytes of data.
 * From 195.149.232.241 icmp_seq=1 Time to live exceeded
 * <p>
 * --- www.google.com ping statistics ---
 * 1 packets transmitted, 0 received, +1 errors, 100% packet loss, time 0ms
 * <p>
 * ping -c 1 -t 6 www.google.com
 * PING www.google.com (172.217.20.164) 56(84) bytes of data.
 * From 72.14.197.128 icmp_seq=1 Time to live exceeded
 * <p>
 * --- www.google.com ping statistics ---
 * 1 packets transmitted, 0 received, +1 errors, 100% packet loss, time 0ms
 * <p>
 * ping -c 1 -t 7 www.google.com
 * PING www.google.com (172.217.20.164) 56(84) bytes of data.
 * From 108.170.250.193 icmp_seq=1 Time to live exceeded
 * <p>
 * --- www.google.com ping statistics ---
 * 1 packets transmitted, 0 received, +1 errors, 100% packet loss, time 0ms
 * <p>
 * ping -c 1 -t 8 www.google.com
 * PING www.google.com (172.217.20.164) 56(84) bytes of data.
 * From 216.239.41.165 icmp_seq=1 Time to live exceeded
 * <p>
 * --- www.google.com ping statistics ---
 * 1 packets transmitted, 0 received, +1 errors, 100% packet loss, time 0ms
 * <p>
 * ping -c 1 -t 9 www.google.com
 * PING www.google.com (172.217.20.164) 56(84) bytes of data.
 * 64 bytes from waw02s07-in-f4.1e100.net (172.217.20.164): icmp_seq=1 ping_ttl_editText=55 time=9.49 ms
 * <p>
 * --- www.google.com ping statistics ---
 * 1 packets transmitted, 1 received, 0% packet loss, time 0ms
 * rtt min/avg/max/mdev = 9.497/9.497/9.497/0.000 ms
 * ping -c 1 -t 10 www.google.com
 * PING www.google.com (172.217.20.164) 56(84) bytes of data.
 * 64 bytes from waw02s07-in-f4.1e100.net (172.217.20.164): icmp_seq=1 ping_ttl_editText=55 time=8.08 ms
 * <p>
 * --- www.google.com ping statistics ---
 * 1 packets transmitted, 1 received, 0% packet loss, time 0ms
 * rtt min/avg/max/mdev = 8.083/8.083/8.083/0.000 ms
 */
public class PingOutputParserTest
{
	private static final String example1 =
			" PING www.google.com (172.217.20.164) 56(84) bytes of data.\n" +
					"64 bytes from waw02s07-in-f4.1e100.net (172.217.20.164): icmp_seq=1 ping_ttl_editText=55 time=7.95 ms\n" +
					"--- www.google.com ping statistics ---\n" +
					"1 packets transmitted, 1 received, 0% packet loss, time 0ms\n" +
					"rtt min/avg/max/mdev = 7.950/7.950/7.950/0.000 ms\n";
	private static final String example2 =
			" * PING www.google.com (172.217.20.164) 56(84) bytes of data.\n" +
					" * From 91-224-118-241.fttx.telefonserwis.pl (91.224.118.241) icmp_seq=1 Time to live exceeded\n";
	private static final String invalidIp = "255.255.255.256";
	private static final String validIp1 = "0.0.0.0";
	private static final String validIp2 = "255.255.255.255";
	private static final String notIp = "asdasdasd.asdasdasdasd.asdasdasda.awfrafasfasf";
	private static final String partial = "123.123.123.";
	private static final int binaryIp1 = 25576378;

	@Test
	public void extractPingAddressIp() throws Exception
	{
		Assert.assertEquals("172.217.20.164", PingOutputParser.extractPingAddressIp(example1));
		Assert.assertEquals("172.217.20.164", PingOutputParser.extractPingAddressIp(example2));
		Assert.assertEquals("", PingOutputParser.extractPingAddressIp(invalidIp));
		Assert.assertEquals("0.0.0.0", PingOutputParser.extractPingAddressIp(validIp1));
		Assert.assertEquals("255.255.255.255", PingOutputParser.extractPingAddressIp(validIp2));
		Assert.assertEquals("", PingOutputParser.extractPingAddressIp(notIp));
		Assert.assertEquals("", PingOutputParser.extractPingAddressIp(partial));
	}

	@Test
	public void extractPingTargetAddressIp() throws Exception
	{
		Assert.assertEquals("172.217.20.164", PingOutputParser.extractPingTargetAddressIp(example1));
		Assert.assertEquals("91.224.118.241", PingOutputParser.extractPingTargetAddressIp(example2));
	}

	@Test
	public void extractPingHostname() throws Exception
	{
		Assert.assertEquals(" waw02s07-in-f4.1e100.net ", PingOutputParser.extractPingHostname(example1));
		Assert.assertEquals(" 91-224-118-241.fttx.telefonserwis.pl ", PingOutputParser.extractPingHostname(example2));
	}

	@Test
	public void extractPingStatistics() throws Exception
	{

	}
	
	@Test
	public void binaryToString() throws Exception
	{
		Assert.assertEquals("186.67.134.1", PingOutputParser.binaryToString(binaryIp1));
	}

}