package kalucki.przemyslaw.unt.Utils;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by prka on 20.11.17.
 */
public class SignalUtilTest
{
	@Test
	public void calculateSignalStrengthPercent_boundaryValuesTest() throws Exception
	{
		Assert.assertEquals(100.0, SignalUtil.calcStrenghtPercent(0), 1e-3);
		Assert.assertEquals(0.0, SignalUtil.calcStrenghtPercent(-800), 1e-3);
	}

}