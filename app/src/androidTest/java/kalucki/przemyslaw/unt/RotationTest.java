package kalucki.przemyslaw.unt;

import android.Manifest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.rule.GrantPermissionRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.matcher.ViewMatchers.isCompletelyDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isRoot;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static kalucki.przemyslaw.unt.Utils.OrientationChangeAction.orientationLandscape;
import static kalucki.przemyslaw.unt.Utils.OrientationChangeAction.orientationPortrait;
import static org.hamcrest.Matchers.allOf;

/**
 * Created by prka on 20.11.17.
 */

@RunWith (AndroidJUnit4.class)
public class RotationTest
{
	@Rule
	public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

	@Rule
	public GrantPermissionRule permissionRule = GrantPermissionRule.grant(Manifest.permission.READ_PHONE_STATE);

	@Test
	public void changeOrientation_swipeToAll_NoThrow()
	{
		for(int i = 0; i < 13; i++)//TODO get number of fragments dynamicaly to avoid chaning this test every time
		{
			wait(2000);
			onView(allOf(isRoot(), isCompletelyDisplayed())).perform(orientationLandscape());
			wait(3000);
			onView(allOf(isRoot(), isCompletelyDisplayed())).perform(orientationPortrait());
			wait(3000);
			onView(allOf(withId(R.id.pager), isCompletelyDisplayed())).perform(swipeLeft());
		}
	}

	private void wait(int value)
	{
		try
		{
			Thread.sleep(value);
		}
		catch(InterruptedException e)
		{
			e.printStackTrace();
		}
	}
}
