package kalucki.przemyslaw.unt;


import android.*;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.test.rule.ActivityTestRule;
import android.support.test.rule.GrantPermissionRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.isPlatformPopup;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isEnabled;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

@LargeTest
@RunWith (AndroidJUnit4.class)
public class PingSettingsDialogTest
{
	private static final String PREFERENCES = "kalucki.przemyslaw.unt.preference_file";

	@Rule
	public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

	@Rule
	public GrantPermissionRule permissionRule = GrantPermissionRule.grant(android.Manifest.permission.READ_PHONE_STATE);

	@Before
	public void cleanSharedPrefs()
	{
		SharedPreferences sharedPreferences = getInstrumentation().getTargetContext().getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.clear();
		editor.commit();
	}

	/*
	* open settings then set values 10 for all fields expect port in ICMP mode, press OK to save, open settings again to verify it saved values
	* */
	@Test
	public void setProperValues_ConfirmAndSave_CheckIfValuesSaved()
	{
		onView(allOf(withText("Ping"), isDisplayed())).perform(click());

		openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());

		onView(allOf(withId(R.id.title), isDisplayed())).perform(click());
		onView(allOf(withId(R.id.ping_count_editText), isDisplayed())).perform(click());

		onView(allOf(withId(R.id.ping_count_editText), isDisplayed())).perform(replaceText("10"), closeSoftKeyboard());
		onView(allOf(withId(R.id.ping_interval_editText), isDisplayed())).perform(replaceText("10"), closeSoftKeyboard());
		onView(allOf(withId(R.id.ping_packetsize_editText), isDisplayed())).perform(replaceText("10"), closeSoftKeyboard());
		onView(allOf(withId(R.id.ping_ttl_editText), isDisplayed())).perform(replaceText("10"), closeSoftKeyboard());
		onView(allOf(withId(R.id.ping_timeout_editText), isDisplayed())).perform(replaceText("10"), closeSoftKeyboard());
		onView(allOf(withId(R.id.ping_ok_button), isDisplayed())).perform(click());

		openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());

		onView(allOf(withId(R.id.title), isDisplayed())).perform(click());

		onView(allOf(withId(R.id.ping_count_editText), isDisplayed())).check(matches(withText("10")));
		onView(allOf(withId(R.id.ping_interval_editText), isDisplayed())).check(matches(withText("10")));
		onView(allOf(withId(R.id.ping_packetsize_editText), isDisplayed())).check(matches(withText("10")));
		onView(allOf(withId(R.id.ping_ttl_editText), isDisplayed())).check(matches(withText("10")));
		onView(allOf(withId(R.id.ping_timeout_editText), isDisplayed())).check(matches(withText("10")));
		onView(allOf(withId(R.id.ping_timeout_editText), isDisplayed())).check(matches(withText("10")));
	}

	/*
	* open settings then set values 10 for all fields expect port in ICMP mode, press OK to save, open settings again, press restore defaults, open and check if values are set to default
	* */
	@Test
	public void setProperValues_ConfirmAndSave_CheckIfValuesSaved_ResetToDefaultAndCheck()
	{
		onView(allOf(withText("Ping"), isDisplayed())).perform(click());

		openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());

		onView(allOf(withId(R.id.title), isDisplayed())).perform(click());
		onView(allOf(withId(R.id.ping_count_editText), isDisplayed())).perform(click());

		onView(allOf(withId(R.id.ping_count_editText), isDisplayed())).perform(replaceText("10"), closeSoftKeyboard());
		onView(allOf(withId(R.id.ping_interval_editText), isDisplayed())).perform(replaceText("10"), closeSoftKeyboard());
		onView(allOf(withId(R.id.ping_packetsize_editText), isDisplayed())).perform(replaceText("10"), closeSoftKeyboard());
		onView(allOf(withId(R.id.ping_ttl_editText), isDisplayed())).perform(replaceText("10"), closeSoftKeyboard());
		onView(allOf(withId(R.id.ping_timeout_editText), isDisplayed())).perform(replaceText("10"), closeSoftKeyboard());

		onView(allOf(withId(R.id.ping_ok_button), isDisplayed())).perform(click());

		openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());

		onView(allOf(withId(R.id.title), isDisplayed())).perform(click());

		onView(allOf(withId(R.id.ping_count_editText), isDisplayed())).check(matches(withText("10")));
		onView(allOf(withId(R.id.ping_interval_editText), isDisplayed())).check(matches(withText("10")));
		onView(allOf(withId(R.id.ping_packetsize_editText), isDisplayed())).check(matches(withText("10")));
		onView(allOf(withId(R.id.ping_ttl_editText), isDisplayed())).check(matches(withText("10")));
		onView(allOf(withId(R.id.ping_timeout_editText), isDisplayed())).check(matches(withText("10")));

		onView(allOf(withId(R.id.ping_restore_button), isDisplayed())).perform(click());

		openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());

		onView(allOf(withId(R.id.title), isDisplayed())).perform(click());

		onView(allOf(withId(R.id.ping_count_editText), isDisplayed())).check(matches(withText("0")));
		onView(allOf(withId(R.id.ping_interval_editText), isDisplayed())).check(matches(withText("1000")));
		onView(allOf(withId(R.id.ping_packetsize_editText), isDisplayed())).check(matches(withText("0")));
		onView(allOf(withId(R.id.ping_ttl_editText), isDisplayed())).check(matches(withText("0")));
		onView(allOf(withId(R.id.ping_timeout_editText), isDisplayed())).check(matches(withText("200")));
	}

	/*
	* Open settings, verify port is disabled in ICMP mode, switch to TCP mode, verify packer size and TTL fields are disabled
	* */
	@Test
	public void changeModes_VerifyIfDisablesEnables()
	{
		onView(allOf(withText("Ping"), isDisplayed())).perform(click());

		openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());

		onView(allOf(withId(R.id.title), withText("Settings"))).perform(click());

		onView(withId(R.id.ping_packetsize_editText)).check(matches(isEnabled()));
		onView(withId(R.id.ping_ttl_editText)).check(matches(isEnabled()));
		onView(withId(R.id.ping_port_editText)).check(matches(not(isEnabled())));

		onView(allOf(withId(R.id.ping_mode_spinner), isDisplayed())).perform(click());

		onData(allOf(is(instanceOf(String.class)), is("TCP"))).inRoot(isPlatformPopup()).perform(click());

		onView(withId(R.id.ping_packetsize_editText)).check(matches(not(isEnabled())));
		onView(withId(R.id.ping_ttl_editText)).check(matches(not(isEnabled())));
		onView(withId(R.id.ping_port_editText)).check(matches(isEnabled()));
	}

}
