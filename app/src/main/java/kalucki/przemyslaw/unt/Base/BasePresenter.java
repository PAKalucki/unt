package kalucki.przemyslaw.unt.Base;

/**
 * Created by prka on 15.11.17.
 */

public interface BasePresenter<T>
{
	void bind(T view);
	void unbind();
}
