package kalucki.przemyslaw.unt.Base;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import kalucki.przemyslaw.unt.Adapter.BasicAdapter.BasicAdapter;
import kalucki.przemyslaw.unt.R;

public abstract class BaseRecyclerWithButtonFragment extends BaseFragment implements BaseStandardView
{
	@BindView (R.id.base_button) ImageButton button;
	@BindView (R.id.base_recyclerView) RecyclerView recyclerView;
	private Unbinder unbinder;
	protected ArrayList<String> outputItems = new ArrayList<>();
	protected BasicAdapter adapter;
	protected boolean isStarted = false;
	private String isStartedKey;
	private String outputKey;

	@Override
	public void onCreate(Bundle bundle)
	{
		if(bundle != null)
		{
			isStarted = bundle.getBoolean(isStartedKey);
			outputItems = bundle.getStringArrayList(outputKey);
		}

		super.onCreate(bundle);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.fragment_base_recyclerview, container, false);
		unbinder = ButterKnife.bind(this, view);
		return view;
	}

	@Override
	public void onStart()
	{
		super.onStart();

		View view = getView();

		if(view != null)
		{
			recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext().getApplicationContext()));
			adapter = new BasicAdapter(outputItems);
			recyclerView.setAdapter(adapter);

			handleButtonImage();
		}
	}

	@Override
	public void onDestroyView()
	{
		super.onDestroyView();
		unbinder.unbind();
	}

	@Override
	public void onSaveInstanceState(Bundle bundle)
	{
		bundle.putBoolean(isStartedKey, isStarted);
		bundle.putStringArrayList(outputKey, outputItems);

		super.onSaveInstanceState(bundle);
	}

	@Override
	public void start()
	{
		if(adapter != null)
		{
			adapter.clearAll();
		}

		isStarted = true;
		handleButtonImage();
	}

	@Override
	public void update(String value)
	{
		outputItems.add(value);
		adapter.notifyDataSetChanged();
		recyclerView.scrollToPosition(outputItems.size() - 1);
	}

	@Override
	public void stop()
	{
		isStarted = false;
		handleButtonImage();
	}

	public void setIsStartedKey(String isStartedKey)
	{
		this.isStartedKey = isStartedKey;
	}

	public void setOutputKey(String outputKey)
	{
		this.outputKey = outputKey;
	}

	private void handleButtonImage()
	{
		if(isStarted)
		{
			button.setImageDrawable(getResources().getDrawable(R.drawable.ic_stop_black_18dp));
		}
		else
		{
			button.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_arrow_black_18dp));
		}
	}
}
