package kalucki.przemyslaw.unt.Base;

/**
 * Created by prka on 30.11.17.
 */

public interface BaseStandardView
{
	void start();
	void update(String value);
	void stop();
}
