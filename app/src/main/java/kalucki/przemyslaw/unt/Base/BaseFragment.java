package kalucki.przemyslaw.unt.Base;

import android.content.Context;
import android.support.v4.app.Fragment;

import kalucki.przemyslaw.unt.MainApplication;

/**
 * Created by prka on 16.11.17.
 */

public abstract class BaseFragment extends Fragment
{
	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

		injectDependencies(MainApplication.getApplication(context));
	}

	protected abstract void injectDependencies(MainApplication application);
}
