package kalucki.przemyslaw.unt.Base;

import android.app.DialogFragment;
import android.content.Context;

import kalucki.przemyslaw.unt.MainApplication;

/**
 * Created by prka on 16.11.17.
 */

public abstract class BaseDialogFragment extends DialogFragment
{
	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

		injectDependencies(MainApplication.getApplication(context));
	}

	protected abstract void injectDependencies(MainApplication application);
}
