package kalucki.przemyslaw.unt.Utils;

import kalucki.przemyslaw.unt.Features.Ping.PingSettings;

/**
 * Created by przemek on 01.10.16.
 */

/*Usage: ping [-aAbBdDfhLnOqrRUvV] [-c count] [-i interval] [-I interface]
        [-m mark] [-M pmtudisc_option] [-l preload] [-p pattern] [-Q tos]
        [-s packetsize] [-S sndbuf] [-t ping_ttl_editText] [-T timestamp_option]
        [-w deadline] [-W timeout] [hop1 ...] destination*/
public class PingComandFormatter
{
	private static final String countOption = "-c %d ";
	private static final String intervalOption = "-i %d ";
	private static final String packetsizeOption = "-s %d ";
	private static final String ttlOption = "-t %d ";
	private static final String timeoutOption = "-W %d ";

	public static String getCommand(String address, PingSettings settings)
	{
		String output = "ping ";
		if(settings.count != 0)
		{
			output = output.concat(String.format(countOption, settings.count));
		}
		if(settings.interval != 0)
		{
			output = output.concat(String.format(intervalOption, settings.interval));
		}
		if(settings.packetsize != 0)
		{
			output = output.concat(String.format(packetsizeOption, settings.packetsize));
		}
		if(settings.ttl != 0)
		{
			output = output.concat(String.format(ttlOption, settings.ttl));
		}
		if(settings.timeout != 0)
		{
			output = output.concat(String.format(timeoutOption, settings.timeout));
		}
		if(address != null)
		{
			output = output.concat(address);
		}

		return output;
	}

}
