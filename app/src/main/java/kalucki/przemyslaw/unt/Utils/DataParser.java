package kalucki.przemyslaw.unt.Utils;

import android.support.annotation.NonNull;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Scheduler;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by prka on 17.10.17.
 */

public class DataParser
{
	public static <T, U> Observable<U> parseDataInParallel(@NonNull List<T> data, Function<List<T>, ObservableSource<U>> worker)
	{
		int threadCount = Runtime.getRuntime().availableProcessors();
		ExecutorService threadPoolExecutor = Executors.newFixedThreadPool(threadCount);
		Scheduler scheduler = Schedulers.from(threadPoolExecutor);

		AtomicInteger groupIndex = new AtomicInteger();

		return Observable.fromIterable(data).groupBy(k->groupIndex.getAndIncrement() % threadCount)
		                 .flatMap(group->group.observeOn(scheduler).toList().flatMapObservable(worker));
	}
}
