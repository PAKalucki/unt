package kalucki.przemyslaw.unt.Utils;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by prka on 06.10.17.
 * EXAMPLE PING OUTPUT:
 * <p>
 * PING www.google.com (172.217.20.164) 56(84) bytes of data.
 * 64 bytes from waw02s07-in-f4.1e100.net (172.217.20.164): icmp_seq=1 ping_ttl_editText=55 time=7.95 ms
 * <p>
 * --- www.google.com ping statistics ---
 * 1 packets transmitted, 1 received, 0% packet loss, time 0ms
 * rtt min/avg/max/mdev = 7.950/7.950/7.950/0.000 ms
 */

public class PingOutputParser
{
	static private final String IPV4_REGEX = "\\b((((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))|(((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){1,3}\\*))\\b";
	static private final String IPV4_TARGET_REGEX = "\\(((((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))|(((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){1,3}\\*))\\):";
	static private final String HOSTNAME_REGEX = "(?<=[Ff]rom)(.*)(?=\\()";
	static private final Pattern IPV4_PATTERN = Pattern.compile(IPV4_REGEX);
	static private final Pattern IPV4_TARGET_PATTERN = Pattern.compile(IPV4_TARGET_REGEX);
	static private final Pattern HOSTNAME_PATTERN = Pattern.compile(HOSTNAME_REGEX);

	public static String extractPingAddressIp(String input)
	{
		Matcher matcher = IPV4_PATTERN.matcher(input);
		if(matcher.find())
		{
			return matcher.group();
		}
		else
		{
			return "";
		}
	}

	public static String extractPingTargetAddressIp(String input)
	{
		Matcher matcher1 = IPV4_TARGET_PATTERN.matcher(input);

		if(matcher1.find())
		{
			Matcher matcher2 = IPV4_PATTERN.matcher(matcher1.group());
			if(matcher2.find())
			{
				return matcher2.group();
			}
		}
		return "";
	}

	public static String extractPingHostname(String input)
	{
		Matcher matcher = HOSTNAME_PATTERN.matcher(input);
		if(matcher.find())
		{
			return matcher.group();
		}
		else
		{
			return " unknown";
		}
	}

	public static Map<String, Integer> extractPingStatistics()
	{
		Map<String, Integer> statistics = new HashMap<>();//TODO IMPLEMENT
		return statistics;
	}

	public static String binaryToString(int ipAddress)
	{
		return String.format("%d.%d.%d.%d", (ipAddress & 0xff), (ipAddress >> 8 & 0xff), (ipAddress >> 16 & 0xff), (ipAddress >> 24 & 0xff));
	}
}
