package kalucki.przemyslaw.unt.Utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.LinkProperties;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Bundle;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by przemek on 28.05.17.
 */

public class InternetUtils
{
	private static final String WIFI_CONNECTION = "WIFI";
	private static final String CELLULAR_CONNECTION = "CELLULAR";
	private static final String BLUETOOTH_CONNECTION = "BLUETOOTH";
	private static final String VPN_CONNECTION = "VPN";
	private static final String NO_CONNECTION = "OFFLINE";
	private Context context;
	private BroadcastReceiver broadcastReceiver;
	private PublishSubject<Boolean> internetStatusHotObservable;

	@Inject
	public InternetUtils(Context context)
	{
		this.context = context;
	}

	public String getCurrentConnectionType()
	{
		ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

		if(activeNetworkInfo != null && activeNetworkInfo.isConnected())
		{
			switch(activeNetworkInfo.getType())
			{
				case ConnectivityManager.TYPE_WIFI:
					return WIFI_CONNECTION;
				case ConnectivityManager.TYPE_MOBILE:
					return CELLULAR_CONNECTION;
				case ConnectivityManager.TYPE_BLUETOOTH:
					return BLUETOOTH_CONNECTION;
				case ConnectivityManager.TYPE_VPN:
					return VPN_CONNECTION;
				default:
					return NO_CONNECTION;
			}
		}
		else
		{
			return NO_CONNECTION;
		}
	}

	public boolean isNetworkConnected()
	{
		ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

		return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnectedOrConnecting();
	}

	public Observable<Boolean> isNetworkConnectedObservable()
	{
		return Observable.fromCallable(this::isNetworkConnected);
	}

	//TODO check NetworkCapabilities class as alternative
	public boolean isInternetOn()
	{
		try
		{//generate response code instead of fetching entire google page, faster
			HttpURLConnection urlc = (HttpURLConnection) (new URL("http://clients3.google.com/generate_204")
					.openConnection());//TODO what if google is down or banned in the country ?
			urlc.setRequestProperty("User-Agent", "Android");
			urlc.setRequestProperty("Connection", "close");
			urlc.setConnectTimeout(1000);
			urlc.connect();
			return (urlc.getResponseCode() == 204 && urlc.getContentLength() == 0);
		}
		catch(IOException e)
		{
			return false;
		}
	}

	public Observable<Boolean> isInternetOnObservable()
	{
		return Observable.fromCallable(this::isInternetOn);
	}

//	public Observable<Boolean> getInternetStatusObservable()
//	{
//		internetStatus = PublishSubject.create();
//		return internetStatus
//	}

	public void registerBroadCastReceiver()
	{
		internetStatusHotObservable = PublishSubject.create();

		final IntentFilter filter = new IntentFilter();
		filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);

		broadcastReceiver = new BroadcastReceiver()
		{
			@Override
			public void onReceive(Context context, Intent intent)
			{
				Bundle extras = intent.getExtras();
				NetworkInfo info = extras.getParcelable("networkInfo");
//				assert info != null;
				if(info != null && info.getState() == NetworkInfo.State.CONNECTED)
				{
					internetStatusHotObservable.onNext(isInternetOn());
				}
			}
		};

		context.registerReceiver(broadcastReceiver, filter);
	}

	public void unRegisterBroadCastReceiver()
	{
		context.unregisterReceiver(broadcastReceiver);
	}

	public String[] getDNSServersList()
	{
		List<String> result = new ArrayList<>();

		if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.M)
		{
			return null;
		}

		ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		Network network = connectivityManager.getActiveNetwork();
		LinkProperties linkProperties = connectivityManager.getLinkProperties(network);

		for(InetAddress address : linkProperties.getDnsServers())
		{
			result.add(address.getHostAddress());
		}

		return result.toArray(new String[result.size()]);
	}

}
