package kalucki.przemyslaw.unt.Utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by prka on 21.09.17.
 */

public class IpUtils
{
	//TODO this regex is wanky, doesnt detect last 4 bytes properly
	static private final String IPV4_REGEX = "(([0-1]?[0-9]{1,2}\\.)|(2[0-4][0-9]\\.)|(25[0-5]\\.)){3}(([0-1]?[0-9]{1,2})|(2[0-4][0-9])|(25[0-5]))";
	static private Pattern IPV4_PATTERN = Pattern.compile(IPV4_REGEX);

	public static String extractIpFromString(String value)
	{
		Matcher matcher = IPV4_PATTERN.matcher(value);
		if(matcher.find())
		{
			return matcher.group();
		}
		else
		{
			return "";
		}
	}

	public static String intToString(int ipAddress)
	{
		return String.format("%d.%d.%d.%d", (ipAddress & 0xff), (ipAddress >> 8 & 0xff), (ipAddress >> 16 & 0xff), (ipAddress >> 24 & 0xff));
	}

}
