package kalucki.przemyslaw.unt.Utils;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by prka on 26.09.17.
 *
 * @brief Custom drawable class used to make non editable text parts in EditText
 */

public class TextDrawable extends Drawable
{
	private final String text;
	private final Paint paint;

	public TextDrawable(String text)
	{
		this.text = text;
		this.paint = new Paint();

		paint.setColor(Color.BLACK);
		paint.setTextSize(16f);//TODO no hardcoded sizes change
		paint.setAntiAlias(true);
		paint.setTextAlign(Paint.Align.LEFT);
	}

	@Override
	public void draw(@NonNull Canvas canvas)
	{
		canvas.drawText(text, 0, 6, paint);
	}

	@Override
	public void setAlpha(@IntRange (from = 0, to = 255) int i)
	{
		paint.setAlpha(i);
	}

	@Override
	public void setColorFilter(@Nullable ColorFilter colorFilter)
	{
		paint.setColorFilter(colorFilter);
	}

	@Override
	public int getOpacity()
	{
		return PixelFormat.TRANSLUCENT;
	}
}
