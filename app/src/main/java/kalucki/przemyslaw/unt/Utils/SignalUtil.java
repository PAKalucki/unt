package kalucki.przemyslaw.unt.Utils;

/**
 * Created by prka on 20.11.17.
 */

public class SignalUtil
{
	public static float calcStrenghtPercent(float value)
	{
		//signal strength returned by android is in scale <-800,0> changing to 0%-100% here
		return 100 - (100 * (-1 * value) / 800);
	}
}
