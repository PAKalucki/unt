package kalucki.przemyslaw.unt.Utils;

/**
 * Created by prka on 07.11.17.
 */

public class UsageUtils
{
	public static String formatUsageOutput(long value)//TODO u sure about this math ?
	{
		if(value < 1024)
		{
			return String.valueOf(value) + " B";
		}
		else if(value >= 1024 && value < 1024 * 1024)
		{
			return String.valueOf(value / 1024) + " KiB";
		}
		else if(value >= 1024 * 1024 && value <= 1024 * 1024 * 1024)
		{
			return String.valueOf(value / (1024 * 1024)) + " MiB";
		}
		else if(value >= 1024 * 1024 * 1024)
		{
			return String.valueOf(value/(1024*1024*1024)) + " GiB";
		}
		return "Error";
	}
}
