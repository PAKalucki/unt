package kalucki.przemyslaw.unt.Utils;

import io.reactivex.Scheduler;

/**
 * Created by prka on 15.11.17.
 */

public interface SchedulerProvider
{
	Scheduler backgroundScheduler();
	Scheduler mainScheduler();
}
