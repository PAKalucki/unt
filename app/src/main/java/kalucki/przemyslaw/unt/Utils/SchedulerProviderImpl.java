package kalucki.przemyslaw.unt.Utils;

import javax.inject.Inject;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by prka on 15.11.17.
 */

public class SchedulerProviderImpl implements SchedulerProvider
{
	@Inject
	public SchedulerProviderImpl()
	{

	}

	@Override
	public Scheduler backgroundScheduler()
	{
		return Schedulers.io();
	}

	@Override
	public Scheduler mainScheduler()
	{
		return AndroidSchedulers.mainThread();
	}
}
