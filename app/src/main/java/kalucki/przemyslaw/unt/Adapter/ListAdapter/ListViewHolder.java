package kalucki.przemyslaw.unt.Adapter.ListAdapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import javax.annotation.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;
import kalucki.przemyslaw.unt.R;

/**
 * Created by przemek on 25.05.17.
 */

public class ListViewHolder extends RecyclerView.ViewHolder
{
	@Nullable @BindView (R.id.listItemImage) ImageView image;
	@Nullable @BindView (R.id.listItemText) TextView text;

	public ListViewHolder(View itemView)
	{
		super(itemView);
		ButterKnife.bind(this, itemView);
	}
}
