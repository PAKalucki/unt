package kalucki.przemyslaw.unt.Adapter.ListAdapter;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Nullable;

/**
 * Created by prka on 03.11.17.
 */

public final class ListItem implements Parcelable
{
	Bitmap icon;
	String text;

	public ListItem(@Nullable Bitmap icon, @Nullable String text)
	{
		this.icon = icon;
		this.text = text;
	}

	@Override
	public int describeContents()
	{
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeParcelable(this.icon, flags);
		dest.writeString(this.text);
	}

	protected ListItem(Parcel in)
	{
		this.icon = in.readParcelable(Bitmap.class.getClassLoader());
		this.text = in.readString();
	}

	public static final Parcelable.Creator<ListItem> CREATOR = new Parcelable.Creator<ListItem>()
	{
		@Override
		public ListItem createFromParcel(Parcel source)
		{
			return new ListItem(source);
		}

		@Override
		public ListItem[] newArray(int size)
		{
			return new ListItem[size];
		}
	};
}
