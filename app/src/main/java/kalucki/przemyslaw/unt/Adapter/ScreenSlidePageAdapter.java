package kalucki.przemyslaw.unt.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by przemek on 02.04.17.
 */

public class ScreenSlidePageAdapter extends FragmentStatePagerAdapter
{
	private final List<Fragment> fragmentList = new ArrayList<>();
	private final String tabTitles[];

	public ScreenSlidePageAdapter(FragmentManager fragmentManager, String tabTitles[])
	{
		super(fragmentManager);
		this.tabTitles = tabTitles;
	}

	@Override
	public Fragment getItem(int position)
	{
		return fragmentList.get(position);
	}

	@Override
	public int getCount()
	{
		return fragmentList.size();
	}

	@Override
	public CharSequence getPageTitle(int position)
	{
		return tabTitles[position];
	}

	public void addFragment(Fragment fragment)
	{
		fragmentList.add(fragment);
	}
}
