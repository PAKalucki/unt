package kalucki.przemyslaw.unt.Adapter.ListAdapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import kalucki.przemyslaw.unt.R;

/**
 * Created by przemek on 25.05.17.
 */

public class ListAdapter extends RecyclerView.Adapter<ListViewHolder>
{
	private List<ListItem> listItems;

	public ListAdapter(List<ListItem> listItems)
	{
		this.listItems = listItems;
	}

	@Override
	public ListViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
	{
		View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);

		return new ListViewHolder(itemView);
	}

	@Override
	public void onBindViewHolder(ListViewHolder holder, int position)
	{
		ListItem item = listItems.get(position);
		holder.text.setText(item.text);
		holder.image.setImageBitmap(item.icon);
	}

	@Override
	public int getItemCount()
	{
		return listItems.size();
	}

	public List<ListItem> getListItems()
	{
		return listItems;
	}

	public void clearAll()
	{
		int size = getItemCount();
		listItems.clear();
		notifyItemRangeRemoved(0, size);
	}
}
