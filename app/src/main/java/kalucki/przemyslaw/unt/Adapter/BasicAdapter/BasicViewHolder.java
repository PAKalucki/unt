package kalucki.przemyslaw.unt.Adapter.BasicAdapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import javax.annotation.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;
import kalucki.przemyslaw.unt.R;

/**
 * Created by prka on 21.11.17.
 */

public class BasicViewHolder extends RecyclerView.ViewHolder
{
	@Nullable @BindView(R.id.basic_item) TextView text;

	public BasicViewHolder(View itemView)
	{
		super(itemView);
		ButterKnife.bind(this, itemView);
	}
}
