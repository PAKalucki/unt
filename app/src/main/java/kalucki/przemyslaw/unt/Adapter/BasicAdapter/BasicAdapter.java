package kalucki.przemyslaw.unt.Adapter.BasicAdapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import kalucki.przemyslaw.unt.R;

/**
 * Created by prka on 21.11.17.
 */

public class BasicAdapter extends RecyclerView.Adapter<BasicViewHolder>
{
	private List<String> listItems;

	public BasicAdapter(List<String> listItems)
	{
		this.listItems = listItems;
	}

	@Override
	public BasicViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
	{
		View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.basic_item, parent, false);

		return new BasicViewHolder(itemView);
	}

	@Override
	public void onBindViewHolder(BasicViewHolder holder, int position)
	{
		String text = listItems.get(position);
		holder.text.setText(text);
	}

	@Override
	public int getItemCount()
	{
		return listItems.size();
	}

	public void clearAll()
	{
		int size = getItemCount();
		listItems.clear();
		notifyItemRangeRemoved(0, size);
	}

	public void addAll(List<String> list)
	{
		listItems.addAll(list);
		notifyDataSetChanged();
	}

	//column number from 0
	public void fillColumn(int columnNumber, int totalNumberOfColumns, List<String> newValues)//TODO check for too many newValues ?
	{
		for(int i = 0 + columnNumber, j = 0; j<newValues.size() && i<listItems.size(); i=i+totalNumberOfColumns, j++)
		{
			listItems.set(i, newValues.get(j));
		}
		notifyDataSetChanged();
	}

	public void fillRow(int row, int numberOfColumns, List<String> list)
	{

	}
}
