package kalucki.przemyslaw.unt.Features.DNSLookUp.ui;


import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.widget.EditText;

import org.xbill.DNS.DClass;
import org.xbill.DNS.Type;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import kalucki.przemyslaw.unt.Base.BaseRecyclerWithButtonFragment;
import kalucki.przemyslaw.unt.Features.DNSLookUp.DNSQuery;
import kalucki.przemyslaw.unt.Features.DNSLookUp.LookUpSettings;
import kalucki.przemyslaw.unt.Features.DNSLookUp.presenter.LookUpPresenter;
import kalucki.przemyslaw.unt.MainApplication;
import kalucki.przemyslaw.unt.R;
import kalucki.przemyslaw.unt.Utils.InternetUtils;

public class LookUpFragment extends BaseRecyclerWithButtonFragment implements LookUpView
{
	@BindView (R.id.base_editText) EditText editText;
	@Inject InternetUtils utils;
	@Inject LookUpPresenter presenter;
	@Inject SharedPreferences preferences;
	private static final String OUTPUTKEY = "LOOKUP_OUTPUT_KEY";
	private static final String STARTEDKEY = "LOOKUP_STARTED_KEY";

	public LookUpFragment()
	{
		// Required empty public constructor
	}

	@Override
	public void onAttach(Context context)
	{
		super.onAttach(context);
		presenter.bind(this);

		setIsStartedKey(STARTEDKEY);
		setOutputKey(OUTPUTKEY);
	}

	@Override
	public void onDetach()
	{
		super.onDetach();
		presenter.unbind();
	}

	@Override
	public LookUpSettings loadSettings()
	{
		return new LookUpSettings();
	}

	@OnClick ({R.id.base_button})
	public void onClick()
	{
		if(isStarted)
		{
			presenter.stop();
		}
		else
		{
			if(!TextUtils.isEmpty(editText.getText()))
			{
				DNSQuery query = new DNSQuery(editText.getText().toString(), utils.getDNSServersList(), Type.ANY, DClass.ANY);
				presenter.start(query);
			}
		}
	}

	@Override
	protected void injectDependencies(MainApplication application)
	{
		application.getLookUpSubComponent().inject(this);
	}
}
