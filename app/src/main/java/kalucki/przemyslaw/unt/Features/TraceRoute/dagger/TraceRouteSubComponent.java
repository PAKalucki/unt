package kalucki.przemyslaw.unt.Features.TraceRoute.dagger;

import dagger.Subcomponent;
import kalucki.przemyslaw.unt.Features.TraceRoute.ui.TraceRouteFragment;
import kalucki.przemyslaw.unt.Features.TraceRoute.ui.TraceRouteSettingsDialog;

/**
 * Created by prka on 16.11.17.
 */

@Trace
@Subcomponent(modules = TraceRouteModule.class)
public interface TraceRouteSubComponent
{
	void inject(TraceRouteFragment traceRouteFragment);
	void inject(TraceRouteSettingsDialog traceRouteSettingsDialog);
}
