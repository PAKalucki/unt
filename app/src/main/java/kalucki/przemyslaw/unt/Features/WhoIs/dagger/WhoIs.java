package kalucki.przemyslaw.unt.Features.WhoIs.dagger;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by prka on 16.11.17.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
@interface WhoIs
{
}
