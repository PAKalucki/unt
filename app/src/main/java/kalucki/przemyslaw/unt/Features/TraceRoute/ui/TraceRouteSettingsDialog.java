package kalucki.przemyslaw.unt.Features.TraceRoute.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import kalucki.przemyslaw.unt.Base.BaseDialogFragment;
import kalucki.przemyslaw.unt.MainApplication;
import kalucki.przemyslaw.unt.R;
import kalucki.przemyslaw.unt.Utils.TextDrawable;

/**
 * Created by prka on 27.09.17.
 */

public class TraceRouteSettingsDialog extends BaseDialogFragment
{
	@BindString (R.string.trace_count_key) String traceCountKey;
	@BindString (R.string.count_string) String traceCountString;
	@BindString (R.string.trace_hops_key) String traceHopsKey;
	@BindString (R.string.hops_string) String traceHopsString;
	@BindString (R.string.trace_timeout_key) String traceTimeoutKey;
	@BindString (R.string.timeout_string) String traceTimeoutString;
	@BindView (R.id.trace_count_editText) EditText editTextCount;
	@BindView (R.id.trace_hops_editText) EditText editTextHops;
	@BindView (R.id.trace_timeout_editText) EditText editTextTimeOut;
	@Inject SharedPreferences preferences;
	private Unbinder unbinder;

	public TraceRouteSettingsDialog()
	{
	}

	public static TraceRouteSettingsDialog newInstance()
	{
		return new TraceRouteSettingsDialog();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.traceroute_settings, container, false);

		unbinder = ButterKnife.bind(this, view);

		return view;
	}

	@Override
	public void onDestroyView()
	{
		super.onDestroyView();
		unbinder.unbind();
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle bundle)
	{
		super.onViewCreated(view, bundle);

		editTextHops.setCompoundDrawablesWithIntrinsicBounds(new TextDrawable(traceHopsString), null, null, null);
		editTextCount.setCompoundDrawablesWithIntrinsicBounds(new TextDrawable(traceCountString), null, null, null);
		editTextTimeOut.setCompoundDrawablesWithIntrinsicBounds(new TextDrawable(traceTimeoutString), null, null, null);

		editTextHops.setCompoundDrawablePadding(traceHopsString.length() * 10);
		editTextCount.setCompoundDrawablePadding(traceCountString.length() * 10);
		editTextTimeOut.setCompoundDrawablePadding(traceTimeoutString.length() * 10);

		editTextHops.setText(String.valueOf(preferences.getInt(traceHopsKey, 10)));
		editTextCount.setText(String.valueOf(preferences.getInt(traceCountKey, 1)));
		editTextTimeOut.setText(String.valueOf(preferences.getInt(traceTimeoutKey, 0)));
	}

	private void readAndSavePreferences()
	{
		SharedPreferences.Editor editor = preferences.edit();

		editor.putInt(traceHopsKey, Integer.valueOf(editTextHops.getText().toString()));
		editor.putInt(traceCountKey, Integer.valueOf(editTextCount.getText().toString()));
		editor.putInt(traceTimeoutKey, Integer.valueOf(editTextTimeOut.getText().toString()));
		editor.apply();
		dismiss();
	}

	private void cancel()
	{
		dismiss();
	}

	private void restoreDefaultSettings()
	{
		SharedPreferences.Editor editor = preferences.edit();

		editor.putInt(traceHopsKey, 10);
		editor.putInt(traceCountKey, 1);
		editor.putInt(traceTimeoutKey, 0);
		editor.apply();
		dismiss();
	}

	@OnClick ({R.id.trace_ok_button, R.id.trace_cancel_button, R.id.trace_restore_button})
	public void onClick(View view)
	{
		switch(view.getId())
		{
			case R.id.trace_ok_button:
				readAndSavePreferences();
				break;
			case R.id.trace_cancel_button:
				cancel();
				break;
			case R.id.trace_restore_button:
				restoreDefaultSettings();
				break;
		}
	}

	@Override
	protected void injectDependencies(MainApplication application)
	{
		application.getTraceRouteSubComponent().inject(this);
	}
}
