package kalucki.przemyslaw.unt.Features.UsageStats;

/**
 * Created by prka on 13.11.17.
 */

public final class UsageStatsResult
{
	public final long download;
	public final long upload;

	public UsageStatsResult(long download, long upload)
	{
		this.download = download;
		this.upload = upload;
	}
}
