package kalucki.przemyslaw.unt.Features.WhoIs.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import kalucki.przemyslaw.unt.Base.BaseDialogFragment;
import kalucki.przemyslaw.unt.MainApplication;
import kalucki.przemyslaw.unt.R;
import kalucki.przemyslaw.unt.Utils.TextDrawable;

/**
 * Created by prka on 27.09.17.
 */

public class WhoIsSettingsDialog extends BaseDialogFragment
{
	@BindString (R.string.default_string) String defaultString;
	@BindString(R.string.whois_server_key) String whoisServerKey;
	@BindString(R.string.serverName_string) String whoisServerNameString;
	@BindString(R.string.whois_timeout_key) String whoisTimeoutKey;
	@BindString(R.string.timeout_string) String whoisTimeoutString;
	@BindView (R.id.whois_server_editText) EditText editTextServer;
	@BindView (R.id.whois_timeout_editText) EditText editTextTimeout;
	@Inject SharedPreferences preferences;
	private Unbinder unbinder;

	public WhoIsSettingsDialog()
	{
		// Required empty public constructor
	}

	public static WhoIsSettingsDialog newInstance()
	{
		return new WhoIsSettingsDialog();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.whois_settings, container, false);

		unbinder = ButterKnife.bind(this, view);

		return view;
	}

	@Override
	public void onDestroyView()
	{
		super.onDestroyView();
		unbinder.unbind();
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle bundle)
	{
		super.onViewCreated(view, bundle);

		editTextTimeout.setCompoundDrawablesWithIntrinsicBounds(new TextDrawable(whoisTimeoutString), null, null, null);
		editTextServer.setCompoundDrawablesWithIntrinsicBounds(new TextDrawable(whoisServerNameString), null, null, null);

		editTextTimeout.setCompoundDrawablePadding(whoisTimeoutString.length() * 10);
		editTextServer.setCompoundDrawablePadding(whoisServerNameString.length() * 10);

		editTextTimeout.setText(String.valueOf(preferences.getInt(whoisTimeoutKey, 0)));
		editTextServer.setText(String.valueOf(preferences.getString(whoisServerKey, defaultString)));
	}

	private void readAndSavePreferences()
	{
		SharedPreferences.Editor editor = preferences.edit();

		editor.putInt(whoisTimeoutKey, Integer.valueOf(editTextTimeout.getText().toString()));
		editor.putString(whoisServerKey, editTextServer.getText().toString());
		editor.apply();
		dismiss();
	}

	private void cancel()
	{
		dismiss();
	}

	private void restoreDefaultSettings()
	{
		SharedPreferences.Editor editor = preferences.edit();

		editor.putInt(whoisTimeoutKey, 0);
		editor.putString(whoisServerKey, defaultString);
		editor.apply();
		dismiss();
	}

	@OnClick ({R.id.whois_ok_button, R.id.whois_cancel_button, R.id.whois_restore_button})
	public void onClick(View view)
	{
		switch(view.getId())
		{
			case R.id.whois_ok_button:
				readAndSavePreferences();
				break;
			case R.id.whois_cancel_button:
				cancel();
				break;
			case R.id.whois_restore_button:
				restoreDefaultSettings();
				break;
		}
	}

	@Override
	protected void injectDependencies(MainApplication application)
	{
		application.getWhoIsSubComponent().inject(this);
	}
}
