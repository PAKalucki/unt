package kalucki.przemyslaw.unt.Features.Ping.dagger;

import dagger.Subcomponent;
import kalucki.przemyslaw.unt.Features.Ping.ui.PingFragment;
import kalucki.przemyslaw.unt.Features.Ping.ui.PingSettingsDialog;

/**
 * Created by prka on 16.11.17.
 */

@Ping
@Subcomponent(modules = PingModule.class)
public interface PingSubComponent
{
	void inject(PingFragment pingFragment);
	void inject(PingSettingsDialog pingSettingsDialog);
}
