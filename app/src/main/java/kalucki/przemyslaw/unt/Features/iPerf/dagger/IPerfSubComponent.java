package kalucki.przemyslaw.unt.Features.iPerf.dagger;

import javax.inject.Inject;

import dagger.Subcomponent;
import kalucki.przemyslaw.unt.Features.iPerf.ui.IPerfFragment;
import kalucki.przemyslaw.unt.Features.iPerf.ui.IPerfSettingsDialog;

/**
 * Created by prka on 16.11.17.
 */

@IPerf
@Subcomponent(modules = IPerfModule.class)
public interface IPerfSubComponent
{
	void inject(IPerfFragment iPerfFragment);
	void inject(IPerfSettingsDialog iPerfSettingsDialog);
}
