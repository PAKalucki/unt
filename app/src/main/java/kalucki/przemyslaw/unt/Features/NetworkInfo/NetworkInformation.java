package kalucki.przemyslaw.unt.Features.NetworkInfo;

import io.reactivex.Flowable;

/**
 * Created by prka on 21.11.17.
 */

public interface NetworkInformation
{
	Flowable<NetworkInfoResult> getNetworkInfo();
}
