package kalucki.przemyslaw.unt.Features.WhoIs.presenter;

import kalucki.przemyslaw.unt.Base.BasePresenter;
import kalucki.przemyslaw.unt.Features.WhoIs.ui.WhoIsView;

/**
 * Created by prka on 16.11.17.
 */

public interface WhoIsPresenter extends BasePresenter<WhoIsView>
{
	void start(String s);
	void stop();
}
