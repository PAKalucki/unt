package kalucki.przemyslaw.unt.Features.WhoIs.ui;

import kalucki.przemyslaw.unt.Base.BaseStandardView;
import kalucki.przemyslaw.unt.Features.WhoIs.WhoIsSettings;

/**
 * Created by prka on 02.11.17.
 */

public interface WhoIsView extends BaseStandardView
{
	WhoIsSettings loadSettings();
}
