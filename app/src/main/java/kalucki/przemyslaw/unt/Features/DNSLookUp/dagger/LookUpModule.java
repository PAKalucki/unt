package kalucki.przemyslaw.unt.Features.DNSLookUp.dagger;

import dagger.Module;
import dagger.Provides;
import kalucki.przemyslaw.unt.Features.DNSLookUp.presenter.LookUpPresenter;
import kalucki.przemyslaw.unt.Features.DNSLookUp.presenter.LookUpPresenterImpl;

/**
 * Created by prka on 16.11.17.
 */

@Module
public class LookUpModule
{
	@Provides
	@LookUp
	LookUpPresenter provideLookUpPresenter(LookUpPresenterImpl presenter)
	{
		return presenter;
	}
}
