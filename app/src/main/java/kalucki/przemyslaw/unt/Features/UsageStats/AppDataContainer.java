package kalucki.przemyslaw.unt.Features.UsageStats;

import android.graphics.Bitmap;

/**
 * Created by prka on 03.11.17.
 */

public final class AppDataContainer
{
	public final int uid;
	public final Bitmap icon;
	public final String name;
	public final long download;
	public final long upload;

	public AppDataContainer(int uid, Bitmap icon, String name, long download, long upload)
	{
		this.uid = uid;
		this.icon = icon;
		this.name = name;
		this.download = download;
		this.upload = upload;
	}
}
