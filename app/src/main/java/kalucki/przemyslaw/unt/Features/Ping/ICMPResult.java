package kalucki.przemyslaw.unt.Features.Ping;


/**
 * Created by prka on 24.10.17.
 */

public final class ICMPResult
{
	public final String output;
	public final boolean success;

	public ICMPResult(String output, boolean success)
	{
		this.output = output;
		this.success = success;
	}

	@Override
	public String toString()
	{
		return "ICMPResult{" +
				"output='" + output + '\'' +
				", success=" + success +
				'}';
	}
}
