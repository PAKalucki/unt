package kalucki.przemyslaw.unt.Features.Wifi.dagger;

import dagger.Module;
import dagger.Provides;
import kalucki.przemyslaw.unt.Features.Wifi.presenter.WifiPresenter;
import kalucki.przemyslaw.unt.Features.Wifi.presenter.WifiPresenterImpl;

/**
 * Created by prka on 16.11.17.
 */

@Module
public class WifiModule
{
	@Provides
	@Wifi
	WifiPresenter provideWifiPresenter(WifiPresenterImpl presenter)
	{
		return presenter;
	}
}
