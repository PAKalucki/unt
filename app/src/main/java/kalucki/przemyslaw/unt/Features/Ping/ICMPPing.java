package kalucki.przemyslaw.unt.Features.Ping;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;

import io.reactivex.Observable;
import kalucki.przemyslaw.unt.Utils.PingComandFormatter;

/**
 * Created by prka on 20.09.17.
 * <p>
 * ICMPPing class using Process and ping unix command,
 * as far as I know it's available in almost all android phones without root
 * <p>
 * ICMPPing successfull returns all in getInputStream
 * Couldn't resolve host issues return in getErrorStream
 * <p>
 * <p>
 * EXAMPLE PING OUTPUT:
 * <p>
 * PING www.google.com (172.217.20.164) 56(84) bytes of data.
 * 64 bytes from waw02s07-in-f4.1e100.net (172.217.20.164): icmp_seq=1 ping_ttl_editText=55 time=7.95 ms
 * <p>
 * --- www.google.com ping statistics ---
 * 1 packets transmitted, 1 received, 0% packet loss, time 0ms
 * rtt min/avg/max/mdev = 7.950/7.950/7.950/0.000 ms
 */

public class ICMPPing
{
	private boolean isActive = false;
	private PingSettings settings;
	private Process process;
	private static final String killCommand = "kill -INT ";
	private String address;

	public void setSettings(PingSettings settings)
	{
		this.settings = settings;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public Observable<ICMPResult> getOutput()
	{
		isActive = true;
		return Observable.create(
				subscriber->
				{
					try
					{
						Log.i("PING", "ADDRESS: " + address);
						process = Runtime.getRuntime().exec(PingComandFormatter.getCommand(address, settings));
						BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
						BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));

						String s;
						String e;

						while((s = stdInput.readLine()) != null && process != null)
						{
							subscriber.onNext(new ICMPResult(s, true));
						}

						while((e = stdError.readLine()) != null && process != null)
						{
							subscriber.onNext(new ICMPResult(e, false));
						}

						stdInput.close();
						stdError.close();

						if(process != null)
						{
							process.destroy();
						}

						Log.i("PING", "KONIEC");
						subscriber.onComplete();
						isActive = false;//TODO put this in onComplete() somehow?
					}
					catch(IOException e)
					{
						Log.i("PING", "IOEXCEPTION");
						isActive = false;
						subscriber.onError(e);
						if(process != null)
						{
							process.destroy();
						}
					}
				});
	}

	public void stop()
	{
		try
		{
			if(isActive && getPid(process) != 0)
			{
				Log.i("Wykonywanie: ", killCommand + getPid(process));
				Runtime.getRuntime().exec(killCommand + getPid(process)).waitFor();//TODO is this waitFor a good idea?
				isActive = false;
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
			isActive = false;
		}
		catch(InterruptedException e)
		{
			e.printStackTrace();
			isActive = false;
		}
	}

	private static int getPid(Process process)//TODO since this is static anyways move it to Utils ?
	{
		int pid = 0; //pid 0 kills entire app, be carefull with this

		try
		{
			Field f = process.getClass().getDeclaredField("pid");
			if(f != null)
			{
				f.setAccessible(true);
				pid = f.getInt(process);
				f.setAccessible(false);
			}
		}
		catch(Throwable e)
		{
			pid = 0;
		}
		return pid;
	}

	public boolean isActive()
	{
		return isActive;
	}
}
