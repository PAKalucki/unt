package kalucki.przemyslaw.unt.Features.Lan.ui;

import kalucki.przemyslaw.unt.Features.Lan.LanSettings;

/**
 * Created by prka on 02.11.17.
 */

public interface LanView
{
	void start();
	void stop();
	void update(String value);
	LanSettings loadSettings();
}
