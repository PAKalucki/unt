package kalucki.przemyslaw.unt.Features.Lan.dagger;

import dagger.Subcomponent;
import kalucki.przemyslaw.unt.Features.Lan.ui.LanFragment;
import kalucki.przemyslaw.unt.Features.Lan.ui.LanSettingsDialog;

/**
 * Created by prka on 16.11.17.
 */

@Lan
@Subcomponent(modules = LanModule.class)
public interface LanSubComponent
{
	void inject(LanFragment lanFragment);
	void inject(LanSettingsDialog lanSettingsDialog);
}
