package kalucki.przemyslaw.unt.Features.UPnPScan.presenter;

import kalucki.przemyslaw.unt.Base.BasePresenter;
import kalucki.przemyslaw.unt.Features.UPnPScan.ui.UPnPScanView;

/**
 * Created by prka on 16.11.17.
 */

public interface UPnPScanPresenter extends BasePresenter<UPnPScanView>
{
	void start();
	void stop();
}
