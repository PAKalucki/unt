package kalucki.przemyslaw.unt.Features.Ping.presenter;

import kalucki.przemyslaw.unt.Base.BasePresenter;
import kalucki.przemyslaw.unt.Features.Ping.ui.PingView;

/**
 * Created by prka on 16.11.17.
 */

public interface PingPresenter extends BasePresenter<PingView>
{
	void start(String s);
	void stop();
}
