package kalucki.przemyslaw.unt.Features.PortScanner;

/**
 * Created by prka on 30.11.17.
 */

public final class PortScanResult
{
	public final boolean success;
	public final int port;

	public PortScanResult(boolean success, int port)
	{
		this.success = success;
		this.port = port;
	}
}
