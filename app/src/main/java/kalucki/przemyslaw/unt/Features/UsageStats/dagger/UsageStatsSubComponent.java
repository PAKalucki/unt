package kalucki.przemyslaw.unt.Features.UsageStats.dagger;

import dagger.Subcomponent;
import kalucki.przemyslaw.unt.Features.UsageStats.ui.UsageStatsFragment;
import kalucki.przemyslaw.unt.Features.UsageStats.ui.UsageStatsSettingsDialog;

/**
 * Created by prka on 16.11.17.
 */

@Usage
@Subcomponent(modules = UsageStatsModule.class)
public interface UsageStatsSubComponent
{
	void inject(UsageStatsFragment usageStatsFragment);
	void inject(UsageStatsSettingsDialog usageStatsSettingsDialog);
}
