package kalucki.przemyslaw.unt.Features.UsageStats.presenter;

import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.disposables.CompositeDisposable;
import kalucki.przemyslaw.unt.Features.UsageStats.UsageStatistics;
import kalucki.przemyslaw.unt.Features.UsageStats.UsageStatsSettings;
import kalucki.przemyslaw.unt.Features.UsageStats.ui.UsageStatsView;
import kalucki.przemyslaw.unt.Utils.SchedulerProvider;
import kalucki.przemyslaw.unt.Utils.UsageUtils;

/**
 * Created by prka on 20.10.17.
 */
//TODO FOR ALL need some method to determine when to dispose of subscripttions
public class UsageStatsPresenterImpl implements UsageStatsPresenter//TODO need something to listen to settings change
{
	private CompositeDisposable compositeDisposable = new CompositeDisposable();
	private WeakReference<UsageStatsView> view;
	private SchedulerProvider scheduler;

	@Inject
	public UsageStatsPresenterImpl(SchedulerProvider scheduler)
	{
		this.scheduler = scheduler;
	}

	@Override
	public void bind(UsageStatsView view)
	{
		this.view = new WeakReference<>(view);
	}

	@Override
	public void unbind()
	{
		view.clear();
		view = null;

		if(compositeDisposable != null && !compositeDisposable.isDisposed())
		{
			compositeDisposable.clear();
		}
	}

	@Override
	public void start()
	{
		UsageStatistics stats = view.get().getUsage();
		UsageStatsSettings settings = view.get().loadSettings();

		view.get().start();
		stats.setInterval(settings.poolingIntervalGraph);

		//download
		compositeDisposable.add(
				Flowable.interval(settings.poolingIntervalGraph, TimeUnit.MILLISECONDS)
				        .concatMapEager(value->stats.getCurrentTotalDownloadObservable())
				        .subscribeOn(scheduler.backgroundScheduler())
				        .observeOn(scheduler.mainScheduler())
				        .subscribe(value->
				                   {
					                   if(view != null)
					                   {
						                   view.get().updateGraph(value.floatValue(), 0);
					                   }
				                   })
		);

		//upload
		compositeDisposable.add(
				Flowable.interval(settings.poolingIntervalGraph, TimeUnit.MILLISECONDS)
				        .concatMapEager(value->stats.getCurrentTotalUploadObservable())
				        .subscribeOn(scheduler.backgroundScheduler())
				        .observeOn(scheduler.mainScheduler())
				        .subscribe(value->
				                   {
					                   if(view != null)
					                   {
						                   view.get().updateGraph(value.floatValue(), 1);
					                   }
				                   })
		);
	}

	@Override
	public void stop()
	{
		view.get().stop();
	}

	@Override
	public void refresh()
	{
		view.get().clear();
		UsageStatistics stats = view.get().getUsage();

		compositeDisposable.add(
				stats.getApps()
				     .subscribeOn(scheduler.backgroundScheduler())
				     .observeOn(scheduler.mainScheduler())
				     .doOnNext(data->
				               {
					               if(view != null)
					               {
						               //TODO this is a big complication if you want to sort by download/upload later, easiest solution is probably new list item with separate fields for each
						               String output = " Download: " + UsageUtils.formatUsageOutput(data.download) + " Upload: " + UsageUtils
								               .formatUsageOutput(data.upload);
						               view.get().updateTable(data, output);
					               }
				               })
				     .doOnComplete(()->
				                   {
					                   if(view != null)
					                   {
						                   //TODO this throw nullptr expection when view is not visible, need solution, temporary fix checking for null
						                   view.get().stop();
					                   }
				                   })
				     .subscribe());
	}
}
