package kalucki.przemyslaw.unt.Features.TraceRoute.presenter;

import kalucki.przemyslaw.unt.Base.BasePresenter;
import kalucki.przemyslaw.unt.Features.TraceRoute.ui.TraceRouteView;

/**
 * Created by prka on 16.11.17.
 */

public interface TraceRoutePresenter extends BasePresenter<TraceRouteView>
{
	void start(String s);
	void stop();
}
