package kalucki.przemyslaw.unt.Features.WhoIs;

import org.apache.commons.net.whois.WhoisClient;

import java.io.IOException;

import io.reactivex.Observable;

/**
 * Created by prka on 27.09.17.
 */

public class WhoIs
{
	private WhoisClient client;
	private String hostname;
	private static final String DEFAULT_WHOIS_SERVER = WhoisClient.DEFAULT_HOST;
	private WhoIsSettings settings;

	public void setSettings(WhoIsSettings settings)
	{
		this.settings = settings;
	}

	public Observable<String> getOutput(String value)//TODO throws ConnectException ??
	{
		return Observable.create(subscriber->
		{
			client = new WhoisClient();

			WhoIsSettings whoIsSettings = (WhoIsSettings) settings;//TODO OVERENGINEERING?

			if(whoIsSettings.timeout > 0)
			{
				client.setConnectTimeout(whoIsSettings.timeout);//TODO just ugly ugh
			}

			if(!whoIsSettings.serverName.equals("default"))//TODO staph plz
			{
				hostname = whoIsSettings.serverName;
			}
			else
			{
				hostname = DEFAULT_WHOIS_SERVER;
			}

			try
			{
				client.connect(hostname);
				subscriber.onNext(client.query("=" + value.trim()));
//				client.disconnect();
				subscriber.onComplete();
			}
			catch(IOException e)
			{
				subscriber.onError(e);
				e.printStackTrace();
				client.disconnect();
			}
		});
	}

	public void stop()
	{
		if(client != null && client.isConnected())
		{
			try
			{
				client.disconnect();
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
		}
	}
}
