package kalucki.przemyslaw.unt.Features.UPnPScan.ui;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import dagger.Lazy;
import kalucki.przemyslaw.unt.Base.BaseFragment;
import kalucki.przemyslaw.unt.Features.UPnPScan.presenter.UPnPScanPresenter;
import kalucki.przemyslaw.unt.MainApplication;
import kalucki.przemyslaw.unt.R;

public class UPnPScanFragment extends BaseFragment implements UPnPScanView
{
	@Inject Lazy<UPnPScanPresenter> presenter;

	public UPnPScanFragment()
	{
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState)
	{
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_upnp_scan, container, false);
	}

	@Override
	protected void injectDependencies(MainApplication application)
	{
		application.getUPnPScanSubComponent().inject(this);
	}
}
