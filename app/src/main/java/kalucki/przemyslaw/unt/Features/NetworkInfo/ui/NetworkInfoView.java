package kalucki.przemyslaw.unt.Features.NetworkInfo.ui;

import java.util.List;

import kalucki.przemyslaw.unt.Features.NetworkInfo.NetworkInfoSettings;
import kalucki.przemyslaw.unt.Features.NetworkInfo.NetworkInformation;

/**
 * Created by prka on 02.11.17.
 */

public interface NetworkInfoView
{
	void start();

	void updateTable(List<String> values);

	void updateGraph(Float value);

	NetworkInfoSettings loadSettings();

	NetworkInformation getNetworkInformation();
}
