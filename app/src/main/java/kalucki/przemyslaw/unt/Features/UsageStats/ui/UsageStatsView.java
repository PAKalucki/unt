package kalucki.przemyslaw.unt.Features.UsageStats.ui;

import kalucki.przemyslaw.unt.Features.UsageStats.AppDataContainer;
import kalucki.przemyslaw.unt.Features.UsageStats.UsageStatistics;
import kalucki.przemyslaw.unt.Features.UsageStats.UsageStatsSettings;

/**
 * Created by prka on 02.11.17.
 */

public interface UsageStatsView
{
	void start();
	void stop();
	void updateGraph(Float value, int dataset);
	void updateTable(AppDataContainer data, String newValue);
	void clear();
	UsageStatistics getUsage();
	UsageStatsSettings loadSettings();
}
