package kalucki.przemyslaw.unt.Features.Ping.ui;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import butterknife.Unbinder;
import kalucki.przemyslaw.unt.Base.BaseDialogFragment;
import kalucki.przemyslaw.unt.MainApplication;
import kalucki.przemyslaw.unt.R;
import kalucki.przemyslaw.unt.Utils.TextDrawable;

public class PingSettingsDialog extends BaseDialogFragment
{
	@BindString (R.string.count_string) String pingCountString;
	@BindString (R.string.interval_string) String pingIntervalString;
	@BindString (R.string.packetsize_string) String pingPacketSizeString;
	@BindString (R.string.ping_count_key) String pingCountKey;
	@BindString (R.string.ping_interval_key) String pingIntervalKey;
	@BindString (R.string.ping_mode_key) String pingModeKey;
	@BindString (R.string.ping_packetsize_key) String pingPacketsizeKey;
	@BindString (R.string.ping_port_key) String pingPortKey;
	@BindString (R.string.ping_timeout_key) String pingTimeoutKey;
	@BindString (R.string.ping_ttl_key) String pingTTLKey;
	@BindString (R.string.port_string) String pingPortString;
	@BindString (R.string.timeout_string) String pingTimeoutString;
	@BindString (R.string.ttl_string) String pingTTLString;
	@BindView (R.id.ping_count_editText) EditText editTextCount;
	@BindView (R.id.ping_interval_editText) EditText editTextInterval;
	@BindView (R.id.ping_mode_spinner) Spinner spinnerPingMode;
	@BindView (R.id.ping_packetsize_editText) EditText editTextPacketSize;
	@BindView (R.id.ping_port_editText) EditText editTextPort;
	@BindView (R.id.ping_timeout_editText) EditText editTextTimeOut;
	@BindView (R.id.ping_ttl_editText) EditText editTextTTL;
	@Inject SharedPreferences preferences;
	private Unbinder unbinder;

	public PingSettingsDialog()
	{
		// Required empty public constructor
	}

	public static PingSettingsDialog newInstance()
	{
		return new PingSettingsDialog();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.ping_settings, container, false);

		unbinder = ButterKnife.bind(this, view);

		return view;
	}

	@Override
	public void onDestroyView()
	{
		super.onDestroyView();
		unbinder.unbind();
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle bundle)
	{
		super.onViewCreated(view, bundle);

		ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter
				.createFromResource(view.getContext(), R.array.ping_mode_spinner, android.R.layout.simple_spinner_item);
		arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerPingMode.setAdapter(arrayAdapter);

		editTextCount.setCompoundDrawablesWithIntrinsicBounds(new TextDrawable(pingCountString), null, null, null);
		editTextInterval.setCompoundDrawablesWithIntrinsicBounds(new TextDrawable(pingIntervalString), null, null, null);
		editTextPacketSize.setCompoundDrawablesWithIntrinsicBounds(new TextDrawable(pingPacketSizeString), null, null, null);
		editTextTTL.setCompoundDrawablesWithIntrinsicBounds(new TextDrawable(pingTTLString), null, null, null);
		editTextTimeOut.setCompoundDrawablesWithIntrinsicBounds(new TextDrawable(pingTimeoutString), null, null, null);
		editTextPort.setCompoundDrawablesWithIntrinsicBounds(new TextDrawable(pingPortString), null, null, null);

		editTextCount.setCompoundDrawablePadding(pingCountString.length() * 10);
		editTextInterval.setCompoundDrawablePadding(pingIntervalString.length() * 10);
		editTextPacketSize.setCompoundDrawablePadding(pingPacketSizeString.length() * 10);
		editTextTTL.setCompoundDrawablePadding(pingTTLString.length() * 10);
		editTextTimeOut.setCompoundDrawablePadding(pingTimeoutString.length() * 10);
		editTextPort.setCompoundDrawablePadding(pingPortString.length() * 10);

		spinnerPingMode.setSelection(preferences.getInt(pingModeKey, 0));
		editTextCount.setText(String.valueOf(preferences.getInt(pingCountKey, 0)));
		editTextInterval.setText(String.valueOf(preferences.getInt(pingIntervalKey, 0)));
		editTextPacketSize.setText(String.valueOf(preferences.getInt(pingPacketsizeKey, 0)));
		editTextTTL.setText(String.valueOf(preferences.getInt(pingTTLKey, 0)));
		editTextTimeOut.setText(String.valueOf(preferences.getInt(pingTimeoutKey, 0)));
		editTextPort.setText(String.valueOf(preferences.getInt(pingPortKey, 80)));

		//to disable controls basing on mode
		switch(spinnerPingMode.getSelectedItemPosition())
		{
			case 0:
				onICMPMode();
				break;
			case 1:
				onTCPMode();
				break;
		}
	}

	private void readAndSavePreferences()
	{
		SharedPreferences.Editor editor = preferences.edit();

		editor.putInt(pingModeKey, spinnerPingMode.getSelectedItemPosition());
		editor.putInt(pingCountKey, Integer.valueOf(editTextCount.getText().toString()));
		editor.putInt(pingIntervalKey, Integer.valueOf(editTextInterval.getText().toString()));
		editor.putInt(pingPacketsizeKey, Integer.valueOf(editTextPacketSize.getText().toString()));
		editor.putInt(pingTTLKey, Integer.valueOf(editTextTTL.getText().toString()));
		editor.putInt(pingTimeoutKey, Integer.valueOf(editTextTimeOut.getText().toString()));
		editor.putInt(pingPortKey, Integer.valueOf(editTextPort.getText().toString()));
		editor.apply();
		dismiss();
	}

	private void cancel()
	{
		dismiss();
	}

	private void restoreDefaultSettings()
	{
		SharedPreferences.Editor editor = preferences.edit();

		editor.putInt(pingModeKey, 0);
		editor.putInt(pingCountKey, 0);
		editor.putInt(pingIntervalKey, 1000);
		editor.putInt(pingPacketsizeKey, 0);
		editor.putInt(pingTTLKey, 0);
		editor.putInt(pingTimeoutKey, 200);
		editor.putInt(pingPortKey, 80);
		editor.apply();
		dismiss();
	}

	@OnClick ({R.id.ping_ok_button, R.id.ping_cancel_button, R.id.ping_restore_button})
	public void onClick(View view)
	{
		switch(view.getId())
		{
			case R.id.ping_ok_button:
				readAndSavePreferences();
				break;
			case R.id.ping_cancel_button:
				cancel();
				break;
			case R.id.ping_restore_button:
				restoreDefaultSettings();
				break;
		}
	}

	@OnItemSelected (R.id.ping_mode_spinner)
	public void onItemSelected(int position)
	{
		spinnerPingMode.setSelection(position);
		switch(spinnerPingMode.getSelectedItemPosition())
		{
			case 0:
				onICMPMode();
				break;
			case 1:
				onTCPMode();
				break;
		}
	}

	private void onTCPMode()
	{
		editTextPacketSize.setEnabled(false);
		editTextTTL.setEnabled(false);
		editTextPort.setEnabled(true);
	}

	private void onICMPMode()
	{
		editTextPacketSize.setEnabled(true);
		editTextTTL.setEnabled(true);
		editTextPort.setEnabled(false);
	}

	@Override
	protected void injectDependencies(MainApplication application)
	{
		application.getPingSubComponent().inject(this);
	}
}
