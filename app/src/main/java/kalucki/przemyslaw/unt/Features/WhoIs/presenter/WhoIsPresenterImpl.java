package kalucki.przemyslaw.unt.Features.WhoIs.presenter;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import kalucki.przemyslaw.unt.Features.WhoIs.WhoIs;
import kalucki.przemyslaw.unt.Features.WhoIs.ui.WhoIsView;
import kalucki.przemyslaw.unt.Utils.SchedulerProvider;

/**
 * Created by prka on 27.09.17.
 */

public class WhoIsPresenterImpl implements WhoIsPresenter
{
	private WeakReference<WhoIsView> view;
	private CompositeDisposable compositeDisposable = new CompositeDisposable();
	private WhoIs whoIs = new WhoIs();
	SchedulerProvider scheduler;

	@Inject
	public WhoIsPresenterImpl(SchedulerProvider scheduler)
	{
		this.scheduler = scheduler;
	}

	@Override
	public void bind(WhoIsView view)
	{
		this.view = new WeakReference<>(view);
	}

	@Override
	public void unbind()
	{
		view.clear();
		view = null;
	}

	@Override
	public void start(String address)
	{
		view.get().start();
		whoIs.setSettings(view.get().loadSettings());

		compositeDisposable.add(
				whoIs.getOutput(address)
				     .subscribeOn(scheduler.backgroundScheduler())
				     .observeOn(scheduler.mainScheduler())
				     .doOnNext(value->
				               {
					               if(view != null)
					               {
						               if(value != null && !value.isEmpty())
						               {
							               view.get().update(value);
						               }
					               }
				               })
				     .doOnComplete(()->
				                   {
					                   if(view != null)
					                   {
						                   view.get().stop();
					                   }
				                   })
				     .doOnError(value->
				                {
					                if(view != null)
					                {
						                view.get().update("NETWORK ERROR");
					                }
				                })
				     .subscribe()
		);
	}

	@Override
	public void stop()
	{
		view.get().stop();
		whoIs.stop();
		if(compositeDisposable != null && !compositeDisposable.isDisposed())
		{
			compositeDisposable.clear();
		}
	}

}
