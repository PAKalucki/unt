package kalucki.przemyslaw.unt.Features.DNSLookUp.presenter;

import kalucki.przemyslaw.unt.Base.BasePresenter;
import kalucki.przemyslaw.unt.Features.DNSLookUp.DNSQuery;
import kalucki.przemyslaw.unt.Features.DNSLookUp.ui.LookUpView;

/**
 * Created by prka on 16.11.17.
 */

public interface LookUpPresenter extends BasePresenter<LookUpView>
{
	void start(DNSQuery query);
	void stop();
}
