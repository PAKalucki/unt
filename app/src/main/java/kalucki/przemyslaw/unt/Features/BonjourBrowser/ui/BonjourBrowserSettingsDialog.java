package kalucki.przemyslaw.unt.Features.BonjourBrowser.ui;

import kalucki.przemyslaw.unt.Base.BaseDialogFragment;
import kalucki.przemyslaw.unt.MainApplication;

/**
 * Created by prka on 08.11.17.
 */

public class BonjourBrowserSettingsDialog extends BaseDialogFragment
{
	public BonjourBrowserSettingsDialog()
	{
	}

	public static BonjourBrowserSettingsDialog newInstance()
	{
		return new BonjourBrowserSettingsDialog();
	}

	@Override
	protected void injectDependencies(MainApplication application)
	{
		application.getBonjourBrowserSubComponent().inject(this);
	}
}
