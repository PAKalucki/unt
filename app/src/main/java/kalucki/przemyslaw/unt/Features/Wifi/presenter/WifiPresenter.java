package kalucki.przemyslaw.unt.Features.Wifi.presenter;

import kalucki.przemyslaw.unt.Base.BasePresenter;
import kalucki.przemyslaw.unt.Features.Wifi.ui.WifiView;

/**
 * Created by prka on 16.11.17.
 */

public interface WifiPresenter extends BasePresenter<WifiView>
{
	void start();
	void stop();
}
