package kalucki.przemyslaw.unt.Features.Ping;

/**
 * Created by prka on 17.10.17.
 */

public final class TCPResult
{
	public final boolean success;
	public final long time;

	public TCPResult(boolean success, long time)
	{
		this.success = success;
		this.time = time;
	}

	@Override
	public String toString()
	{
		return "TCPResult{" +
				"success=" + success +
				", time=" + time +
				'}';
	}
}
