package kalucki.przemyslaw.unt.Features.BonjourBrowser.dagger;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import de.mannodermaus.rxbonjour.RxBonjour;
import de.mannodermaus.rxbonjour.drivers.jmdns.JmDNSDriver;
import de.mannodermaus.rxbonjour.platforms.android.AndroidPlatform;
import kalucki.przemyslaw.unt.Features.BonjourBrowser.presenter.BonjourBrowserPresenter;
import kalucki.przemyslaw.unt.Features.BonjourBrowser.presenter.BonjourBrowserPresenterImpl;

/**
 * Created by prka on 16.11.17.
 */

@Module
public class BonjourBrowserModule
{
	@Provides
	@BonjourBrowser
	BonjourBrowserPresenter provideBonjourBrowserPresenter(BonjourBrowserPresenterImpl presenter)
	{
		return presenter;
	}

	@Provides
	@BonjourBrowser
	RxBonjour provideRxBonjour(Context context)
	{
		return new RxBonjour.Builder()
				.platform(AndroidPlatform.create(context))
				.driver(JmDNSDriver.create())
				.create();
	}
}
