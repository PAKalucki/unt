package kalucki.przemyslaw.unt.Features.PortScanner;


import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;

import io.reactivex.Observable;

/**
 * Created by prka on 09.10.17.
 */

public class PortScanner
{
	public Observable<PortScanResult> getOutput(String ip, int startPort, int endPort)
	{
		return Observable.create(subscriber->
		                         {
			                         for(int currentPort = startPort; currentPort < endPort; currentPort++)
			                         {
				                         try(Socket socket = new Socket())
				                         {
					                         SocketAddress address = new InetSocketAddress(ip, currentPort);
					                         socket.connect(address, 200);
					                         subscriber.onNext(new PortScanResult(true, currentPort));
				                         }
				                         catch(UnknownHostException e)
				                         {
					                         subscriber.onError(e);
				                         }
				                         catch(IOException e)
				                         {
					                         subscriber.onNext(new PortScanResult(false, currentPort));
				                         }
			                         }
		                         });

	}
}
