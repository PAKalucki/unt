package kalucki.przemyslaw.unt.Features.Ping.dagger;

import dagger.Module;
import dagger.Provides;
import kalucki.przemyslaw.unt.Features.Ping.presenter.PingPresenter;
import kalucki.przemyslaw.unt.Features.Ping.presenter.PingPresenterImpl;

/**
 * Created by prka on 16.11.17.
 */

@Module
public class PingModule
{
	@Provides
	@Ping
	PingPresenter providePingPresenter(PingPresenterImpl presenter)
	{
		return presenter;
	}
}
