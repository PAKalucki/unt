package kalucki.przemyslaw.unt.Features.UPnPScan.presenter;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import kalucki.przemyslaw.unt.Features.UPnPScan.ui.UPnPScanView;
import kalucki.przemyslaw.unt.Utils.SchedulerProvider;

/**
 * Created by prka on 08.11.17.
 */

public class UPnPScanPresenterImpl implements UPnPScanPresenter
{
	private WeakReference<UPnPScanView> view;
	private SchedulerProvider scheduler;

	@Inject
	public UPnPScanPresenterImpl(SchedulerProvider scheduler)
	{
		this.scheduler = scheduler;
	}

	@Override
	public void bind(UPnPScanView view)
	{
		this.view = new WeakReference<>(view);
	}

	@Override
	public void unbind()
	{
		view.clear();
		view = null;
	}

	@Override
	public void start()
	{

	}

	@Override
	public void stop()
	{

	}
}
