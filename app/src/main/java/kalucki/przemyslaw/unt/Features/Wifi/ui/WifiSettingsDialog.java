package kalucki.przemyslaw.unt.Features.Wifi.ui;

import kalucki.przemyslaw.unt.Base.BaseDialogFragment;
import kalucki.przemyslaw.unt.MainApplication;

/**
 * Created by prka on 08.11.17.
 */

public class WifiSettingsDialog extends BaseDialogFragment
{
	public WifiSettingsDialog()
	{

	}

	public static WifiSettingsDialog newInstance()
	{
		return new WifiSettingsDialog();
	}

	@Override
	protected void injectDependencies(MainApplication application)
	{
		application.getWifiSubComponent().inject(this);
	}
}
