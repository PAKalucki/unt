package kalucki.przemyslaw.unt.Features.DNSLookUp;

/**
 * Created by prka on 24.11.17.
 */

public final class DNSQuery
{
	String address;
	String[] serverNames;
	int type;
	int dclass;

	public DNSQuery(String address, String[] serverNames, int type, int dclass)
	{
		this.address = address;
		this.serverNames = serverNames;
		this.type = type;
		this.dclass = dclass;
	}
}
