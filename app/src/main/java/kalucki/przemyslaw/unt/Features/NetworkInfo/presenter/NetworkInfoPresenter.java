package kalucki.przemyslaw.unt.Features.NetworkInfo.presenter;

import kalucki.przemyslaw.unt.Base.BasePresenter;
import kalucki.przemyslaw.unt.Features.NetworkInfo.ui.NetworkInfoView;

/**
 * Created by prka on 16.11.17.
 */

public interface NetworkInfoPresenter extends BasePresenter<NetworkInfoView>
{
	void start();
}
