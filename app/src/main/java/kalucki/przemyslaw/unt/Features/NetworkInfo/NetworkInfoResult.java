package kalucki.przemyslaw.unt.Features.NetworkInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by prka on 21.11.17.
 */

public class NetworkInfoResult
{
	private String type = "";
	private String bssidValue = "";
	private String ipAddress = "";
	private String ipv6Address = "";
	private String speed = "";
	private String macAddress = "";
	private String broadcastAddress = "";
	private String rssiValue = "";
	private String ssidValue = "";
	private String dnsValue1 = "";
	private String dnsValue2 = "";
	private String gateway = "";
	private String leaseDuration = "";
	private String netmask = "";
	private String dhcpServer = "";

	public void setType(String type)
	{
		this.type = type;
	}

	public void setBssidValue(String bssidValue)
	{
		this.bssidValue = bssidValue;
	}

	public void setIpAddress(String ipAddress)
	{
		this.ipAddress = ipAddress;
	}

	public void setIpv6Address(String ipv6Address)
	{
		this.ipv6Address = ipv6Address;
	}

	public void setSpeed(String speed)
	{
		this.speed = speed;
	}

	public void setMacAddress(String macAddress)
	{
		this.macAddress = macAddress;
	}

	public void setBroadcastAddress(String broadcastAddress)
	{
		this.broadcastAddress = broadcastAddress;
	}

	public void setRssiValue(String rssiValue)
	{
		this.rssiValue = rssiValue;
	}

	public void setSsidValue(String ssidValue)
	{
		this.ssidValue = ssidValue;
	}

	public void setDnsValue1(String dnsValue1)
	{
		this.dnsValue1 = dnsValue1;
	}

	public void setDnsValue2(String dnsValue2)
	{
		this.dnsValue2 = dnsValue2;
	}

	public void setGateway(String gateway)
	{
		this.gateway = gateway;
	}

	public void setLeaseDuration(String leaseDuration)
	{
		this.leaseDuration = leaseDuration;
	}

	public void setNetmask(String netmask)
	{
		this.netmask = netmask;
	}

	public void setDhcpServer(String dhcpServer)
	{
		this.dhcpServer = dhcpServer;
	}

	public String getType()
	{
		return type;
	}

	public String getBssidValue()
	{
		return bssidValue;
	}

	public String getIpAddress()
	{
		return ipAddress;
	}

	public String getIpv6Address()
	{
		return ipv6Address;
	}

	public String getSpeed()
	{
		return speed;
	}

	public String getMacAddress()
	{
		return macAddress;
	}

	public String getBroadcastAddress()
	{
		return broadcastAddress;
	}

	public String getRssiValue()
	{
		return rssiValue;
	}

	public String getSsidValue()
	{
		return ssidValue;
	}

	public String getDnsValue1()
	{
		return dnsValue1;
	}

	public String getDnsValue2()
	{
		return dnsValue2;
	}

	public String getGateway()
	{
		return gateway;
	}

	public String getLeaseDuration()
	{
		return leaseDuration;
	}

	public String getNetmask()
	{
		return netmask;
	}

	public String getDhcpServer()
	{
		return dhcpServer;
	}

	public List<String> getAll()
	{
		ArrayList<String> list = new ArrayList<>();

		list.add(type);
		list.add(ssidValue);
		list.add(bssidValue);
		list.add(rssiValue);
		list.add(ipAddress);
		list.add(ipv6Address);
		list.add(macAddress);
		list.add(netmask);
		list.add(broadcastAddress);
		list.add(dnsValue1);
		list.add(dnsValue2);
		list.add(dhcpServer);
		list.add(gateway);
		list.add(speed);
		list.add(leaseDuration);

		return list;
	}
}
