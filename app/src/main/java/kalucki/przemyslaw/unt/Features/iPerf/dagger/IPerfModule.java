package kalucki.przemyslaw.unt.Features.iPerf.dagger;

import dagger.Module;
import dagger.Provides;
import kalucki.przemyslaw.unt.Features.iPerf.presenter.IPerfPresenter;
import kalucki.przemyslaw.unt.Features.iPerf.presenter.IPerfPresenterImpl;

/**
 * Created by prka on 16.11.17.
 */

@Module
public class IPerfModule
{
	@Provides
	@IPerf
	IPerfPresenter provideIPerfPresenter(IPerfPresenterImpl presenter)
	{
		return presenter;
	}
}
