package kalucki.przemyslaw.unt.Features.TraceRoute.presenter;

import android.util.Log;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import kalucki.przemyslaw.unt.Features.Ping.ICMPPing;
import kalucki.przemyslaw.unt.Features.Ping.PingSettings;
import kalucki.przemyslaw.unt.Features.TraceRoute.TraceRouteSettings;
import kalucki.przemyslaw.unt.Features.TraceRoute.ui.TraceRouteView;
import kalucki.przemyslaw.unt.Utils.PingOutputParser;
import kalucki.przemyslaw.unt.Utils.SchedulerProvider;

/**
 * Created by prka on 22.09.17.
 * <p>
 * Traceroute implementation using ICMP pings, increase TTL in loop getting addresses of each hops to get tracerouteText-like function
 * <p>
 * prka@mobica-R50:~/workspace/android/UNT$ ping -c 1 -t 1 www.google.com
 * PING www.google.com (172.217.20.164) 56(84) bytes of data.
 * From gateway (192.168.135.161) icmp_seq=1 Time to live exceeded
 * <p>
 * --- www.google.com ping statistics ---
 * 1 packets transmitted, 0 received, +1 errors, 100% packet loss, time 0ms
 * <p>
 * prka@mobica-R50:~/workspace/android/UNT$ ping -c 1 -t 2 www.google.com
 * PING www.google.com (172.217.20.164) 56(84) bytes of data.
 * From 10.0.66.101 icmp_seq=1 Time to live exceeded
 * <p>
 * --- www.google.com ping statistics ---
 * 1 packets transmitted, 0 received, +1 errors, 100% packet loss, time 0ms
 * <p>
 * ping -c 1 -t 3 www.google.com
 * PING www.google.com (172.217.20.164) 56(84) bytes of data.
 * From 91-224-118-65.fttx.telefonserwis.pl (91.224.118.65) icmp_seq=1 Time to live exceeded
 * <p>
 * --- www.google.com ping statistics ---
 * 1 packets transmitted, 0 received, +1 errors, 100% packet loss, time 0ms
 * <p>
 * ping -c 1 -t 4 www.google.com
 * PING www.google.com (172.217.20.164) 56(84) bytes of data.
 * From 91-224-118-241.fttx.telefonserwis.pl (91.224.118.241) icmp_seq=1 Time to live exceeded
 * <p>
 * --- www.google.com ping statistics ---
 * 1 packets transmitted, 0 received, +1 errors, 100% packet loss, time 0ms
 * <p>
 * ping -c 1 -t 5 www.google.com
 * PING www.google.com (172.217.20.164) 56(84) bytes of data.
 * From 195.149.232.241 icmp_seq=1 Time to live exceeded
 * <p>
 * --- www.google.com ping statistics ---
 * 1 packets transmitted, 0 received, +1 errors, 100% packet loss, time 0ms
 * <p>
 * ping -c 1 -t 6 www.google.com
 * PING www.google.com (172.217.20.164) 56(84) bytes of data.
 * From 72.14.197.128 icmp_seq=1 Time to live exceeded
 * <p>
 * --- www.google.com ping statistics ---
 * 1 packets transmitted, 0 received, +1 errors, 100% packet loss, time 0ms
 * <p>
 * ping -c 1 -t 7 www.google.com
 * PING www.google.com (172.217.20.164) 56(84) bytes of data.
 * From 108.170.250.193 icmp_seq=1 Time to live exceeded
 * <p>
 * --- www.google.com ping statistics ---
 * 1 packets transmitted, 0 received, +1 errors, 100% packet loss, time 0ms
 * <p>
 * ping -c 1 -t 8 www.google.com
 * PING www.google.com (172.217.20.164) 56(84) bytes of data.
 * From 216.239.41.165 icmp_seq=1 Time to live exceeded
 * <p>
 * --- www.google.com ping statistics ---
 * 1 packets transmitted, 0 received, +1 errors, 100% packet loss, time 0ms
 * <p>
 * ping -c 1 -t 9 www.google.com
 * PING www.google.com (172.217.20.164) 56(84) bytes of data.
 * 64 bytes from waw02s07-in-f4.1e100.net (172.217.20.164): icmp_seq=1 ping_ttl_editText=55 time=9.49 ms
 * <p>
 * --- www.google.com ping statistics ---
 * 1 packets transmitted, 1 received, 0% packet loss, time 0ms
 * rtt min/avg/max/mdev = 9.497/9.497/9.497/0.000 ms
 * ping -c 1 -t 10 www.google.com
 * PING www.google.com (172.217.20.164) 56(84) bytes of data.
 * 64 bytes from waw02s07-in-f4.1e100.net (172.217.20.164): icmp_seq=1 ping_ttl_editText=55 time=8.08 ms
 * <p>
 * --- www.google.com ping statistics ---
 * 1 packets transmitted, 1 received, 0% packet loss, time 0ms
 * rtt min/avg/max/mdev = 8.083/8.083/8.083/0.000 ms
 */

public class TraceRoutePresenterImpl implements TraceRoutePresenter
{
	private boolean isStarted = false;
	private CompositeDisposable compositeDisposable = new CompositeDisposable();
	private ICMPPing icmpPing = new ICMPPing();
	private PingSettings pingSettings = new PingSettings();
	private WeakReference<TraceRouteView> view;
	private SchedulerProvider scheduler;

	@Inject
	public TraceRoutePresenterImpl(SchedulerProvider scheduler)
	{
		this.scheduler = scheduler;
	}

	@Override
	public void bind(TraceRouteView view)
	{
		this.view = new WeakReference<>(view);
	}

	@Override
	public void unbind()
	{
		view.clear();
		view = null;
	}

	@Override
	public void start(String address)
	{
		TraceRouteSettings traceRouteSettings = view.get().loadSettings();

		pingSettings.count = traceRouteSettings.count;
		pingSettings.timeout = traceRouteSettings.timeout;
		pingSettings.ttl = 1;
		icmpPing.setSettings(pingSettings);
		icmpPing.setAddress(address);

		view.get().start();

		compositeDisposable.add(
				icmpPing.getOutput()
				        .subscribeOn(scheduler.backgroundScheduler())
				        .observeOn(scheduler.mainScheduler())
				        .doOnNext(value->
				                  {
					                  if(view != null)//TODO caching when view is null during orientation change
					                  {
						                  if(value.success && !value.output.isEmpty())
						                  {
							                  Log.i("TRACEROUTE: ", value.output);
							                  String output = String.valueOf(pingSettings.ttl) + " " + PingOutputParser
									                  .extractPingTargetAddressIp(value.output) + PingOutputParser.extractPingHostname(value.output);
							                  view.get().update(output);
						                  }
					                  }
				                  })
				        .repeatUntil(()->
				                     {
					                     if(pingSettings.ttl < traceRouteSettings.hops)
					                     {
						                     pingSettings.ttl++;
						                     return false;
					                     }
					                     else
					                     {
						                     return true;
					                     }
				                     })
				        .doOnComplete(()->
				                      {
					                      if(view != null)
					                      {
						                      view.get().stop();
					                      }
				                      })
				        .subscribe()
		);
	}

	@Override
	public void stop()
	{
		isStarted = false;
		icmpPing.stop();
		view.get().stop();

		if(compositeDisposable != null && !compositeDisposable.isDisposed())
		{
			compositeDisposable.clear();
		}
	}

	public boolean isStarted()
	{
		return isStarted;
	}
}
