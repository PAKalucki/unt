package kalucki.przemyslaw.unt.Features.Lan.presenter;

import kalucki.przemyslaw.unt.Base.BasePresenter;
import kalucki.przemyslaw.unt.Features.Lan.ui.LanView;

/**
 * Created by prka on 16.11.17.
 */

public interface LanPresenter extends BasePresenter<LanView>
{
	void start(String s);
	void stop();
}
