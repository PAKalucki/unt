package kalucki.przemyslaw.unt.Features.UPnPScan.dagger;

import dagger.Module;
import dagger.Provides;
import kalucki.przemyslaw.unt.Features.UPnPScan.presenter.UPnPScanPresenter;
import kalucki.przemyslaw.unt.Features.UPnPScan.presenter.UPnPScanPresenterImpl;

/**
 * Created by prka on 16.11.17.
 */

@Module
public class UPnPScanModule
{
	@Provides
	@UPnP
	UPnPScanPresenter provideUPnPScanPresenter(UPnPScanPresenterImpl presenter)
	{
		return presenter;
	}
}
