package kalucki.przemyslaw.unt.Features.TraceRoute.dagger;

import dagger.Module;
import dagger.Provides;
import kalucki.przemyslaw.unt.Features.TraceRoute.presenter.TraceRoutePresenter;
import kalucki.przemyslaw.unt.Features.TraceRoute.presenter.TraceRoutePresenterImpl;

/**
 * Created by prka on 16.11.17.
 */

@Module
public class TraceRouteModule
{
	@Provides
	@Trace
	TraceRoutePresenter provideTraceRoutePresenter(TraceRoutePresenterImpl presenter)
	{
		return presenter;
	}

}
