package kalucki.przemyslaw.unt.Features.Watcher.presenter;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import kalucki.przemyslaw.unt.Features.Watcher.ui.WatcherView;
import kalucki.przemyslaw.unt.Utils.SchedulerProvider;

/**
 * Created by prka on 08.11.17.
 */

public class WatcherPresenterImpl implements WatcherPresenter
{
	private WeakReference<WatcherView> view;
	SchedulerProvider scheduler;

	@Inject
	public WatcherPresenterImpl(SchedulerProvider scheduler)
	{
		this.scheduler = scheduler;
	}

	@Override
	public void bind(WatcherView view)
	{
		this.view = new WeakReference<>(view);
	}

	@Override
	public void unbind()
	{
		view.clear();
		view = null;
	}

	@Override
	public void start()
	{}

	@Override
	public void stop()
	{}
}
