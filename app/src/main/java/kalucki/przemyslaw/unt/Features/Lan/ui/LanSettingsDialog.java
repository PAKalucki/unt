package kalucki.przemyslaw.unt.Features.Lan.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import butterknife.Unbinder;
import kalucki.przemyslaw.unt.Base.BaseDialogFragment;
import kalucki.przemyslaw.unt.MainApplication;
import kalucki.przemyslaw.unt.R;

/**
 * Created by prka on 28.09.17.
 */

public class LanSettingsDialog extends BaseDialogFragment
{
	@BindString(R.string.lan_mode_key) String lanModeKey;
	@BindString(R.string.lan_sort_key) String lanSortKey;
	@BindView (R.id.lan_mode_spinner) Spinner spinnerMode;
	@BindView (R.id.lan_sort_spinner) Spinner spinnerSortBy;
	@Inject SharedPreferences preferences;
	Unbinder unbinder;

	public LanSettingsDialog()
	{
		// Required empty public constructor
	}

	public static LanSettingsDialog newInstance()
	{
		return new LanSettingsDialog();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.lan_settings, container, false);

		unbinder = ButterKnife.bind(this, view);

		ArrayAdapter<CharSequence> arrayAdapterSortBy = ArrayAdapter
				.createFromResource(view.getContext(), R.array.lan_settings_spinner1, android.R.layout.simple_spinner_item);
		arrayAdapterSortBy.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerSortBy.setAdapter(arrayAdapterSortBy);

		ArrayAdapter<CharSequence> arrayAdapterModeSelection = ArrayAdapter
				.createFromResource(view.getContext(), R.array.lan_settings_spinner2, android.R.layout.simple_spinner_item);
		arrayAdapterModeSelection.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerMode.setAdapter(arrayAdapterModeSelection);

		return view;
	}

	@Override
	public void onDestroyView()
	{
		super.onDestroyView();
		unbinder.unbind();
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle bundle)
	{
		super.onViewCreated(view, bundle);

		spinnerSortBy.setSelection(preferences.getInt(lanSortKey, 0));
		spinnerMode.setSelection(preferences.getInt(lanModeKey, 0));
	}

	private void readAndSavePreferences()
	{
		SharedPreferences.Editor editor = preferences.edit();

		editor.putInt(lanSortKey, spinnerSortBy.getSelectedItemPosition());
		editor.putInt(lanModeKey, spinnerMode.getSelectedItemPosition());

		editor.apply();
		dismiss();
	}

	private void cancel()
	{
		dismiss();
	}

	@OnClick ({R.id.lan_ok_button, R.id.lan_cancel_button})
	public void onClick(View view)
	{
		switch(view.getId())
		{
			case R.id.lan_ok_button:
				readAndSavePreferences();
				break;
			case R.id.lan_cancel_button:
				cancel();
				break;
		}
	}

	@OnItemSelected ({R.id.lan_sort_spinner, R.id.lan_mode_spinner})
	public void onItemSelected(View view, int position)
	{
		switch(view.getId())
		{
			case R.id.lan_sort_spinner:
				spinnerSortBy.setSelection(position);
				break;
			case R.id.lan_mode_spinner:
				spinnerMode.setSelection(position);
				break;
		}
	}

	@Override
	protected void injectDependencies(MainApplication application)
	{
		application.getLanSubComponent().inject(this);
	}
}
