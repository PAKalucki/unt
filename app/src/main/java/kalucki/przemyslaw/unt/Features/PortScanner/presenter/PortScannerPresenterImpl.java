package kalucki.przemyslaw.unt.Features.PortScanner.presenter;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import kalucki.przemyslaw.unt.Features.PortScanner.PortScanner;
import kalucki.przemyslaw.unt.Features.PortScanner.ui.PortScannerView;
import kalucki.przemyslaw.unt.Utils.SchedulerProvider;

/**
 * Created by prka on 09.10.17.
 */

public class PortScannerPresenterImpl implements PortScannerPresenter
{
	private WeakReference<PortScannerView> view;
	private PortScanner portScanner = new PortScanner();
	private SchedulerProvider scheduler;

	@Inject
	public PortScannerPresenterImpl(SchedulerProvider scheduler)
	{
		this.scheduler = scheduler;
	}

	@Override
	public void bind(PortScannerView view)
	{
		this.view = new WeakReference<>(view);
	}

	@Override
	public void unbind()
	{
		view.clear();
		view = null;
	}

	@Override
	public void start(String address)
	{

	}

	@Override
	public void stop()
	{

	}
}
