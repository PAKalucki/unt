package kalucki.przemyslaw.unt.Features.UsageStats.presenter;

import kalucki.przemyslaw.unt.Base.BasePresenter;
import kalucki.przemyslaw.unt.Features.UsageStats.ui.UsageStatsView;

/**
 * Created by prka on 16.11.17.
 */

public interface UsageStatsPresenter extends BasePresenter<UsageStatsView>
{
	void start();
	void stop();
	void refresh();
}
