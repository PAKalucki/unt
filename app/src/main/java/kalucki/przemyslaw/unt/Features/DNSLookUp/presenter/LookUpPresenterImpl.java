package kalucki.przemyslaw.unt.Features.DNSLookUp.presenter;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import kalucki.przemyslaw.unt.Features.DNSLookUp.DNSLookUp;
import kalucki.przemyslaw.unt.Features.DNSLookUp.DNSQuery;
import kalucki.przemyslaw.unt.Features.DNSLookUp.LookUpSettings;
import kalucki.przemyslaw.unt.Features.DNSLookUp.ui.LookUpView;
import kalucki.przemyslaw.unt.Utils.SchedulerProvider;

/**
 * Created by prka on 08.11.17.
 */

public class LookUpPresenterImpl implements LookUpPresenter
{
	@Inject SchedulerProvider scheduler;
	private CompositeDisposable compositeDisposable = new CompositeDisposable();
	private DNSLookUp dnsLookUp = new DNSLookUp();
	private LookUpSettings settings;
	private WeakReference<LookUpView> view;

	@Inject
	public LookUpPresenterImpl(SchedulerProvider scheduler)
	{
		this.scheduler = scheduler;
	}

	@Override
	public void bind(LookUpView view)
	{
		this.view = new WeakReference<>(view);
	}

	@Override
	public void unbind()
	{
		view.clear();
		view = null;
	}

	@Override
	public void start(DNSQuery query)
	{
		view.get().start();
		settings = view.get().loadSettings();

		compositeDisposable.add(
				dnsLookUp.getOutput(query)
				         .subscribeOn(scheduler.backgroundScheduler())
				         .observeOn(scheduler.mainScheduler())
				         .subscribe(output->
				                    {
					                    if(view != null)
					                    {
						                    view.get().update(output);
					                    }
				                    },
				                    Throwable::printStackTrace,
				                    ()->
				                    {
					                    if(view != null)
					                    {
						                    view.get().stop();
					                    }
				                    }
				         ));
	}

	@Override
	public void stop()
	{
		if(compositeDisposable != null && !compositeDisposable.isDisposed())
		{
			compositeDisposable.clear();
		}
	}
}
