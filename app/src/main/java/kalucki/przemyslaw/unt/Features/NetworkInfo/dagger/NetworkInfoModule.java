package kalucki.przemyslaw.unt.Features.NetworkInfo.dagger;

import dagger.Module;
import dagger.Provides;
import kalucki.przemyslaw.unt.Features.NetworkInfo.presenter.NetworkInfoPresenter;
import kalucki.przemyslaw.unt.Features.NetworkInfo.presenter.NetworkInfoPresenterImpl;

/**
 * Created by prka on 16.11.17.
 */

@Module
public class NetworkInfoModule
{
	@Provides
	@NetworkInfo
	NetworkInfoPresenter provideNetworkInfoPresenter(NetworkInfoPresenterImpl presenter)
	{
		return presenter;
	}
}
