package kalucki.przemyslaw.unt.Features.UsageStats.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import javax.annotation.Nullable;
import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import butterknife.Unbinder;
import kalucki.przemyslaw.unt.Base.BaseDialogFragment;
import kalucki.przemyslaw.unt.MainApplication;
import kalucki.przemyslaw.unt.R;
import kalucki.przemyslaw.unt.Utils.TextDrawable;

/**
 * Created by prka on 31.10.17.
 */

public class UsageStatsSettingsDialog extends BaseDialogFragment
{
	@BindString (R.string.usage_graphpooling_key) String usageGraphPoolingKey;
	@BindString (R.string.usage_graph_pooling_string) String usageGraphPoolingString;
	@BindString (R.string.usage_sort_key) String usageSortKey;
	@BindString (R.string.usage_tablepooling_key) String usageTablePoolingKey;
	@BindString (R.string.usage_table_pooling_string) String usageTablePoolingString;
	@BindView (R.id.usage_graphInterval_editText) EditText editTextGraphInterval;
	@BindView (R.id.usage_mode_editText) Spinner spinnerSortBy;
	@BindView (R.id.usage_tableInterval_editText) EditText editTextTableInterval;
	@Inject SharedPreferences preferences;
	Unbinder unbinder;

	public UsageStatsSettingsDialog()
	{

	}

	public static UsageStatsSettingsDialog newInstance()
	{
		return new UsageStatsSettingsDialog();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.usage_settings, container, false);

		unbinder = ButterKnife.bind(this, view);

		return view;
	}

	@Override
	public void onDestroyView()
	{
		super.onDestroyView();
		unbinder.unbind();
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle bundle)
	{
		super.onViewCreated(view, bundle);

		ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(
				view.getContext(), R.array.usage_mode_spinner, android.R.layout.simple_spinner_item);
		arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		editTextGraphInterval.setCompoundDrawablesWithIntrinsicBounds(new TextDrawable(usageGraphPoolingString), null, null, null);
		editTextTableInterval.setCompoundDrawablesWithIntrinsicBounds(new TextDrawable(usageTablePoolingString), null, null, null);

		editTextGraphInterval.setCompoundDrawablePadding(usageGraphPoolingString.length() * 10);
		editTextTableInterval.setCompoundDrawablePadding(usageTablePoolingString.length() * 10);

		spinnerSortBy.setSelection(preferences.getInt(usageSortKey, 0));

		editTextTableInterval.setText(String.valueOf(preferences.getLong(usageGraphPoolingKey, 1000)));
		editTextTableInterval.setText(String.valueOf(preferences.getLong(usageTablePoolingKey, 1000)));
	}

	private void readAndSavePreferences()
	{
		SharedPreferences.Editor editor = preferences.edit();

		editor.putLong(usageGraphPoolingKey, Long.valueOf(editTextGraphInterval.getText().toString()));
		editor.putLong(usageTablePoolingKey, Long.valueOf(editTextTableInterval.getText().toString()));
		editor.putInt(usageSortKey, spinnerSortBy.getSelectedItemPosition());

		editor.apply();
		dismiss();
	}

	private void cancel()
	{
		dismiss();
	}

	private void restoreDefaultSettings()
	{
		SharedPreferences.Editor editor = preferences.edit();

		editor.putLong(usageGraphPoolingKey, 1000);
		editor.putLong(usageTablePoolingKey, 1000);
		editor.putInt(usageSortKey, 0);

		editor.apply();
		dismiss();
	}

	@OnClick ({R.id.usage_ok_button, R.id.usage_cancel_button, R.id.usage_restore_button})
	public void onClick(View view)
	{
		switch(view.getId())
		{
			case R.id.usage_ok_button:
				readAndSavePreferences();
				break;
			case R.id.usage_cancel_button:
				cancel();
				break;
			case R.id.usage_restore_button:
				restoreDefaultSettings();
				break;
		}
	}

	@OnItemSelected ({R.id.usage_mode_editText})
	public void onItemSelected(int position)
	{
		spinnerSortBy.setSelection(position);
	}

	@Override
	protected void injectDependencies(MainApplication application)
	{
		application.getUsageStatsSubComponent().inject(this);
	}
}
