package kalucki.przemyslaw.unt.Features.UsageStats.ui;


import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import kalucki.przemyslaw.unt.Adapter.ListAdapter.ListAdapter;
import kalucki.przemyslaw.unt.Adapter.ListAdapter.ListItem;
import kalucki.przemyslaw.unt.Base.BaseFragment;
import kalucki.przemyslaw.unt.Features.UsageStats.AppDataContainer;
import kalucki.przemyslaw.unt.Features.UsageStats.UsageStatistics;
import kalucki.przemyslaw.unt.Features.UsageStats.presenter.UsageStatsPresenter;
import kalucki.przemyslaw.unt.Features.UsageStats.UsageStatsSettings;
import kalucki.przemyslaw.unt.MainApplication;
import kalucki.przemyslaw.unt.R;
import kalucki.przemyslaw.unt.Utils.UsageUtils;

public class UsageStatsFragment extends BaseFragment implements UsageStatsView
{
	@BindString (R.string.usage_graphpooling_key) String usageGraphPoolingKey;
	@BindString (R.string.usage_sort_key) String usageSortKey;
	@BindString (R.string.usage_tablepooling_key) String usageTablePoolingKey;
	@BindView (R.id.usage_list_recyclerview) RecyclerView recyclerView;
	@BindView (R.id.usage_updown_chart) LineChart chart;
	@BindView (R.id.usage_swiperefresh_layout) SwipeRefreshLayout swipeRefreshLayout;
	@Inject SharedPreferences preferences;
	@Inject UsageStatsPresenter presenter;
	private ListAdapter adapter;
	private List<ListItem> listItems = new ArrayList<>();
	private Unbinder unbinder;

	public UsageStatsFragment()
	{
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.fragment_usage, container, false);
		unbinder = ButterKnife.bind(this, view);
		return view;
	}

	@Override
	public void onDestroyView()
	{
		super.onDestroyView();
		unbinder.unbind();
	}

	@Override
	public void onStart()
	{
		super.onStart();
		View view = getView();

		if(view != null)
		{
			presenter.bind(this);

			recyclerView.setLayoutManager(new GridLayoutManager(view.getContext(), 1, GridLayoutManager.VERTICAL, false));
			adapter = new ListAdapter(listItems);
			recyclerView.setAdapter(adapter);

			presenter.start();

			swipeRefreshLayout.setOnRefreshListener(()->presenter.refresh());
		}
	}

	@Override
	public void onStop()
	{
		super.onStop();
		presenter.unbind();
	}

	@Override
	public void start()
	{
		prepareChart();
	}

	@Override
	public void stop()
	{
		if(swipeRefreshLayout.isRefreshing())
		{
			swipeRefreshLayout.setRefreshing(false);
		}
	}

	@Override
	public void updateGraph(Float value, int dataSetIndex)
	{
		if(value != null)
		{
			LineData data = chart.getData();

			if(data != null)
			{
				ILineDataSet set = data.getDataSetByIndex(dataSetIndex);

				if(set == null)
				{
					set = createSet(dataSetIndex);
					data.addDataSet(set);
				}

				if(dataSetIndex == 0)
				{
					set.setLabel("Download: " + UsageUtils.formatUsageOutput(value.longValue()) + "\\s ");
				}
				else
				{
					set.setLabel("Upload: " + UsageUtils.formatUsageOutput(value.longValue()) + "\\s ");
				}

				data.addEntry(new Entry(set.getEntryCount(), value), dataSetIndex);
				data.notifyDataChanged();

				chart.notifyDataSetChanged();
				chart.setVisibleXRangeMaximum(10);
				chart.moveViewToX(data.getEntryCount());
			}
		}
	}

	@Override
	public void updateTable(AppDataContainer data, String newValue)
	{
		listItems.add(new ListItem(data.icon, data.name + newValue));
		adapter.notifyDataSetChanged();
		recyclerView.scrollToPosition(listItems.size() -1);
	}

	@Override
	public void clear()
	{
		if(adapter != null)
		{
			adapter.clearAll();
		}
	}

	@Override
	public UsageStatistics getUsage()
	{
		return new UsageStatistics(getContext().getApplicationContext());
	}

	@Override
	public UsageStatsSettings loadSettings()
	{
		UsageStatsSettings settings = new UsageStatsSettings();

		settings.poolingIntervalGraph = preferences.getLong(usageGraphPoolingKey, 1000);
		settings.poolingIntervalTable = preferences.getLong(usageTablePoolingKey, 1000);
		settings.sortMode = preferences.getInt(usageSortKey, 0);

		return settings;
	}

	private void prepareChart()
	{
		if(chart != null)
		{
			chart.getDescription().setEnabled(false);

			chart.setPinchZoom(false);
			chart.setDrawBorders(false);
			chart.setDrawGridBackground(false);
			chart.setDragEnabled(false);
			chart.setScaleEnabled(false);
			chart.setHighlightPerDragEnabled(false);
			chart.setHighlightPerTapEnabled(false);

			chart.getAxisLeft().setEnabled(false);
			chart.getAxisRight().setEnabled(false);

			chart.getXAxis().setEnabled(true);
			chart.getXAxis().setDrawLabels(false);
			chart.getXAxis().setDrawGridLines(false);

			LineData data = new LineData();
			chart.setData(data);
		}
	}

	private LineDataSet createSet(int flag)
	{
		LineDataSet set = new LineDataSet(null, "");
		set.setAxisDependency(YAxis.AxisDependency.RIGHT);

		if(flag == 0)//TODO this is very crude...
		{
			set.setColor(Color.rgb(0, 10, 100));
		}
		else
		{
			set.setColor(Color.rgb(0, 100, 10));
		}

		set.setDrawCircles(false);
		set.setLineWidth(2f);
		set.setDrawValues(false);
		return set;
	}

	@Override
	protected void injectDependencies(MainApplication application)
	{
		application.getUsageStatsSubComponent().inject(this);
	}
}
