package kalucki.przemyslaw.unt.Features.Ping;

import android.support.annotation.IntRange;

/**
 * Created by prka on 20.09.17.
 */

public class PingSettings//TODO code smell ? Is there a better way to store this ?
{
	//0 icmp, 1 tcp
	@IntRange(from = 0, to = 1)
	public int mode;
	public int count;
	public int interval;
	public int packetsize;
	public int ttl;
	public int timeout;
	public int port;

	public PingSettings()
	{
	}

	public PingSettings(int mode, int count, int interval, int packetsize, int ttl, int timeout, int port)
	{
		this.mode = mode;
		this.count = count;
		this.interval = interval;
		this.packetsize = packetsize;
		this.ttl = ttl;
		this.timeout = timeout;
		this.port = port;
	}
}
