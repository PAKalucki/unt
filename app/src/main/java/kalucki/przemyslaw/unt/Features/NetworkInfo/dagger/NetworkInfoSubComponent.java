package kalucki.przemyslaw.unt.Features.NetworkInfo.dagger;

import dagger.Subcomponent;
import kalucki.przemyslaw.unt.Features.NetworkInfo.ui.NetworkInfoFragment;
import kalucki.przemyslaw.unt.Features.NetworkInfo.ui.NetworkInfoSettingsDialog;

/**
 * Created by prka on 16.11.17.
 */

@NetworkInfo
@Subcomponent(modules = NetworkInfoModule.class)
public interface NetworkInfoSubComponent
{
	void inject(NetworkInfoFragment networkInfoFragment);
	void inject(NetworkInfoSettingsDialog networkInfoSettingsDialog);
}
