package kalucki.przemyslaw.unt.Features.Lan.presenter;

import android.util.Log;

import org.apache.commons.net.util.SubnetUtils;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import kalucki.przemyslaw.unt.Features.Lan.Lan;
import kalucki.przemyslaw.unt.Features.Lan.LanSettings;
import kalucki.przemyslaw.unt.Features.Lan.ui.LanView;
import kalucki.przemyslaw.unt.Utils.SchedulerProvider;

/**
 * Created by prka on 03.10.17.
 */

public class LanPresenterImpl implements LanPresenter
{
	private CompositeDisposable compositeDisposable = new CompositeDisposable();
	private Lan lan = new Lan();
	private LanSettings lanSettings;
	private SchedulerProvider scheduler;
	private WeakReference<LanView> view;

	@Inject
	public LanPresenterImpl(SchedulerProvider scheduler)
	{
		this.scheduler = scheduler;
	}

	@Override
	public void bind(LanView view)
	{
		this.view = new WeakReference<>(view);
	}

	@Override
	public void unbind()
	{
		view.clear();
		view = null;
	}

	@Override
	public void start(String address)
	{
		lanSettings = view.get().loadSettings();
		view.get().start();

		int threadCount = Runtime.getRuntime().availableProcessors();
		Log.i("LAN SCAN THREADS: ", String.valueOf(threadCount));
		SubnetUtils subnetUtils = new SubnetUtils(address, getMask(address));
		String[] ipPool = subnetUtils.getInfo().getAllAddresses();
		String[][] ipPools = splitArray(ipPool, threadCount);

		for(int CURRENT_CHUNK = 0; CURRENT_CHUNK < threadCount; CURRENT_CHUNK++)
		{
			compositeDisposable.add(lan.runICMPScan(ipPools[CURRENT_CHUNK], 200)
			                           .subscribeOn(scheduler.backgroundScheduler())
			                           .observeOn(scheduler.mainScheduler())
			                           .doOnNext(value->
			                                     {
				                                     if(view != null)
				                                     {
					                                     view.get().update(value.toString());
				                                     }
			                                     })
			                           .doOnComplete(()->
			                                         {
				                                         if(view != null)
				                                         {
					                                         view.get().stop();
				                                         }
			                                         })
			                           .subscribe());
		}

//		compositeDisposable.add(
//				(DataParser.parseDataInParallel(Arrays.asList(ipPool), (sublist)->
//				{
//					List<LanResult> scanResults = new ArrayList<>();
//					for(String data : sublist)
//					{
//						boolean success = InetAddress.getByName(data).isReachable(200);
//						scanResults.add(new LanResult(data, success));
//					}
//					return Observable.just(scanResults);
//				}))
//						.subscribeOn(backgroundScheduler)
//						.observeOn(mainScheduler)
//						.doOnNext(value->
//						{
//							for(LanResult adr : value)
//							{
//								view.updateTable(adr.address);
//							}
//						})
//						.subscribe());
	}

	@Override
	public void stop()
	{
		view.get().stop();
		if(compositeDisposable != null && !compositeDisposable.isDisposed())
		{
			compositeDisposable.clear();
		}
	}

	private String[][] splitArray(String[] ipPool, int numberOfSplits)//TODO plz no array of arrays
	{
		String[][] chunks = new String[numberOfSplits][];

		for(int i = 0; i < numberOfSplits; ++i)
		{
			int start = (i * ipPool.length) / numberOfSplits;
			int length = Math.min(ipPool.length - start, ipPool.length / numberOfSplits);

			String[] temp = new String[length];
			System.arraycopy(ipPool, start, temp, 0, length);
			Log.i("SPLIT ARRAY SIZE: ", "ARRAY" + String.valueOf(i) + " " + String.valueOf(temp.length));
			chunks[i] = temp;
		}

		return chunks;
	}

	private String getMask(String address)//TODO IMPLEMENT AND MOVE TO UTILS
	{
		return "255.255.255.0";
	}
}
