package kalucki.przemyslaw.unt.Features.WhoIs.dagger;

import dagger.Subcomponent;
import kalucki.przemyslaw.unt.Features.WhoIs.ui.WhoIsFragment;
import kalucki.przemyslaw.unt.Features.WhoIs.ui.WhoIsSettingsDialog;

/**
 * Created by prka on 16.11.17.
 */

@WhoIs
@Subcomponent(modules = WhoIsModule.class)
public interface WhoIsSubComponent
{
	void inject(WhoIsFragment whoIsFragment);
	void inject(WhoIsSettingsDialog whoIsSettingsDialog);
}
