package kalucki.przemyslaw.unt.Features.Watcher.presenter;

import kalucki.przemyslaw.unt.Base.BasePresenter;
import kalucki.przemyslaw.unt.Features.Watcher.ui.WatcherView;

/**
 * Created by prka on 16.11.17.
 */

public interface WatcherPresenter extends BasePresenter<WatcherView>
{
	void start();
	void stop();
}
