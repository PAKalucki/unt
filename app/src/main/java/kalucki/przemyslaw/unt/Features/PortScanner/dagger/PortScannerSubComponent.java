package kalucki.przemyslaw.unt.Features.PortScanner.dagger;

import dagger.Subcomponent;
import kalucki.przemyslaw.unt.Features.PortScanner.ui.PortScannerFragment;
import kalucki.przemyslaw.unt.Features.PortScanner.ui.PortScannerSettingsDialog;

/**
 * Created by prka on 16.11.17.
 */

@PortScan
@Subcomponent(modules = PortScannerModule.class)
public interface PortScannerSubComponent
{
	void inject(PortScannerFragment portScannerFragment);
	void inject(PortScannerSettingsDialog portScannerSettingsDialog);
}
