package kalucki.przemyslaw.unt.Features.Lan.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.Lazy;
import kalucki.przemyslaw.unt.Adapter.ListAdapter.ListAdapter;
import kalucki.przemyslaw.unt.Adapter.ListAdapter.ListItem;
import kalucki.przemyslaw.unt.Base.BaseFragment;
import kalucki.przemyslaw.unt.Features.Lan.presenter.LanPresenter;
import kalucki.przemyslaw.unt.Features.Lan.LanSettings;
import kalucki.przemyslaw.unt.MainApplication;
import kalucki.przemyslaw.unt.R;
import kalucki.przemyslaw.unt.Utils.IpUtils;

/**
 * Created by prka on 28.09.17.
 */

public class LanFragment extends BaseFragment implements LanView
{
	@BindString (R.string.lan_mode_key) String lanModeKey;
	@BindString (R.string.lan_sort_key) String lanSortKey;
	@BindView (R.id.lan_list_recyclerview) RecyclerView recyclerView;
	@BindView (R.id.lan_swiperefresh_layout) SwipeRefreshLayout swipeRefreshLayout;
	@BindView (R.id.lan_swipetorefresh_textView) TextView swipeToRefreshInfoText;
	@Inject Lazy<LanPresenter> presenter;
	@Inject SharedPreferences preferences;

	private ArrayList<ListItem> outputItems = new ArrayList<>();
	private ListAdapter adapter;
	private boolean isStarted;
	private Unbinder unbinder;

	public LanFragment()
	{

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.fragment_lan, container, false);
		unbinder = ButterKnife.bind(this, view);
		return view;
	}

	@Override
	public void onDestroyView()
	{
		super.onDestroyView();
		unbinder.unbind();
	}

	@Override
	public void onCreate(Bundle bundle)
	{
		if(bundle != null)
		{
			isStarted = bundle.getBoolean(getString(R.string.lan_isStarted_key));
			outputItems = bundle.getParcelableArrayList(getString(R.string.lan_outputlist_key));
		}

		super.onCreate(bundle);
	}

	@Override
	public void onSaveInstanceState(Bundle bundle)
	{
		bundle.putBoolean(getString(R.string.lan_isStarted_key), isStarted);
		bundle.putParcelableArrayList(getString(R.string.lan_outputlist_key), outputItems);

		super.onSaveInstanceState(bundle);
	}

	@Override
	public void onStart()
	{
		super.onStart();

		View view = getView();

		if(view != null)
		{
			presenter.get().bind(this);

			recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
			adapter = new ListAdapter(outputItems);
			recyclerView.setAdapter(adapter);

			swipeRefreshLayout.setOnRefreshListener(()->presenter.get().start(getIp()));
		}
	}

	@Override
	public void onStop()
	{
		super.onStop();
		presenter.get().unbind();
	}

	private String getIp()
	{
		WifiManager wifiManager = (WifiManager) getContext().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
		WifiInfo wifiInfo = wifiManager.getConnectionInfo();
		return IpUtils.intToString(wifiInfo.getIpAddress());
	}
//	private void scan()
//	{
//		SharedPreferences preferences = getActivity().getSharedPreferences(getString(R.string.preference_file), Context.MODE_PRIVATE);
//
//		switch(preferences.getInt(getString(R.string.lan_mode_key), 0))
//		{
//			case 0:
//				pingScan();
//				break;
//			case 1:
//				httpScan();
//				break;
//		}
//	}

	@Override
	public void stop()
	{
		if(swipeRefreshLayout.isRefreshing())
		{
			swipeRefreshLayout.setRefreshing(false);
		}
		isStarted = false;
	}

	@Override
	public void start()
	{
		if(adapter != null)
		{
			adapter.clearAll();
		}
		swipeToRefreshInfoText.setVisibility(View.INVISIBLE);
		isStarted = true;
	}

	@Override
	public void update(String value)
	{
		outputItems.add(new ListItem(null, value));
		adapter.notifyDataSetChanged();
		recyclerView.smoothScrollToPosition(outputItems.size() - 1);
	}

	@Override
	public LanSettings loadSettings()
	{
		LanSettings settings = new LanSettings();

		settings.modeOption = preferences.getInt(lanModeKey, 0);
		settings.sortOption = preferences.getInt(lanSortKey, 0);

		return settings;
	}

	@Override
	protected void injectDependencies(MainApplication application)
	{
		application.getLanSubComponent().inject(this);
	}
}
