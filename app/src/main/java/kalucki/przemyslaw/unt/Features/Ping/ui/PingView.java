package kalucki.przemyslaw.unt.Features.Ping.ui;

import kalucki.przemyslaw.unt.Features.Ping.PingSettings;

/**
 * Created by prka on 02.11.17.
 */

public interface PingView
{
	void start();

	void stop();

	void update(String value);

	PingSettings loadSettings();
}
