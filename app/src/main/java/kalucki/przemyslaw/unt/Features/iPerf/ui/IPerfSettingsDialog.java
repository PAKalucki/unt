package kalucki.przemyslaw.unt.Features.iPerf.ui;

import kalucki.przemyslaw.unt.Base.BaseDialogFragment;
import kalucki.przemyslaw.unt.MainApplication;

/**
 * Created by prka on 08.11.17.
 */

public class IPerfSettingsDialog extends BaseDialogFragment
{
	public IPerfSettingsDialog()
	{
	}

	public static IPerfSettingsDialog newInstance()
	{
		return new IPerfSettingsDialog();
	}

	@Override
	protected void injectDependencies(MainApplication application)
	{
		application.getPerfSubComponent().inject(this);
	}
}
