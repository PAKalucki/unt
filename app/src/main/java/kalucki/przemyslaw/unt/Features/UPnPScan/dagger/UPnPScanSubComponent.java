package kalucki.przemyslaw.unt.Features.UPnPScan.dagger;

import dagger.Subcomponent;
import kalucki.przemyslaw.unt.Features.UPnPScan.ui.UPnPScanFragment;
import kalucki.przemyslaw.unt.Features.UPnPScan.ui.UPnPScanSettingsDialog;

/**
 * Created by prka on 16.11.17.
 */

@UPnP
@Subcomponent(modules = UPnPScanModule.class)
public interface UPnPScanSubComponent
{
	void inject(UPnPScanFragment uPnPScanFragment);
	void inject(UPnPScanSettingsDialog uPnPScanSettingsDialog);
}
