package kalucki.przemyslaw.unt.Features.BonjourBrowser;

import android.support.annotation.IntRange;

/**
 * Created by prka on 08.11.17.
 */

public class BonjourBrowserSettings
{
	//0 name asc, 1 name dsc
	@IntRange (from = 0, to = 1)
	public int sortMode;
}
