package kalucki.przemyslaw.unt.Features.iPerf.presenter;

import kalucki.przemyslaw.unt.Base.BasePresenter;
import kalucki.przemyslaw.unt.Features.iPerf.ui.IPerfView;

/**
 * Created by prka on 16.11.17.
 */

public interface IPerfPresenter extends BasePresenter<IPerfView>
{
	void start();
	void stop();
}
