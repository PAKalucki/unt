package kalucki.przemyslaw.unt.Features.BonjourBrowser.dagger;

import javax.inject.Singleton;

import dagger.Subcomponent;
import kalucki.przemyslaw.unt.Features.BonjourBrowser.ui.BonjourBrowserFragment;
import kalucki.przemyslaw.unt.Features.BonjourBrowser.ui.BonjourBrowserSettingsDialog;

/**
 * Created by prka on 16.11.17.
 */

@BonjourBrowser
@Subcomponent (modules = BonjourBrowserModule.class)
public interface BonjourBrowserSubComponent
{
	void inject(BonjourBrowserFragment bonjourBrowserFragment);
	void inject(BonjourBrowserSettingsDialog bonjourBrowserSettingsDialog);
}
