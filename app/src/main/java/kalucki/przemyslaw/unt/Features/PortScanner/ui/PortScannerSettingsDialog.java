package kalucki.przemyslaw.unt.Features.PortScanner.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import kalucki.przemyslaw.unt.Base.BaseDialogFragment;
import kalucki.przemyslaw.unt.MainApplication;
import kalucki.przemyslaw.unt.R;

/**
 * Created by prka on 09.10.17.
 */

public class PortScannerSettingsDialog extends BaseDialogFragment
{
	@BindString (R.string.port_mode_key) String portModeKey;
	@BindString (R.string.port_range_key) String portRangeKey;
	@BindView (R.id.scan_mode_spinner) Spinner spinnerMode;
	@BindView (R.id.scan_range_editText) EditText rangeEdit;
	@Inject SharedPreferences preferences;
	private Unbinder unbinder;

	public PortScannerSettingsDialog()
	{

	}

	public static PortScannerSettingsDialog newInstance()
	{
		return new PortScannerSettingsDialog();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.scan_settings, container, false);

		unbinder = ButterKnife.bind(this, view);

		ArrayAdapter<CharSequence> arrayAdapterModeSelection = ArrayAdapter.createFromResource(
				view.getContext(), R.array.scan_settings_spinner, android.R.layout.simple_spinner_item);
		arrayAdapterModeSelection.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerMode.setAdapter(arrayAdapterModeSelection);

		return view;
	}

	@Override
	public void onDestroyView()
	{
		super.onDestroyView();
		unbinder.unbind();
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle bundle)
	{
		super.onViewCreated(view, bundle);

		spinnerMode.setSelection(preferences.getInt(portModeKey, 0));
		rangeEdit.setText(preferences.getString(portRangeKey, "0 - 65535"));//TODO implement
	}

	@OnClick ({R.id.scan_ok_button, R.id.scan_cancel_button})
	public void onClick(View view)
	{
		switch(view.getId())
		{
			case R.id.scan_ok_button:
				readAndSavePreferences();
				break;
			case R.id.scan_cancel_button:
				cancel();
				break;
		}
	}

	private void readAndSavePreferences()
	{
		SharedPreferences.Editor editor = preferences.edit();

		editor.putInt(portModeKey, spinnerMode.getSelectedItemPosition());
		editor.putString(portRangeKey, rangeEdit.getText().toString());

		editor.apply();
		dismiss();
	}

	private void cancel()
	{
		dismiss();
	}

	@Override
	protected void injectDependencies(MainApplication application)
	{
		application.getPortScannerSubComponent().inject(this);
	}
}
