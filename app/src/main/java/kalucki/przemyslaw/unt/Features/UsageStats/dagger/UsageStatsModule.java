package kalucki.przemyslaw.unt.Features.UsageStats.dagger;

import dagger.Module;
import dagger.Provides;
import kalucki.przemyslaw.unt.Features.UsageStats.presenter.UsageStatsPresenter;
import kalucki.przemyslaw.unt.Features.UsageStats.presenter.UsageStatsPresenterImpl;

/**
 * Created by prka on 16.11.17.
 */

@Module
public class UsageStatsModule
{
	@Provides
	@Usage
	UsageStatsPresenter provideUsageStatsPresenter(UsageStatsPresenterImpl presenter)
	{
		return presenter;
	}
}
