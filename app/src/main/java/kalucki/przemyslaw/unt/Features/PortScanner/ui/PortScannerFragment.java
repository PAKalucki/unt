package kalucki.przemyslaw.unt.Features.PortScanner.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import android.widget.EditText;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import kalucki.przemyslaw.unt.Base.BaseRecyclerWithButtonFragment;
import kalucki.przemyslaw.unt.Features.PortScanner.presenter.PortScannerPresenter;
import kalucki.przemyslaw.unt.Features.PortScanner.PortScannerSettings;
import kalucki.przemyslaw.unt.MainApplication;
import kalucki.przemyslaw.unt.R;

/**
 * Created by prka on 09.10.17.
 */

public class PortScannerFragment extends BaseRecyclerWithButtonFragment implements PortScannerView
{
	@BindView (R.id.base_editText) EditText editText;
	@Inject PortScannerPresenter presenter;
	@Inject SharedPreferences preferences;
	private static final String OUTPUTKEY = "LOOKUP_OUTPUT_KEY";
	private static final String STARTEDKEY = "LOOKUP_STARTED_KEY";

	public PortScannerFragment()
	{

	}

	@Override
	public void onAttach(Context context)
	{
		super.onAttach(context);
		presenter.bind(this);

		setIsStartedKey(STARTEDKEY);
		setOutputKey(OUTPUTKEY);
	}

	@Override
	public void onDetach()
	{
		super.onDetach();
		presenter.unbind();
	}

	@Override
	public PortScannerSettings loadSettings()
	{
		return new PortScannerSettings();
	}

	@OnClick ({R.id.base_button})
	public void onClick()
	{
		if(isStarted)
		{
			presenter.stop();
		}
		else
		{
			if(!TextUtils.isEmpty(editText.getText()))
			{
				presenter.start(editText.getText().toString());
			}
		}
	}

	@Override
	protected void injectDependencies(MainApplication application)
	{
		application.getPortScannerSubComponent().inject(this);
	}
}
