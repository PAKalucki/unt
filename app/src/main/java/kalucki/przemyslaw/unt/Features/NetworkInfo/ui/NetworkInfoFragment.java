package kalucki.przemyslaw.unt.Features.NetworkInfo.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindArray;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import kalucki.przemyslaw.unt.Adapter.BasicAdapter.BasicAdapter;
import kalucki.przemyslaw.unt.Base.BaseFragment;
import kalucki.przemyslaw.unt.Features.NetworkInfo.NetworkInfoSettings;
import kalucki.przemyslaw.unt.Features.NetworkInfo.NetworkInformation;
import kalucki.przemyslaw.unt.Features.NetworkInfo.presenter.NetworkInfoPresenter;
import kalucki.przemyslaw.unt.Features.NetworkInfo.NetworkInformationImpl;
import kalucki.przemyslaw.unt.MainApplication;
import kalucki.przemyslaw.unt.R;

public class NetworkInfoFragment extends BaseFragment implements NetworkInfoView
{
	@BindArray (R.array.infoFields) String[] itemsArray;
	@BindString (R.string.networkinfo_graphpooling_key) String graphIntervalKey;
	@BindString (R.string.networkinfo_tablepooling_key) String tableIntervalKey;
	@BindView (R.id.info_list_recyclerview) RecyclerView recyclerView;
	@BindView (R.id.info_signal_chart) LineChart chart;
	@Inject NetworkInfoPresenter presenter;
	@Inject SharedPreferences preferences;
	List<String> outputItems = new ArrayList<>();
	private BasicAdapter adapter;
	private Unbinder unbinder;

	public NetworkInfoFragment()
	{
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.fragment_info, container, false);
		unbinder = ButterKnife.bind(this, view);
		return view;
	}

	@Override
	public void onDestroyView()
	{
		super.onDestroyView();
		unbinder.unbind();
	}

	@Override
	public void onStart()
	{
		super.onStart();
		View view = getView();

//        RxJavaPlugins.setErrorHandler(e ->
//        {
//            if ((e instanceof IOException) || (e instanceof SocketException))//add ConnectException here
//            {
//                Log.i("EXCEPTION CAUGHT", e.getMessage());
//                // fine, irrelevant network problem or API that throws on cancellation
//                return;
//            }
//        });

		if(view != null)
		{
			presenter.bind(this);

			recyclerView.setLayoutManager(new GridLayoutManager(view.getContext(), 2, GridLayoutManager.VERTICAL, false));
			adapter = new BasicAdapter(outputItems);
			recyclerView.setAdapter(adapter);

			presenter.start();
		}
	}

	@Override
	public void onStop()
	{
		super.onStop();
		presenter.unbind();
	}

	@Override
	public void updateGraph(Float yValue)
	{
		if(yValue != null)
		{
			LineData data = chart.getData();

			if(data != null)
			{
				ILineDataSet set = data.getDataSetByIndex(0);

				if(set == null)
				{
					set = createSet();
					data.addDataSet(set);
				}

				set.setLabel("Signal: " + yValue.toString() + "%");

				data.addEntry(new Entry(set.getEntryCount(), yValue), 0);
				data.notifyDataChanged();

				chart.notifyDataSetChanged();
				chart.setVisibleXRangeMaximum(10);
				chart.moveViewToX(data.getEntryCount());
			}
		}
	}

	@Override
	public NetworkInformation getNetworkInformation()
	{
		return new NetworkInformationImpl(getContext().getApplicationContext());
	}

	@Override
	public void start()
	{
		prepareInfoData();
		prepareChart();
	}

	@Override
	public void updateTable(List<String> newValues)
	{
		adapter.fillColumn(1, 2, newValues);
	}

	@Override
	public NetworkInfoSettings loadSettings()
	{
		NetworkInfoSettings settings = new NetworkInfoSettings();
		settings.graphPoolingInterval = preferences.getInt(graphIntervalKey, 1000);
		settings.tablePoolingInterval = preferences.getInt(tableIntervalKey, 2000);

		return settings;
	}

	private void prepareInfoData()
	{
		if(outputItems.isEmpty())
		{
			outputItems.addAll(Arrays.asList(itemsArray));
		}
	}

	private void prepareChart()
	{
		if(chart != null)
		{
			chart.getDescription().setEnabled(false);
//			chart.setVisibleXRangeMaximum(10);

			chart.setPinchZoom(false);
			chart.setDrawBorders(false);
			chart.setDrawGridBackground(false);
			chart.setDragEnabled(false);
			chart.setScaleEnabled(false);
			chart.setHighlightPerDragEnabled(false);
			chart.setHighlightPerTapEnabled(false);

			chart.getAxisLeft().setEnabled(false);

			chart.getXAxis().setEnabled(true);
			chart.getXAxis().setDrawLabels(false);
			chart.getXAxis().setDrawGridLines(false);

			chart.getAxisRight().setEnabled(false);
			chart.getAxisRight().setDrawLabels(true);
//			chart.getAxisRight().setDrawGridLines(false);
//			chart.getAxisRight().setAxisMaximum(100f);
//			chart.getAxisRight().setAxisMinimum(0f);
//			chart.getAxisRight().setLabelCount(3, false);

			LineData data = new LineData();
			chart.setData(data);
		}
	}

	private LineDataSet createSet()
	{
		LineDataSet set = new LineDataSet(null, "");
		set.setAxisDependency(YAxis.AxisDependency.RIGHT);
		set.setColor(ColorTemplate.getHoloBlue());
		set.setDrawCircles(false);
		set.setLineWidth(2f);
		set.setDrawValues(false);
		set.setDrawFilled(true);
		return set;
	}

	@Override
	protected void injectDependencies(MainApplication application)
	{
		application.getNetworkInfoSubComponent().inject(this);
	}
}
