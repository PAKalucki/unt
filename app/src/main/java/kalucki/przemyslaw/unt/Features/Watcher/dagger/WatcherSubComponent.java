package kalucki.przemyslaw.unt.Features.Watcher.dagger;

import dagger.Subcomponent;
import kalucki.przemyslaw.unt.Features.Watcher.ui.WatcherFragment;
import kalucki.przemyslaw.unt.Features.Watcher.ui.WatcherSettingsDialog;

/**
 * Created by prka on 16.11.17.
 */

@Watcher
@Subcomponent(modules = WatcherModule.class)
public interface WatcherSubComponent
{
	void inject(WatcherFragment watcherFragment);
	void inject(WatcherSettingsDialog watcherSettingsDialog);
}
