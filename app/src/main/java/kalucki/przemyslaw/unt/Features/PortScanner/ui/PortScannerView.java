package kalucki.przemyslaw.unt.Features.PortScanner.ui;

import kalucki.przemyslaw.unt.Base.BaseStandardView;
import kalucki.przemyslaw.unt.Features.PortScanner.PortScannerSettings;

/**
 * Created by prka on 02.11.17.
 */

public interface PortScannerView extends BaseStandardView
{
	PortScannerSettings loadSettings();
}
