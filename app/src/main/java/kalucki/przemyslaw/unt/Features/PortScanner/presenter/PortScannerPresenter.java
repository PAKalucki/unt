package kalucki.przemyslaw.unt.Features.PortScanner.presenter;

import kalucki.przemyslaw.unt.Base.BasePresenter;
import kalucki.przemyslaw.unt.Features.PortScanner.ui.PortScannerView;

/**
 * Created by prka on 16.11.17.
 */

public interface PortScannerPresenter extends BasePresenter<PortScannerView>
{
	void start(String value);
	void stop();
}
