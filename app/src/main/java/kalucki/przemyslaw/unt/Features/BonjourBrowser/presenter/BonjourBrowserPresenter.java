package kalucki.przemyslaw.unt.Features.BonjourBrowser.presenter;

import kalucki.przemyslaw.unt.Base.BasePresenter;
import kalucki.przemyslaw.unt.Features.BonjourBrowser.ui.BonjourBrowserView;

/**
 * Created by prka on 15.11.17.
 */

public interface BonjourBrowserPresenter extends BasePresenter<BonjourBrowserView>
{
	void start(String value);
	void stop();
}
