package kalucki.przemyslaw.unt.Features.Watcher.ui;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import dagger.Lazy;
import kalucki.przemyslaw.unt.Base.BaseFragment;
import kalucki.przemyslaw.unt.Features.Watcher.presenter.WatcherPresenter;
import kalucki.przemyslaw.unt.MainApplication;
import kalucki.przemyslaw.unt.R;

public class WatcherFragment extends BaseFragment implements WatcherView
{
	@Inject Lazy<WatcherPresenter> presenter;

	public WatcherFragment()
	{
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_watcher, container, false);
	}

	@Override
	protected void injectDependencies(MainApplication application)
	{
		application.getWatcherSubComponent().inject(this);
	}
}
