package kalucki.przemyslaw.unt.Features.DNSLookUp.ui;

import kalucki.przemyslaw.unt.Base.BaseStandardView;
import kalucki.przemyslaw.unt.Features.DNSLookUp.LookUpSettings;

/**
 * Created by prka on 08.11.17.
 */

public interface LookUpView extends BaseStandardView
{
	LookUpSettings loadSettings();
}
