package kalucki.przemyslaw.unt.Features.TraceRoute.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.widget.EditText;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;
import kalucki.przemyslaw.unt.Base.BaseRecyclerWithButtonFragment;
import kalucki.przemyslaw.unt.Features.TraceRoute.TraceRouteSettings;
import kalucki.przemyslaw.unt.Features.TraceRoute.presenter.TraceRoutePresenter;
import kalucki.przemyslaw.unt.MainApplication;
import kalucki.przemyslaw.unt.R;

public class TraceRouteFragment extends BaseRecyclerWithButtonFragment implements TraceRouteView
{
	@BindString (R.string.trace_count_key) String traceCountKey;
	@BindString (R.string.trace_hops_key) String traceHopsKey;
	@BindString (R.string.trace_timeout_key) String traceTimeoutKey;
	@BindView (R.id.base_editText) EditText input;
	@Inject SharedPreferences preferences;
	@Inject TraceRoutePresenter presenter;
	private static final String OUTPUTKEY = "TRACE_OUTPUT_KEY";
	private static final String STARTEDKEY = "TRACE_STARTED_KEY";

	public TraceRouteFragment()
	{
		// Required empty public constructor
	}

	@Override
	public void onAttach(Context context)
	{
		super.onAttach(context);
		presenter.bind(this);

		setIsStartedKey(STARTEDKEY);
		setOutputKey(OUTPUTKEY);
	}

	@Override
	public void onDetach()
	{
		super.onDetach();
		presenter.unbind();
	}

	@Override
	public TraceRouteSettings loadSettings()
	{
		TraceRouteSettings settings = new TraceRouteSettings();

		settings.count = preferences.getInt(traceCountKey, 0);
		settings.hops = preferences.getInt(traceHopsKey, 0);
		settings.timeout = preferences.getInt(traceTimeoutKey, 0);

		return settings;
	}

	@OnClick ({R.id.base_button})
	public void onClick()
	{
		if(isStarted)
		{
			presenter.stop();
		}
		else
		{
			if(!TextUtils.isEmpty(input.getText()))
			{
				presenter.start(input.getText().toString());
			}
		}
	}

	@Override
	protected void injectDependencies(MainApplication application)
	{
		application.getTraceRouteSubComponent().inject(this);
	}
}
