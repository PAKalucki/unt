package kalucki.przemyslaw.unt.Features.NetworkInfo.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import javax.annotation.Nullable;
import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import kalucki.przemyslaw.unt.Base.BaseDialogFragment;
import kalucki.przemyslaw.unt.MainApplication;
import kalucki.przemyslaw.unt.R;
import kalucki.przemyslaw.unt.Utils.TextDrawable;

/**
 * Created by prka on 31.10.17.
 */

public class NetworkInfoSettingsDialog extends BaseDialogFragment
{
	@BindString (R.string.networkinfo_graphpooling_key) String graphIntervalKey;
	@BindString (R.string.networkinfo_graphpooling_string) String graphIntervalString;
	@BindString (R.string.networkinfo_tablepooling_key) String tableIntervalKey;
	@BindString (R.string.networkinfo_tablepooling_string) String tableIntervalString;
	@BindView (R.id.networkinfo_graphInterval_editText) EditText editTextGraphInterval;
	@BindView (R.id.networkinfo_tableInterval_editText) EditText editTextTableInterval;
	@Inject SharedPreferences preferences;
	private Unbinder unbinder;

	public NetworkInfoSettingsDialog()
	{
	}

	public static NetworkInfoSettingsDialog newInstance()
	{
		return new NetworkInfoSettingsDialog();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.networkinfo_settings, container, false);

		unbinder = ButterKnife.bind(this, view);

		return view;
	}

	@Override
	public void onDestroyView()
	{
		super.onDestroyView();
		unbinder.unbind();
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle bundle)
	{
		super.onViewCreated(view, bundle);

		editTextGraphInterval.setCompoundDrawablesWithIntrinsicBounds(new TextDrawable(graphIntervalString), null, null, null);
		editTextTableInterval.setCompoundDrawablesWithIntrinsicBounds(new TextDrawable(tableIntervalString), null, null, null);

		editTextGraphInterval.setCompoundDrawablePadding(graphIntervalString.length() * 10);
		editTextTableInterval.setCompoundDrawablePadding(tableIntervalString.length() * 10);

		editTextGraphInterval.setText(String.valueOf(preferences.getInt(graphIntervalKey, 1000)));
		editTextTableInterval.setText(String.valueOf(preferences.getInt(tableIntervalKey, 2000)));
	}

	@OnClick ({R.id.networkinfo_ok_button, R.id.networkinfo_cancel_button, R.id.networkinfo_restore_button})
	public void onClick(View view)
	{
		switch(view.getId())
		{
			case R.id.networkinfo_ok_button:
				readAndSavePreferences();
				break;
			case R.id.networkinfo_cancel_button:
				cancel();
				break;
			case R.id.networkinfo_restore_button:
				restoreDefaultSettings();
				break;
		}
	}

	@Override
	protected void injectDependencies(MainApplication application)
	{
		application.getNetworkInfoSubComponent().inject(this);
	}

	private void readAndSavePreferences()
	{
		SharedPreferences.Editor editor = preferences.edit();

		editor.putInt(graphIntervalKey, Integer.valueOf(editTextGraphInterval.getText().toString()));
		editor.putInt(tableIntervalKey, Integer.valueOf(editTextTableInterval.getText().toString()));
		editor.apply();
		dismiss();
	}

	private void cancel()
	{
		dismiss();
	}

	private void restoreDefaultSettings()
	{
		SharedPreferences.Editor editor = preferences.edit();

		editor.putInt(graphIntervalKey, 1000);
		editor.putInt(tableIntervalKey, 2000);
		editor.apply();
		dismiss();
	}
}
