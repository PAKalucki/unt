package kalucki.przemyslaw.unt.Features.UsageStats;

import android.support.annotation.IntRange;

/**
 * Created by prka on 07.11.17.
 */

public class UsageStatsSettings
{
	@IntRange (from = 0, to = 5)
	public int sortMode; //0 name desc, 1 name asc, 2 down desc, 3 down asc, 4 up desc, 5 up asc
	public long poolingIntervalGraph;
	public long poolingIntervalTable;
}
