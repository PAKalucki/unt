package kalucki.przemyslaw.unt.Features.Lan.dagger;

import dagger.Module;
import dagger.Provides;
import kalucki.przemyslaw.unt.Features.Lan.presenter.LanPresenter;
import kalucki.przemyslaw.unt.Features.Lan.presenter.LanPresenterImpl;

/**
 * Created by prka on 16.11.17.
 */

@Module
public class LanModule
{
	@Provides
	@Lan
	LanPresenter provideLanPresenter(LanPresenterImpl presenter)
	{
		return presenter;
	}
}
