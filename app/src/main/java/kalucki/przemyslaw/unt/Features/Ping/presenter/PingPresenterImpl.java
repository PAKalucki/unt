package kalucki.przemyslaw.unt.Features.Ping.presenter;

import android.util.Log;

import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import kalucki.przemyslaw.unt.Features.Ping.ICMPPing;
import kalucki.przemyslaw.unt.Features.Ping.PingSettings;
import kalucki.przemyslaw.unt.Features.Ping.TCPPing;
import kalucki.przemyslaw.unt.Features.Ping.ui.PingView;
import kalucki.przemyslaw.unt.Utils.SchedulerProvider;

/**
 * Created by przemek on 01.07.17.
 */

public class PingPresenterImpl implements PingPresenter
{
	private CompositeDisposable compositeDisposable = new CompositeDisposable();
	private ICMPPing icmpPing = new ICMPPing();//TODO initializing this here might be bad idea considering i keep presenters in memory all the time
	private PingSettings pingSettings;
	private SchedulerProvider scheduler;
	private TCPPing tcpPing = new TCPPing();//TODO up ^
	private WeakReference<PingView> view;

	@Inject
	public PingPresenterImpl(SchedulerProvider scheduler)
	{
		this.scheduler = scheduler;
	}

	@Override
	public void bind(PingView view)
	{
		this.view = new WeakReference<>(view);
	}

	@Override
	public void unbind()
	{
		clear();//TODO this is questionable way to release memory but running this in stop() without sleep cuts final summary output

		view.clear();
		view = null;
	}

	@Override
	public void start(String address)
	{
		pingSettings = view.get().loadSettings();
		switch(pingSettings.mode)
		{
			case 0:
				view.get().start();
				runICMPPing(address);
				break;
			case 1:
				view.get().start();
				runTCPing(address);
				break;
		}
	}

	@Override
	public void stop()
	{
		if(icmpPing != null && icmpPing.isActive())
		{
			icmpPing.stop();
		}

		if(tcpPing != null && tcpPing.isActive())
		{
			tcpPing.stop();
		}

		view.get().stop();
	}

	private void runICMPPing(String address)
	{
		icmpPing.setAddress(address);
		icmpPing.setSettings(pingSettings);

		compositeDisposable.add(
				icmpPing.getOutput()//TODO why not have String address here instead of setter ?
				        .subscribeOn(scheduler.backgroundScheduler())
				        .observeOn(scheduler.mainScheduler())
				        .doOnNext(value->
				                  {
					                  if(view != null)//TODO Else cache OR use Flowable and it's caching
					                  {
						                  Log.i("PING ICMP: ", "View not null");
						                  if(value.output != null && !value.output.isEmpty())
						                  {
							                  Log.i("PING ICMP: ", value.output);
							                  view.get().update(value.output);
						                  }
					                  }
				                  })
				        .doOnComplete(()->
				                      {
					                      if(view != null)
					                      {
						                      Log.i("PING ICMP: ", "Completed");
						                      view.get().stop();
					                      }
				                      })
				        .subscribe());
	}

	private void runTCPing(String address)
	{
		tcpPing.setAddress(address);
		tcpPing.setSettings(pingSettings);

		compositeDisposable.add(
				Observable.zip(
						Observable.interval(pingSettings.interval, TimeUnit.MILLISECONDS),
						tcpPing.ping().repeat(pingSettings.count).toObservable(),
						(timer, ping)->ping)
				          .subscribeOn(scheduler.backgroundScheduler())
				          .observeOn(scheduler.mainScheduler())
				          .doOnNext(value->
				                    {
					                    if(view != null)
					                    {
						                    if(value.success)
						                    {
							                    view.get().update("Ping " + address + " Success" + "  Time: " + String
									                    .valueOf(value.time) + " ms");
						                    }
						                    else
						                    {
							                    view.get().update("Error");
						                    }
					                    }
				                    })
				          .doFinally(()->
				                     {
					                     if(view != null)
					                     {
						                     view.get().stop();
					                     }
				                     })
				          .subscribe()
		);
	}

	private void clear()
	{
		if(!tcpPing.isActive() && !icmpPing.isActive())
		{
			if(compositeDisposable != null && !compositeDisposable.isDisposed())
			{
				compositeDisposable.clear();
			}
		}
	}
}
