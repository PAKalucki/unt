package kalucki.przemyslaw.unt.Features.DNSLookUp.dagger;

import dagger.Subcomponent;
import kalucki.przemyslaw.unt.Features.DNSLookUp.ui.LookUpFragment;
import kalucki.przemyslaw.unt.Features.DNSLookUp.ui.LookUpSettingsDialog;

/**
 * Created by prka on 16.11.17.
 */

@LookUp
@Subcomponent(modules = LookUpModule.class)
public interface LookUpSubComponent
{
	void inject(LookUpFragment lookUpFragment);
	void inject(LookUpSettingsDialog lookUpSettingsDialog);
}
