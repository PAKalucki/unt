package kalucki.przemyslaw.unt.Features.Lan;

/**
 * Created by prka on 23.10.17.
 */

public final class LanResult
{
	public final boolean success;
	public final String address;

	public LanResult(String address, boolean success)
	{
		this.address = address;
		this.success = success;
	}

	@Override
	public String toString()
	{
		return success + " " + address;
	}
}
