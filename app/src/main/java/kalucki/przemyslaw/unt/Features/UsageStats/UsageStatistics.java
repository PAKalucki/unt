package kalucki.przemyslaw.unt.Features.UsageStats;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.usage.NetworkStats;
import android.app.usage.NetworkStatsManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.TrafficStats;
import android.os.Build;
import android.os.RemoteException;
import android.telephony.TelephonyManager;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import kalucki.przemyslaw.unt.Utils.IconHelper;

/**
 * Created by przemek on 09.04.17.
 */

public class UsageStatistics
{
	private Context context;
	private long interval;
	private static final int TYPE_MOBILE = ConnectivityManager.TYPE_MOBILE;
	private static final int TYPE_WIFI = ConnectivityManager.TYPE_WIFI;

	public UsageStatistics(Context context)
	{
		this.context = context;
	}

	public long getInterval()
	{
		return interval;
	}

	public void setInterval(long interval)
	{
		this.interval = interval;
	}

	public Long getCurrentTotalDownload()//this seems to be working even above android M ??
	{
		Long receivedBytesFirst = TrafficStats.getTotalRxBytes();

		try
		{
			Thread.sleep(interval);
		}
		catch(Exception e)
		{
		}

		Long receivedBytesAfter = TrafficStats.getTotalRxBytes();

		return (receivedBytesAfter - receivedBytesFirst);
	}

	public Long getCurrentTotalUpload()
	{
		Long sentBytesFirst = TrafficStats.getTotalTxBytes();

		try
		{
			Thread.sleep(interval);
		}
		catch(Exception e)
		{
		}

		Long sentBytesAfter = TrafficStats.getTotalTxBytes();

		return (sentBytesAfter - sentBytesFirst);
	}

	public Flowable<Long> getCurrentTotalDownloadObservable()
	{
		return Flowable.just(getCurrentTotalDownload());
	}

	public Flowable<Long> getCurrentTotalUploadObservable()
	{
		return Flowable.just(getCurrentTotalUpload());
	}

	public Observable<AppDataContainer> getApps()
	{
		return Observable.create(subscriber->
		                         {
			                         PackageManager packageManager = context.getPackageManager();

			                         for(ApplicationInfo info : packageManager.getInstalledApplications(0))
			                         {
				                         AppDataContainer appData = new AppDataContainer(info.uid,
				                                                                         IconHelper.getBitmapFromDrawable(packageManager
						                                                                                                          .getApplicationIcon(info)),
				                                                                         packageManager.getApplicationLabel(info).toString(),
				                                                                         getTotalAppDataDownload(info.uid),
				                                                                         getTotalAppDataUpload(info.uid));

				                         subscriber.onNext(appData);
			                         }

			                         subscriber.onComplete();
		                         });
	}

	private long getTotalAppDataDownload(int uid)
	{
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
		{
			return getAppDataDownloadWiFi(uid) + getAppDataDownloadMobile(uid);
		}
		else
		{
			return TrafficStats.getUidRxBytes(uid);
		}
	}

	private long getTotalAppDataUpload(int uid)
	{
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
		{
			return getAppDataUploadWiFi(uid) + getAppDataUploadMobile(uid);
		}
		else
		{
			return TrafficStats.getUidTxBytes(uid);
		}
	}

	@TargetApi (Build.VERSION_CODES.M)
	private long getAppDataDownloadWiFi(int uid)
	{
		try
		{
			NetworkStatsManager manager = (NetworkStatsManager) context.getSystemService(Context.NETWORK_STATS_SERVICE);
			NetworkStats stats = manager.queryDetailsForUid(TYPE_WIFI, "", 0, System.currentTimeMillis(), uid);
			NetworkStats.Bucket bucket = new NetworkStats.Bucket();
			stats.getNextBucket(bucket);
			return bucket.getRxBytes();
		}
		catch(RemoteException e)
		{
			return -1;
		}
	}

	@TargetApi (Build.VERSION_CODES.M)
	private long getAppDataDownloadMobile(int uid)
	{
		try
		{
			NetworkStatsManager manager = (NetworkStatsManager) context.getSystemService(Context.NETWORK_STATS_SERVICE);
			NetworkStats stats = manager.queryDetailsForUid(TYPE_MOBILE, getId(context, TYPE_MOBILE), 0, System.currentTimeMillis(), uid);
			NetworkStats.Bucket bucket = new NetworkStats.Bucket();
			stats.getNextBucket(bucket);
			return bucket.getRxBytes();
		}
		catch(RemoteException e)
		{
			return -1;
		}
	}

	@SuppressLint ("NewApi")
	private long getAppDataUploadWiFi(int uid)
	{
		try
		{
			NetworkStatsManager manager = (NetworkStatsManager) context.getSystemService(Context.NETWORK_STATS_SERVICE);
			NetworkStats stats = manager.queryDetailsForUid(TYPE_WIFI, "", 0, System.currentTimeMillis(), uid);
			NetworkStats.Bucket bucket = new NetworkStats.Bucket();
			stats.getNextBucket(bucket);
			return bucket.getTxBytes();
		}
		catch(RemoteException e)
		{
			return -1;
		}
	}

	@SuppressLint ("NewApi")
	private long getAppDataUploadMobile(int uid)
	{
		try
		{
			NetworkStatsManager manager = (NetworkStatsManager) context.getSystemService(Context.NETWORK_STATS_SERVICE);
			NetworkStats stats = manager.queryDetailsForUid(TYPE_MOBILE, getId(context, TYPE_MOBILE), 0, System.currentTimeMillis(), uid);
			NetworkStats.Bucket bucket = new NetworkStats.Bucket();
			stats.getNextBucket(bucket);
			return bucket.getTxBytes();
		}
		catch(RemoteException e)
		{
			return -1;
		}
	}

	@SuppressLint ("MissingPermission")
	private String getId(Context context, int networkType)
	{
		if(ConnectivityManager.TYPE_MOBILE == networkType)
		{
			TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
			return tm.getSubscriberId();
		}
		return "";
	}

	private void wait(int interval)
	{


	}
}
