package kalucki.przemyslaw.unt.Features.TraceRoute.ui;

import kalucki.przemyslaw.unt.Features.TraceRoute.TraceRouteSettings;

/**
 * Created by prka on 02.11.17.
 */

public interface TraceRouteView
{
	void start();

	void stop();

	void update(String value);

	TraceRouteSettings loadSettings();
}
