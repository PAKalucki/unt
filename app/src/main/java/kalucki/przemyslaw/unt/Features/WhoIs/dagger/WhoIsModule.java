package kalucki.przemyslaw.unt.Features.WhoIs.dagger;

import dagger.Module;
import dagger.Provides;
import kalucki.przemyslaw.unt.Features.WhoIs.presenter.WhoIsPresenter;
import kalucki.przemyslaw.unt.Features.WhoIs.presenter.WhoIsPresenterImpl;

/**
 * Created by prka on 16.11.17.
 */

@Module
public class WhoIsModule
{
	@Provides
	@WhoIs
	WhoIsPresenter provideWhoIsPresenter(WhoIsPresenterImpl presenter)
	{
		return presenter;
	}
}
