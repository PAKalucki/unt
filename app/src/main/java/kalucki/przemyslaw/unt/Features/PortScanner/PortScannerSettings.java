package kalucki.przemyslaw.unt.Features.PortScanner;

import android.support.annotation.IntRange;

/**
 * Created by prka on 09.10.17.
 */

public class PortScannerSettings
{
	@IntRange (from = 0, to = 65535)
	int rangeMin;

	@IntRange (from = 0, to = 65535)
	int rangeMax;

	//0 - range, 1 - most popular ports
	@IntRange (from = 0, to = 1)
	int mode;
}
