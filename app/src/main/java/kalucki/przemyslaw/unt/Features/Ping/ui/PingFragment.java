package kalucki.przemyslaw.unt.Features.Ping.ui;

import android.content.Context;
import android.content.SharedPreferences;

import android.text.TextUtils;

import android.util.Log;
import android.widget.EditText;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;

import kalucki.przemyslaw.unt.Base.BaseRecyclerWithButtonFragment;

import kalucki.przemyslaw.unt.Features.Ping.presenter.PingPresenter;
import kalucki.przemyslaw.unt.Features.Ping.PingSettings;
import kalucki.przemyslaw.unt.MainApplication;
import kalucki.przemyslaw.unt.R;

public class PingFragment extends BaseRecyclerWithButtonFragment implements PingView
{
	@BindString (R.string.ping_count_key) String pingCountKey;
	@BindString (R.string.ping_interval_key) String pingIntervalKey;
	@BindString (R.string.ping_mode_key) String pingModeKey;
	@BindString (R.string.ping_packetsize_key) String pingPacketSizeKey;
	@BindString (R.string.ping_port_key) String pingPortKey;
	@BindString (R.string.ping_timeout_key) String pingTimeoutKey;
	@BindString (R.string.ping_ttl_key) String pingTtlKey;
	@BindView (R.id.base_editText) EditText editText;
	@Inject PingPresenter presenter;
	@Inject SharedPreferences preferences;
	private static final String OUTPUTKEY = "PING_OUTPUT_KEY";
	private static final String STARTEDKEY = "PING_STARTED_KEY";

	public PingFragment()
	{
		// Required empty public constructor
	}

	@Override
	public void onAttach(Context context)
	{
		super.onAttach(context);
		presenter.bind(this);

		setIsStartedKey(STARTEDKEY);
		setOutputKey(OUTPUTKEY);

		Log.i(this.getClass().getName(), "onAttach");
	}

	@Override
	public void onDetach()
	{
		super.onDetach();
		presenter.unbind();

		Log.i(this.getClass().getName(), "onDetach");
	}

	@Override
	public PingSettings loadSettings()
	{
		PingSettings settings = new PingSettings();

		settings.mode = preferences.getInt(pingModeKey, 0);
		settings.count = preferences.getInt(pingCountKey, 0);
		settings.interval = preferences.getInt(pingIntervalKey, 0);
		settings.packetsize = preferences.getInt(pingPacketSizeKey, 0);
		settings.ttl = preferences.getInt(pingTtlKey, 0);
		settings.timeout = preferences.getInt(pingTimeoutKey, 0);
		settings.port = preferences.getInt(pingPortKey, 80);

		return settings;
	}

	@OnClick ({R.id.base_button})
	public void onClick()
	{
		if(isStarted)
		{
			presenter.stop();
		}
		else
		{
			if(!TextUtils.isEmpty(editText.getText()))
			{
				presenter.start(editText.getText().toString());
			}
		}
	}

	@Override
	protected void injectDependencies(MainApplication application)
	{
		application.getPingSubComponent().inject(this);
	}
}
