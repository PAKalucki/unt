package kalucki.przemyslaw.unt.Features.BonjourBrowser.ui;

import kalucki.przemyslaw.unt.Base.BaseStandardView;
import kalucki.przemyslaw.unt.Features.BonjourBrowser.BonjourBrowserSettings;

/**
 * Created by prka on 08.11.17.
 */

public interface BonjourBrowserView extends BaseStandardView
{
	BonjourBrowserSettings loadSettings();
}
