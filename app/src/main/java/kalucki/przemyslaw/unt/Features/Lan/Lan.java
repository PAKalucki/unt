package kalucki.przemyslaw.unt.Features.Lan;

import java.io.IOException;
import java.net.InetAddress;

import io.reactivex.Observable;

/**
 * Created by prka on 03.10.17.
 */

public class Lan
{
	public Observable<LanResult> runICMPScan(String[] ips, int timeout)//TODO change to dynamic container ?
	{
		return Observable.create(subscriber->
				{
					try
					{
						for(int i = 0; i < ips.length - 1; i++)
						{
							boolean success = InetAddress.getByName(ips[i]).isReachable(timeout);
							String address = ips[i] + " " + InetAddress.getByName(ips[i]).getCanonicalHostName();
							subscriber.onNext(new LanResult(address, success));
						}
						subscriber.onComplete();
					}
					catch(IOException e)
					{
						subscriber.onError(e);
					}
				}
		);
	}
}
