package kalucki.przemyslaw.unt.Features.WhoIs.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;
import kalucki.przemyslaw.unt.Base.BaseRecyclerWithButtonFragment;
import kalucki.przemyslaw.unt.Features.WhoIs.presenter.WhoIsPresenter;
import kalucki.przemyslaw.unt.Features.WhoIs.WhoIsSettings;
import kalucki.przemyslaw.unt.MainApplication;
import kalucki.przemyslaw.unt.R;

public class WhoIsFragment extends BaseRecyclerWithButtonFragment implements WhoIsView
{
	@BindString (R.string.default_string) String defaultString;
	@BindString (R.string.whois_server_key) String whoIsServerKey;
	@BindString (R.string.whois_timeout_key) String whoIsTimeoutKey;
	@BindView (R.id.base_editText) EditText editText;
	@Inject SharedPreferences preferences;
	@Inject WhoIsPresenter presenter;
	private static final String OUTPUTKEY = "WHOIS_OUTPUT_KEY";
	private static final String STARTEDKEY = "WHOIS_STARTED_KEY";

	public WhoIsFragment()
	{
		// Required empty public constructor
	}

	@Override
	public void onAttach(Context context)
	{
		super.onAttach(context);
		presenter.bind(this);

		setIsStartedKey(STARTEDKEY);
		setOutputKey(OUTPUTKEY);
	}

	@Override
	public void onDetach()
	{
		super.onDetach();
		presenter.unbind();
	}


	@Override
	public WhoIsSettings loadSettings()
	{
		WhoIsSettings settings = new WhoIsSettings();

		settings.serverName = preferences.getString(whoIsServerKey, defaultString);
		settings.timeout = preferences.getInt(whoIsTimeoutKey, 0);

		return settings;
	}

	@OnClick ({R.id.base_button})
	public void onClick()
	{
		if(isStarted)
		{
			presenter.stop();
		}
		else
		{
			if(!TextUtils.isEmpty(editText.getText()))
			{
				presenter.start(editText.getText().toString());
			}
		}
	}

	@Override
	protected void injectDependencies(MainApplication application)
	{
		application.getWhoIsSubComponent().inject(this);
	}
}