package kalucki.przemyslaw.unt.Features.NetworkInfo;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.LinkProperties;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

import io.reactivex.Flowable;
import kalucki.przemyslaw.unt.Utils.InternetUtils;
import kalucki.przemyslaw.unt.Utils.IpUtils;

/**
 * Created by przemek on 09.04.17.
 */

public class NetworkInformationImpl implements NetworkInformation
{
	private ConnectivityManager connectivityManager;
	private InternetUtils internetUtils;
	private WifiManager wifiManager;

	public NetworkInformationImpl(Context context)
	{
		connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		internetUtils = new InternetUtils(context);
		wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
	}

	public Flowable<NetworkInfoResult> getNetworkInfo()
	{
		return Flowable.just(getData());
	}

	private NetworkInfoResult getData()
	{
		NetworkInfoResult result = new NetworkInfoResult();

		if(internetUtils.getCurrentConnectionType().equals("WIFI"))
		{
			WifiInfo wifiInfo = wifiManager.getConnectionInfo();
			DhcpInfo dhcpInfo = wifiManager.getDhcpInfo();

			result.setType("WIFI");
			result.setBssidValue(wifiInfo.getBSSID());
			result.setIpAddress(IpUtils.intToString(wifiInfo.getIpAddress()));
			result.setIpv6Address("");
			result.setSpeed(String.valueOf(wifiInfo.getLinkSpeed()).concat(WifiInfo.LINK_SPEED_UNITS));
			result.setBroadcastAddress(IpUtils.intToString((dhcpInfo.ipAddress & dhcpInfo.netmask) | ~dhcpInfo.netmask));
			result.setRssiValue(String.valueOf(wifiInfo.getRssi()));
			result.setSsidValue(wifiInfo.getSSID());
			result.setDnsValue1(IpUtils.intToString(dhcpInfo.dns1));
			result.setDnsValue2(IpUtils.intToString(dhcpInfo.dns2));
			result.setGateway(IpUtils.intToString(dhcpInfo.gateway));
			result.setLeaseDuration(String.valueOf(dhcpInfo.leaseDuration).concat(" Seconds"));
			result.setNetmask(IpUtils.intToString(dhcpInfo.netmask));
			result.setDhcpServer(IpUtils.intToString(dhcpInfo.serverAddress));
			result.setMacAddress(getMacAddr());

			return result;
		}
		else if(internetUtils.getCurrentConnectionType().equals("CELLULAR"))
		{
			LinkProperties properties = null;

			for(Network network : connectivityManager.getAllNetworks())
			{
				NetworkInfo networkInfo = connectivityManager.getNetworkInfo(network);
				if(networkInfo.isConnected())
				{
					properties = connectivityManager.getLinkProperties(network);
					break;
				}
			}

			result.setType("MOBILE");
			result.setIpAddress(properties.getLinkAddresses().get(0).getAddress().getHostAddress());
			result.setDnsValue1(properties.getDnsServers().get(0).getHostAddress());
			result.setDnsValue2(properties.getDnsServers().get(1).getHostAddress());

			return result;
		}

		result.setType("OFFLINE");
		return result;
	}

	private String getMacAddr()
	{
		try
		{
			List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
			for(NetworkInterface nif : all)
			{
				if(!nif.getName().equalsIgnoreCase("wlan0"))
				{
					continue;
				}

				byte[] macBytes = nif.getHardwareAddress();
				if(macBytes == null)
				{
					return "";
				}

				StringBuilder res1 = new StringBuilder();
				for(byte b : macBytes)
				{
					res1.append(String.format("%02X:", b));
				}

				if(res1.length() > 0)
				{
					res1.deleteCharAt(res1.length() - 1);
				}
				return res1.toString();
			}
		}
		catch(Exception ex)
		{
			return "02:00:00:00:00:00";
		}
		return "02:00:00:00:00:00";
	}

}
