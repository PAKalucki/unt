package kalucki.przemyslaw.unt.Features.NetworkInfo.presenter;

import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.disposables.CompositeDisposable;
import kalucki.przemyslaw.unt.Features.NetworkInfo.NetworkInfoSettings;
import kalucki.przemyslaw.unt.Features.NetworkInfo.NetworkInformation;
import kalucki.przemyslaw.unt.Features.NetworkInfo.ui.NetworkInfoView;
import kalucki.przemyslaw.unt.Utils.SchedulerProvider;
import kalucki.przemyslaw.unt.Utils.SignalUtil;

/**
 * Created by przemek on 21.05.17.
 */

public class NetworkInfoPresenterImpl implements NetworkInfoPresenter
{
	private CompositeDisposable compositeDisposable = new CompositeDisposable();
	private SchedulerProvider scheduler;
	private WeakReference<NetworkInfoView> view;

	@Inject
	public NetworkInfoPresenterImpl(SchedulerProvider scheduler)
	{
		this.scheduler = scheduler;
	}

	@Override
	public void bind(NetworkInfoView view)
	{
		this.view = new WeakReference<>(view);
	}

	@Override
	public void unbind()
	{
		view.clear();
		view = null;

		if(compositeDisposable != null && !compositeDisposable.isDisposed())
		{
			compositeDisposable.clear();
		}
	}

	@Override
	public void start()
	{
		view.get().start();
		NetworkInformation networkInformation = view.get().getNetworkInformation();
		NetworkInfoSettings settings = view.get().loadSettings();

		compositeDisposable.add(
				Flowable.interval(settings.graphPoolingInterval, TimeUnit.MILLISECONDS)
				        .concatMap(value->networkInformation.getNetworkInfo())
				        .subscribeOn(scheduler.backgroundScheduler())
				        .observeOn(scheduler.mainScheduler())
				        .doOnNext(value->
				                  {
					                  if(view != null)
					                  {
						                  if(value.getType().equals("WIFI"))
						                  {
							                  view.get().updateGraph(calcSignal(Float.valueOf(value.getRssiValue())));
						                  }
					                  }
				                  })
				        .subscribe());

		compositeDisposable.add(
				Flowable.interval(settings.tablePoolingInterval, TimeUnit.MILLISECONDS)
				        .concatMap(value->networkInformation.getNetworkInfo())
				        .subscribeOn(scheduler.backgroundScheduler())
				        .observeOn(scheduler.mainScheduler())
				        .doOnNext(value->
				                  {
					                  if(view != null)
					                  {
						                  view.get().updateTable(value.getAll());
					                  }
				                  })
				        .subscribe());
	}

	private float calcSignal(float value)
	{
		return SignalUtil.calcStrenghtPercent(value);
	}
}
