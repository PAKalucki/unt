package kalucki.przemyslaw.unt.Features.Watcher.ui;

import kalucki.przemyslaw.unt.Base.BaseDialogFragment;
import kalucki.przemyslaw.unt.MainApplication;

/**
 * Created by prka on 08.11.17.
 */

public class WatcherSettingsDialog extends BaseDialogFragment
{
	public WatcherSettingsDialog()
	{

	}

	public static WatcherSettingsDialog newInstance()
	{
		return new WatcherSettingsDialog();
	}

	@Override
	protected void injectDependencies(MainApplication application)
	{
		application.getWatcherSubComponent().inject(this);
	}
}
