package kalucki.przemyslaw.unt.Features.Lan;

import android.support.annotation.IntRange;

/**
 * Created by prka on 28.09.17.
 */

public class LanSettings
{
	/**
	   0 - Sort by Name ascending
	   1 - Sort by Name descending
	   2 - Sort by Address ascending
	   3 - Sort by Address descending
	 */
	@IntRange(from=0, to=3)
	public int sortOption;
	/**
	 0 - ICMP PING
	 1 - HTTP PING
	 2 - ?
	 */
	@IntRange(from=0, to=2)
	public int modeOption;
}
