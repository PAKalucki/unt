package kalucki.przemyslaw.unt.Features.DNSLookUp.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import butterknife.Unbinder;
import kalucki.przemyslaw.unt.Base.BaseDialogFragment;
import kalucki.przemyslaw.unt.MainApplication;
import kalucki.przemyslaw.unt.R;

/**
 * Created by prka on 08.11.17.
 */

public class LookUpSettingsDialog extends BaseDialogFragment
{
	@BindView (R.id.lookup_class_spinner) Spinner classSpinner;
	@BindView (R.id.lookup_server_editText) EditText serverEditText;
	@BindView (R.id.lookup_type_spinner) Spinner typeSpinner;
	@Inject SharedPreferences preferences;
	private Unbinder unbinder;

	public LookUpSettingsDialog()
	{
	}

	public static LookUpSettingsDialog newInstance()
	{
		return new LookUpSettingsDialog();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.lookup_settings, container, false);

		unbinder = ButterKnife.bind(this, view);

		ArrayAdapter<CharSequence> arrayAdapterType = ArrayAdapter.createFromResource(view.getContext(), R.array.lookup_type_spinner,
		                                                                              android.R.layout.simple_spinner_item);
		arrayAdapterType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		typeSpinner.setAdapter(arrayAdapterType);

		ArrayAdapter<CharSequence> arrayAdapterClass = ArrayAdapter.createFromResource(view.getContext(), R.array.lookup_class_spinner,
		                                                                               android.R.layout.simple_spinner_item);
		arrayAdapterClass.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		classSpinner.setAdapter(arrayAdapterClass);

		return view;
	}

	@Override
	public void onDestroyView()
	{
		super.onDestroyView();
		unbinder.unbind();
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle bundle)
	{
		super.onViewCreated(view, bundle);

		typeSpinner.setSelection(preferences.getInt("lookup_type_key", 0));
		classSpinner.setSelection(preferences.getInt("lookup_class_key", 0));
	}

	@OnClick ({R.id.lookup_ok_button, R.id.lookup_cancel_button, R.id.lookup_restore_button})
	public void onClick(View view)
	{
		switch(view.getId())
		{
			case R.id.lookup_ok_button:
				readAndSavePreferences();
				break;
			case R.id.lookup_cancel_button:
				cancel();
				break;
			case R.id.lookup_restore_button:
				restoreDefaultSettings();
				break;
		}
	}

	@OnItemSelected ({R.id.lookup_type_spinner, R.id.lookup_class_spinner})
	public void onItemSelected(View view, int position)
	{
		switch(view.getId())
		{
			case R.id.lookup_type_spinner:
				typeSpinner.setSelection(position);
				break;
			case R.id.lookup_class_spinner:
				classSpinner.setSelection(position);
				break;
		}
	}

	@Override
	protected void injectDependencies(MainApplication application)
	{
		application.getLookUpSubComponent().inject(this);
	}

	private void readAndSavePreferences()
	{
		SharedPreferences.Editor editor = preferences.edit();

		editor.putInt("lookup_type_key", typeSpinner.getSelectedItemPosition());
		editor.putInt("lookup_class_key", classSpinner.getSelectedItemPosition());

		editor.apply();
		dismiss();
	}

	private void cancel()
	{
		dismiss();
	}

	private void restoreDefaultSettings()
	{

	}
}
