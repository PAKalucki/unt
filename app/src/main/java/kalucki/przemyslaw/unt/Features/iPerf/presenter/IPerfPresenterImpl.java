package kalucki.przemyslaw.unt.Features.iPerf.presenter;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import kalucki.przemyslaw.unt.Features.iPerf.ui.IPerfView;
import kalucki.przemyslaw.unt.Utils.SchedulerProvider;

/**
 * Created by prka on 08.11.17.
 */

public class IPerfPresenterImpl implements IPerfPresenter
{
	private WeakReference<IPerfView> view;
	@Inject SchedulerProvider scheduler;

	@Inject
	public IPerfPresenterImpl(SchedulerProvider scheduler)
	{
		this.scheduler = scheduler;
	}

	@Override
	public void bind(IPerfView view)
	{
		this.view = new WeakReference<>(view);
	}

	@Override
	public void unbind()
	{
		view.clear();
		view = null;
	}

	@Override
	public void start()
	{

	}

	@Override
	public void stop()
	{

	}
}
