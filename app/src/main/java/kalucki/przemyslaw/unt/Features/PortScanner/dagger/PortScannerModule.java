package kalucki.przemyslaw.unt.Features.PortScanner.dagger;

import dagger.Module;
import dagger.Provides;
import kalucki.przemyslaw.unt.Features.PortScanner.presenter.PortScannerPresenter;
import kalucki.przemyslaw.unt.Features.PortScanner.presenter.PortScannerPresenterImpl;

/**
 * Created by prka on 16.11.17.
 */

@Module
public class PortScannerModule
{
	@Provides
	@PortScan
	PortScannerPresenter providePortScannerPresenter(PortScannerPresenterImpl presenter)
	{
		return presenter;
	}
}
