package kalucki.przemyslaw.unt.Features.DNSLookUp;

import org.xbill.DNS.ExtendedResolver;
import org.xbill.DNS.Lookup;
import org.xbill.DNS.Record;

import io.reactivex.Observable;

/**
 * Created by prka on 08.11.17.
 */

public class DNSLookUp
{
	public Observable<String> getOutput(DNSQuery query)
	{
		return Observable.create(subscriber->
		                         {
			                         Lookup lookup = new Lookup(query.address, query.type, query.dclass);
			                         lookup.setResolver(new ExtendedResolver(query.serverNames));
			                         lookup.run();

			                         switch(lookup.getResult())
			                         {
				                         case Lookup.SUCCESSFUL:
					                         Record[] records = lookup.getAnswers();
					                         for(Record record : records)
					                         {
						                         subscriber.onNext(record.rdataToString());
					                         }
					                         break;
				                         case Lookup.TYPE_NOT_FOUND:
					                         subscriber.onNext("Type not found");
					                         break;
				                         case Lookup.HOST_NOT_FOUND:
					                         subscriber.onNext("Host not found");
					                         break;
				                         case Lookup.TRY_AGAIN:
					                         subscriber.onNext("Network Error try again");
					                         break;
				                         case Lookup.UNRECOVERABLE:
					                         subscriber.onNext("Unrecoverable");
					                         break;
				                         default:
					                         subscriber.onNext("Error");
					                         break;
			                         }

			                         subscriber.onComplete();
		                         });
	}
}
