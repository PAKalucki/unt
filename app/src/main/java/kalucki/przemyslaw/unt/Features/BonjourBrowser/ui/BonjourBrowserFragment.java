package kalucki.przemyslaw.unt.Features.BonjourBrowser.ui;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindArray;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import kalucki.przemyslaw.unt.Adapter.ListAdapter.ListAdapter;
import kalucki.przemyslaw.unt.Adapter.ListAdapter.ListItem;
import kalucki.przemyslaw.unt.Base.BaseFragment;
import kalucki.przemyslaw.unt.Features.BonjourBrowser.presenter.BonjourBrowserPresenter;
import kalucki.przemyslaw.unt.Features.BonjourBrowser.BonjourBrowserSettings;
import kalucki.przemyslaw.unt.MainApplication;
import kalucki.przemyslaw.unt.R;

public class BonjourBrowserFragment extends BaseFragment implements BonjourBrowserView
{
	@BindArray (R.array.bonjour_autocomplete) String[] suggestions;
	@BindString (R.string.bonjour_sort_key) String bonjourSortKey;
	@BindView (R.id.bonjour_autocomplete_textview) AutoCompleteTextView textView;
	@BindView (R.id.bonjour_list_recyclerview) RecyclerView recyclerView;
	@BindView(R.id.bonjour_button) Button button;
	@Inject BonjourBrowserPresenter presenter;
	@Inject SharedPreferences preferences;
	private ArrayList<ListItem> outputItems = new ArrayList<>();
	private boolean isStarted = false;
	private ListAdapter adapter;
	private Unbinder unbinder;

	public BonjourBrowserFragment()
	{
		// Required empty public constructor
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.fragment_bonjour_browser, container, false);
		unbinder = ButterKnife.bind(this, view);
		return view;
	}

	@Override
	public void onDestroyView()
	{
		super.onDestroyView();
		unbinder.unbind();
	}

	@Override
	public void onStart()
	{
		super.onStart();

		View view = getView();

		if(view != null)
		{
			presenter.bind(this);

			ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(view.getContext(), android.R.layout.simple_dropdown_item_1line, suggestions);
			textView.setAdapter(arrayAdapter);
			textView.setThreshold(0);

			recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
			adapter = new ListAdapter(outputItems);
			recyclerView.setAdapter(adapter);
		}
	}

	@Override
	public void onStop()
	{
		super.onStop();
		presenter.unbind();
	}

	@Override
	public void start()
	{
		if(adapter != null)
		{
			adapter.clearAll();
		}
		isStarted = true;
		button.setText("Stop");
	}

	@Override
	public void stop()
	{
		isStarted = false;
		button.setText("Scan");
		textView.setText("");
	}

	@Override
	public void update(String value)
	{
		outputItems.add(new ListItem(null, value));
		adapter.notifyDataSetChanged();
		recyclerView.scrollToPosition(outputItems.size() - 1);
	}

	@Override
	public BonjourBrowserSettings loadSettings()
	{
		BonjourBrowserSettings settings = new BonjourBrowserSettings();
		settings.sortMode = preferences.getInt(bonjourSortKey, 0);
		return settings;
	}

	@OnClick ({R.id.bonjour_button})
	public void onClick(View view)
	{
		if(isStarted)
		{
			presenter.stop();
		}
		else
		{
			if(!TextUtils.isEmpty(textView.getText()))
			{
				presenter.start(textView.getText().toString());
			}
		}
	}

	@Override
	protected void injectDependencies(MainApplication application)
	{
		application.getBonjourBrowserSubComponent().inject(this);
	}
}
