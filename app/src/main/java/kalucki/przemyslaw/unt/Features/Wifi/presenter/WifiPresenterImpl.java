package kalucki.przemyslaw.unt.Features.Wifi.presenter;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import kalucki.przemyslaw.unt.Features.Wifi.ui.WifiView;
import kalucki.przemyslaw.unt.Utils.SchedulerProvider;

/**
 * Created by prka on 08.11.17.
 */

public class WifiPresenterImpl implements WifiPresenter
{
	private WeakReference<WifiView> view;
	private SchedulerProvider scheduler;

	@Inject
	public WifiPresenterImpl(SchedulerProvider scheduler)
	{
		this.scheduler = scheduler;
	}

	@Override
	public void bind(WifiView view)
	{
		this.view = new WeakReference<>(view);
	}

	@Override
	public void unbind()
	{
		view.clear();
		view = null;
	}

	@Override
	public void start()
	{

	}

	@Override
	public void stop()
	{

	}
}
