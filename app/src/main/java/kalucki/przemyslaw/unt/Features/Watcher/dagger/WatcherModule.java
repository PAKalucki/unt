package kalucki.przemyslaw.unt.Features.Watcher.dagger;

import dagger.Module;
import dagger.Provides;
import kalucki.przemyslaw.unt.Features.Watcher.presenter.WatcherPresenter;
import kalucki.przemyslaw.unt.Features.Watcher.presenter.WatcherPresenterImpl;

/**
 * Created by prka on 16.11.17.
 */

@Module
public class WatcherModule
{
	@Provides
	@Watcher
	WatcherPresenter provideWatcherPresenter(WatcherPresenterImpl presenter)
	{
		return presenter;
	}
}
