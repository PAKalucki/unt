package kalucki.przemyslaw.unt.Features.Ping;

import android.os.SystemClock;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

import io.reactivex.Single;

/**
 * Created by prka on 03.10.17.
 */

public class TCPPing
{
	private boolean isActive = false;
	private PingSettings settings;
	private Socket socket;
	private String address;

	public void setSettings(PingSettings settings)
	{
		this.settings = settings;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public Single<TCPResult> ping()
	{
		isActive = true;
		return Single.create(subscriber->
		{
			try
			{
				long before = SystemClock.uptimeMillis();
				socket = new Socket();//TODO move this out ?
				socket.connect(new InetSocketAddress(address, settings.port), settings.timeout);
				socket.close();
				long after = SystemClock.uptimeMillis();

				subscriber.onSuccess(new TCPResult(true, after - before));
			}
			catch(Exception e)
			{
				subscriber.onSuccess(new TCPResult(false, 0L));//TODO use onError and handle errors ?
			}
		});
	}

	public void stop()
	{
		isActive = false;
		try
		{
			if(socket != null)
			{
				socket.close();
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}

	public boolean isActive()
	{
		return isActive;
	}
}
