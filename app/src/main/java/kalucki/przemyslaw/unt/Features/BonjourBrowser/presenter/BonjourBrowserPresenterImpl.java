package kalucki.przemyslaw.unt.Features.BonjourBrowser.presenter;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import de.mannodermaus.rxbonjour.IllegalBonjourTypeException;
import de.mannodermaus.rxbonjour.RxBonjour;
import io.reactivex.disposables.CompositeDisposable;
import kalucki.przemyslaw.unt.Features.BonjourBrowser.ui.BonjourBrowserView;
import kalucki.przemyslaw.unt.Utils.SchedulerProvider;

/**
 * Created by prka on 08.11.17.
 */

public class BonjourBrowserPresenterImpl implements BonjourBrowserPresenter
{
	@Inject RxBonjour rxBonjour;
	private CompositeDisposable compositeDisposable = new CompositeDisposable();
	private WeakReference<BonjourBrowserView> view;
	private SchedulerProvider scheduler;

	@Inject
	public BonjourBrowserPresenterImpl(SchedulerProvider scheduler)
	{
		this.scheduler = scheduler;
	}

	@Override
	public void bind(BonjourBrowserView view)
	{
		this.view = new WeakReference<>(view);
	}

	@Override
	public void unbind()
	{
		view.clear();
		view = null;
	}

	@Override
	public void start(String type)//TODO parsing and formatting for the output
	{
		view.get().start();

		compositeDisposable.add(
				rxBonjour.newDiscovery(type)
				         .subscribeOn(scheduler.backgroundScheduler())
				         .observeOn(scheduler.mainScheduler())
				         .subscribe(value->
				                    {
					                    if(view != null)
					                    {
						                    view.get().update(value.toString());
					                    }
				                    },
				                    error->
				                    {
					                    error.printStackTrace();
					                    if(error instanceof IllegalBonjourTypeException)
					                    {
						                    if(view != null)
						                    {
							                    view.get().update("Invalid type to scan");
							                    view.get().stop();
						                    }
					                    }
				                    },
				                    ()->
				                    {
					                    if(view != null)
					                    {
						                    view.get().stop();
					                    }
				                    }));
	}

	@Override
	public void stop()
	{
		view.get().stop();

		if(compositeDisposable != null && !compositeDisposable.isDisposed())
		{
			compositeDisposable.clear();
		}
	}

}
