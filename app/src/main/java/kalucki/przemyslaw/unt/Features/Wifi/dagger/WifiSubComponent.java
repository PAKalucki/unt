package kalucki.przemyslaw.unt.Features.Wifi.dagger;

import dagger.Subcomponent;
import kalucki.przemyslaw.unt.Features.Wifi.ui.WifiFragment;
import kalucki.przemyslaw.unt.Features.Wifi.ui.WifiSettingsDialog;

/**
 * Created by prka on 16.11.17.
 */

@Wifi
@Subcomponent(modules = WifiModule.class)
public interface WifiSubComponent
{
	void inject(WifiFragment wifiFragment);
	void inject(WifiSettingsDialog wifiSettingsDialog);
}
