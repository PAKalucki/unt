package kalucki.przemyslaw.unt.Injections;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import kalucki.przemyslaw.unt.Utils.SchedulerProvider;
import kalucki.przemyslaw.unt.Utils.SchedulerProviderImpl;

/**
 * Created by prka on 27.10.17.
 */

@Module
public class ApplicationModule
{
	private Application application;

	public ApplicationModule(Application application)
	{
		this.application = application;
	}

	@Provides
	@Singleton
	Context provideContext()
	{
		return application.getApplicationContext();
	}

	@Provides
	@Singleton
	SharedPreferences provideSharedPreferences()
	{
		return PreferenceManager.getDefaultSharedPreferences(application);
	}

	@Provides
	@Singleton
	SchedulerProvider provideScheduler()
	{
		return new SchedulerProviderImpl();
	}
}
