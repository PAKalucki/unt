package kalucki.przemyslaw.unt.Injections;

import javax.inject.Singleton;

import dagger.Component;
import kalucki.przemyslaw.unt.Features.BonjourBrowser.dagger.BonjourBrowserModule;
import kalucki.przemyslaw.unt.Features.BonjourBrowser.dagger.BonjourBrowserSubComponent;
import kalucki.przemyslaw.unt.Features.DNSLookUp.dagger.LookUpModule;
import kalucki.przemyslaw.unt.Features.DNSLookUp.dagger.LookUpSubComponent;
import kalucki.przemyslaw.unt.Features.Lan.dagger.LanModule;
import kalucki.przemyslaw.unt.Features.Lan.dagger.LanSubComponent;
import kalucki.przemyslaw.unt.Features.NetworkInfo.dagger.NetworkInfoModule;
import kalucki.przemyslaw.unt.Features.NetworkInfo.dagger.NetworkInfoSubComponent;
import kalucki.przemyslaw.unt.Features.Ping.dagger.PingModule;
import kalucki.przemyslaw.unt.Features.Ping.dagger.PingSubComponent;
import kalucki.przemyslaw.unt.Features.PortScanner.dagger.PortScannerModule;
import kalucki.przemyslaw.unt.Features.PortScanner.dagger.PortScannerSubComponent;
import kalucki.przemyslaw.unt.Features.TraceRoute.dagger.TraceRouteModule;
import kalucki.przemyslaw.unt.Features.TraceRoute.dagger.TraceRouteSubComponent;
import kalucki.przemyslaw.unt.Features.UPnPScan.dagger.UPnPScanModule;
import kalucki.przemyslaw.unt.Features.UPnPScan.dagger.UPnPScanSubComponent;
import kalucki.przemyslaw.unt.Features.UsageStats.dagger.UsageStatsModule;
import kalucki.przemyslaw.unt.Features.UsageStats.dagger.UsageStatsSubComponent;
import kalucki.przemyslaw.unt.Features.Watcher.dagger.WatcherModule;
import kalucki.przemyslaw.unt.Features.Watcher.dagger.WatcherSubComponent;
import kalucki.przemyslaw.unt.Features.WhoIs.dagger.WhoIsModule;
import kalucki.przemyslaw.unt.Features.WhoIs.dagger.WhoIsSubComponent;
import kalucki.przemyslaw.unt.Features.Wifi.dagger.WifiModule;
import kalucki.przemyslaw.unt.Features.Wifi.dagger.WifiSubComponent;
import kalucki.przemyslaw.unt.Features.iPerf.dagger.IPerfModule;
import kalucki.przemyslaw.unt.Features.iPerf.dagger.IPerfSubComponent;
import kalucki.przemyslaw.unt.MainActivity;

/**
 * Created by prka on 27.10.17.
 */

@Singleton
@Component (modules = ApplicationModule.class)
public interface ApplicationComponent
{
	void inject(MainActivity mainActivity);

	BonjourBrowserSubComponent plus(BonjourBrowserModule module);
	LookUpSubComponent plus(LookUpModule module);
	IPerfSubComponent plus(IPerfModule module);
	LanSubComponent plus(LanModule module);
	NetworkInfoSubComponent plus(NetworkInfoModule module);
	PingSubComponent plus(PingModule module);
	PortScannerSubComponent plus(PortScannerModule module);
	TraceRouteSubComponent plus(TraceRouteModule module);
	UPnPScanSubComponent plus(UPnPScanModule module);
	UsageStatsSubComponent plus(UsageStatsModule module);
	WatcherSubComponent plus(WatcherModule module);
	WhoIsSubComponent plus(WhoIsModule module);
	WifiSubComponent plus(WifiModule module);
}