package kalucki.przemyslaw.unt;

import android.annotation.TargetApi;
import android.app.AppOpsManager;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import butterknife.BindArray;
import butterknife.BindView;
import butterknife.ButterKnife;
import kalucki.przemyslaw.unt.Adapter.ScreenSlidePageAdapter;
import kalucki.przemyslaw.unt.Features.BonjourBrowser.ui.BonjourBrowserFragment;
import kalucki.przemyslaw.unt.Features.BonjourBrowser.ui.BonjourBrowserSettingsDialog;
import kalucki.przemyslaw.unt.Features.DNSLookUp.ui.LookUpFragment;
import kalucki.przemyslaw.unt.Features.DNSLookUp.ui.LookUpSettingsDialog;
import kalucki.przemyslaw.unt.Features.Lan.ui.LanFragment;
import kalucki.przemyslaw.unt.Features.Lan.ui.LanSettingsDialog;
import kalucki.przemyslaw.unt.Features.NetworkInfo.ui.NetworkInfoFragment;
import kalucki.przemyslaw.unt.Features.NetworkInfo.ui.NetworkInfoSettingsDialog;
import kalucki.przemyslaw.unt.Features.Ping.ui.PingFragment;
import kalucki.przemyslaw.unt.Features.Ping.ui.PingSettingsDialog;
import kalucki.przemyslaw.unt.Features.PortScanner.ui.PortScannerFragment;
import kalucki.przemyslaw.unt.Features.PortScanner.ui.PortScannerSettingsDialog;
import kalucki.przemyslaw.unt.Features.TraceRoute.ui.TraceRouteFragment;
import kalucki.przemyslaw.unt.Features.TraceRoute.ui.TraceRouteSettingsDialog;
import kalucki.przemyslaw.unt.Features.UPnPScan.ui.UPnPScanFragment;
import kalucki.przemyslaw.unt.Features.UPnPScan.ui.UPnPScanSettingsDialog;
import kalucki.przemyslaw.unt.Features.UsageStats.ui.UsageStatsFragment;
import kalucki.przemyslaw.unt.Features.UsageStats.ui.UsageStatsSettingsDialog;
import kalucki.przemyslaw.unt.Features.Watcher.ui.WatcherFragment;
import kalucki.przemyslaw.unt.Features.Watcher.ui.WatcherSettingsDialog;
import kalucki.przemyslaw.unt.Features.WhoIs.ui.WhoIsFragment;
import kalucki.przemyslaw.unt.Features.WhoIs.ui.WhoIsSettingsDialog;
import kalucki.przemyslaw.unt.Features.Wifi.ui.WifiFragment;
import kalucki.przemyslaw.unt.Features.Wifi.ui.WifiSettingsDialog;
import kalucki.przemyslaw.unt.Features.iPerf.ui.IPerfFragment;
import kalucki.przemyslaw.unt.Features.iPerf.ui.IPerfSettingsDialog;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener
{
	@BindArray (R.array.tabs) String[] tabNames;
	@BindView (R.id.adView) AdView adView;
	@BindView (R.id.drawer_layout) DrawerLayout drawer;
	@BindView (R.id.pager) ViewPager viewPager;
	@BindView (R.id.tabs) TabLayout tabLayout;
	@BindView (R.id.toolbar) Toolbar toolbar;
	@BindView (R.id.nav_view) NavigationView navigationView;
	private boolean doubleBackToExitPressedOnce = false;
	private ScreenSlidePageAdapter pagerAdapter;
	private static final int READ_PHONE_STATE_REQUEST = 37;
	private static final String ACTION_PING = "kalucki.przemyslaw.unt.PING";
	private static final String ACTION_TRACE = "kalucki.przemyslaw.unt.TRACE";
	private static final String ACTION_WHOIS = "kalucki.przemyslaw.unt.WHOIS";

	@Override
	protected void onStart()
	{
		super.onStart();
		requestPermissions();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		ButterKnife.bind(this);

		setSupportActionBar(toolbar);

		ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
		drawer.addDrawerListener(toggle);
		toggle.syncState();

		navigationView.setNavigationItemSelectedListener(this);

		pagerAdapter = new ScreenSlidePageAdapter(getSupportFragmentManager(), tabNames);
		prepareAdapter();
		viewPager.setAdapter(pagerAdapter);
		tabLayout.setupWithViewPager(viewPager);

		MobileAds.initialize(getApplicationContext(), "ca-app-pub-7733708261591143~6239889511");
		AdRequest adRequest = new AdRequest.Builder().build();
		adView.loadAd(adRequest);

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1)
		{
			handleShortcuts();
		}
	}

	@Override
	public void onBackPressed()
	{
		if(drawer.isDrawerOpen(GravityCompat.START))
		{
			drawer.closeDrawer(GravityCompat.START);
		}
		else
		{
			exitWithBackButton();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		int id = item.getItemId();

		if(id == R.id.action_settings)
		{
			showDialogs(viewPager.getCurrentItem());
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@SuppressWarnings ("StatementWithEmptyBody")
	@Override
	public boolean onNavigationItemSelected(MenuItem item)
	{
		int id = item.getItemId();

		switch(id)
		{
			case R.id.info:
				viewPager.setCurrentItem(0);
				break;
			case R.id.ping:
				viewPager.setCurrentItem(1);
				break;
			case R.id.traceroute:
				viewPager.setCurrentItem(2);
				break;
			case R.id.whois:
				viewPager.setCurrentItem(3);
				break;
			case R.id.lan:
				viewPager.setCurrentItem(4);
				break;
			case R.id.dnsLookUp:
				viewPager.setCurrentItem(5);
				break;
			case R.id.bonjour:
				viewPager.setCurrentItem(6);
				break;
			case R.id.wifi:
				viewPager.setCurrentItem(7);
				break;
			case R.id.iperf:
				viewPager.setCurrentItem(8);
				break;
			case R.id.watcher:
				viewPager.setCurrentItem(9);
				break;
			case R.id.upnp:
				viewPager.setCurrentItem(10);
				break;
			case R.id.portscan:
				viewPager.setCurrentItem(11);
				break;
			case R.id.stats:
				viewPager.setCurrentItem(12);
				break;
		}

		drawer.closeDrawer(GravityCompat.START);
		return true;
	}

	private void exitWithBackButton()
	{
		if(doubleBackToExitPressedOnce)
		{
			super.onBackPressed();
			return;
		}

		doubleBackToExitPressedOnce = true;
		Toast.makeText(this, "Press again to EXIT", Toast.LENGTH_SHORT).show();

		new Handler().postDelayed(()->doubleBackToExitPressedOnce = false, 2000);
	}

	private void showDialogs(int number)
	{
		FragmentManager manager = getFragmentManager();
		switch(number)
		{
			case 0:
				NetworkInfoSettingsDialog fragment0 = NetworkInfoSettingsDialog.newInstance();
				fragment0.show(manager, "dialogInfo");
				break;
			case 1:
				PingSettingsDialog fragment2 = PingSettingsDialog.newInstance();
				fragment2.show(manager, "dialogPing");
				break;
			case 2:
				TraceRouteSettingsDialog fragment3 = TraceRouteSettingsDialog.newInstance();
				fragment3.show(manager, "dialogTrace");
				break;
			case 3:
				WhoIsSettingsDialog fragment4 = WhoIsSettingsDialog.newInstance();
				fragment4.show(manager, "dialogWho");
				break;
			case 4:
				LanSettingsDialog fragment5 = LanSettingsDialog.newInstance();
				fragment5.show(manager, "dialogLan");
				break;
			case 5:
				LookUpSettingsDialog fragment6 = LookUpSettingsDialog.newInstance();
				fragment6.show(manager, "dialogLookUp");
				break;
			case 6:
				BonjourBrowserSettingsDialog fragment7 = BonjourBrowserSettingsDialog.newInstance();
				fragment7.show(manager, "dialogBonjour");
				break;
			case 7:
				WifiSettingsDialog fragment8 = WifiSettingsDialog.newInstance();
				fragment8.show(manager, "dialogWifi");
				break;
			case 8:
				IPerfSettingsDialog fragment9 = IPerfSettingsDialog.newInstance();
				fragment9.show(manager, "dialogIPerf");
				break;
			case 9:
				WatcherSettingsDialog fragment10 = WatcherSettingsDialog.newInstance();
				fragment10.show(manager, "dialogWatcher");
				break;
			case 10:
				UPnPScanSettingsDialog fragment = UPnPScanSettingsDialog.newInstance();
				fragment.show(manager, "dialogUPnP");
				break;
			case 11:
				PortScannerSettingsDialog fragment11 = PortScannerSettingsDialog.newInstance();
				fragment11.show(manager, "dialogPortScan");
				break;
			case 12:
				UsageStatsSettingsDialog fragment1 = UsageStatsSettingsDialog.newInstance();
				fragment1.show(manager, "dialogUsage");
				break;
		}
	}

	private void prepareAdapter()//kinda ugly but this is workaround for butterknife binding null to views on fragment recreation in pager
	{
		if(pagerAdapter != null)
		{
			pagerAdapter.addFragment(new NetworkInfoFragment());
			pagerAdapter.addFragment(new PingFragment());
			pagerAdapter.addFragment(new TraceRouteFragment());
			pagerAdapter.addFragment(new WhoIsFragment());
			pagerAdapter.addFragment(new LanFragment());
			pagerAdapter.addFragment(new LookUpFragment());
			pagerAdapter.addFragment(new BonjourBrowserFragment());
			pagerAdapter.addFragment(new WifiFragment());
			pagerAdapter.addFragment(new IPerfFragment());
			pagerAdapter.addFragment(new WatcherFragment());
			pagerAdapter.addFragment(new UPnPScanFragment());
			pagerAdapter.addFragment(new PortScannerFragment());
			pagerAdapter.addFragment(new UsageStatsFragment());
		}
	}

	private void requestPermissions()
	{
		if(!hasPermissionToReadNetworkHistory())
		{
			return;
		}
		if(!hasPermissionToReadPhoneStats())
		{
			requestPhoneStateStats();
		}
	}

	private boolean hasPermissionToReadPhoneStats()
	{
		if(ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_DENIED)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	private void requestPhoneStateStats()
	{
		ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_PHONE_STATE}, READ_PHONE_STATE_REQUEST);
	}

	private boolean hasPermissionToReadNetworkHistory()
	{
		if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
		{
			return true;
		}

		final AppOpsManager appOps = (AppOpsManager) getSystemService(Context.APP_OPS_SERVICE);
		int mode = appOps.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS, android.os.Process.myUid(), getPackageName());

		if(mode == AppOpsManager.MODE_ALLOWED)
		{
			return true;
		}
		appOps.startWatchingMode(AppOpsManager.OPSTR_GET_USAGE_STATS, getApplicationContext().getPackageName(),
		                         new AppOpsManager.OnOpChangedListener()
		                         {
			                         @Override
			                         @TargetApi (Build.VERSION_CODES.M)
			                         public void onOpChanged(String op, String packageName)
			                         {
				                         int mode = appOps.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS, android.os.Process.myUid(),
				                                                          getPackageName());

				                         if(mode != AppOpsManager.MODE_ALLOWED)
				                         {
					                         return;
				                         }

				                         appOps.stopWatchingMode(this);
				                         Intent intent = new Intent(MainActivity.this, MainActivity.class);

				                         if(getIntent().getExtras() != null)
				                         {
					                         intent.putExtras(getIntent().getExtras());
				                         }

				                         intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
				                         getApplicationContext().startActivity(intent);
			                         }
		                         });
		requestReadNetworkHistoryAccess();
		return false;
	}

	@TargetApi (Build.VERSION_CODES.M)
	private void requestReadNetworkHistoryAccess()
	{
		Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
		startActivity(intent);
	}

	private void handleShortcuts()
	{
		switch(getIntent().getAction())
		{
			case ACTION_PING:
				viewPager.setCurrentItem(2);
				break;
			case ACTION_TRACE:
				viewPager.setCurrentItem(3);
				break;
			case ACTION_WHOIS:
				viewPager.setCurrentItem(4);
				break;
		}
	}
}
