package kalucki.przemyslaw.unt;

import android.app.Application;
import android.content.Context;

import kalucki.przemyslaw.unt.Features.BonjourBrowser.dagger.BonjourBrowserModule;
import kalucki.przemyslaw.unt.Features.BonjourBrowser.dagger.BonjourBrowserSubComponent;
import kalucki.przemyslaw.unt.Features.DNSLookUp.dagger.LookUpModule;
import kalucki.przemyslaw.unt.Features.DNSLookUp.dagger.LookUpSubComponent;
import kalucki.przemyslaw.unt.Features.Lan.dagger.LanModule;
import kalucki.przemyslaw.unt.Features.Lan.dagger.LanSubComponent;
import kalucki.przemyslaw.unt.Features.NetworkInfo.dagger.NetworkInfoModule;
import kalucki.przemyslaw.unt.Features.NetworkInfo.dagger.NetworkInfoSubComponent;
import kalucki.przemyslaw.unt.Features.Ping.dagger.PingModule;
import kalucki.przemyslaw.unt.Features.Ping.dagger.PingSubComponent;
import kalucki.przemyslaw.unt.Features.PortScanner.dagger.PortScannerModule;
import kalucki.przemyslaw.unt.Features.PortScanner.dagger.PortScannerSubComponent;
import kalucki.przemyslaw.unt.Features.TraceRoute.dagger.TraceRouteModule;
import kalucki.przemyslaw.unt.Features.TraceRoute.dagger.TraceRouteSubComponent;
import kalucki.przemyslaw.unt.Features.UPnPScan.dagger.UPnPScanModule;
import kalucki.przemyslaw.unt.Features.UPnPScan.dagger.UPnPScanSubComponent;
import kalucki.przemyslaw.unt.Features.UsageStats.dagger.UsageStatsModule;
import kalucki.przemyslaw.unt.Features.UsageStats.dagger.UsageStatsSubComponent;
import kalucki.przemyslaw.unt.Features.Watcher.dagger.WatcherModule;
import kalucki.przemyslaw.unt.Features.Watcher.dagger.WatcherSubComponent;
import kalucki.przemyslaw.unt.Features.WhoIs.dagger.WhoIsModule;
import kalucki.przemyslaw.unt.Features.WhoIs.dagger.WhoIsSubComponent;
import kalucki.przemyslaw.unt.Features.Wifi.dagger.WifiModule;
import kalucki.przemyslaw.unt.Features.Wifi.dagger.WifiSubComponent;
import kalucki.przemyslaw.unt.Features.iPerf.dagger.IPerfModule;
import kalucki.przemyslaw.unt.Features.iPerf.dagger.IPerfSubComponent;
import kalucki.przemyslaw.unt.Injections.ApplicationComponent;
import kalucki.przemyslaw.unt.Injections.ApplicationModule;
import kalucki.przemyslaw.unt.Injections.DaggerApplicationComponent;

/**
 * Created by prka on 24.10.17.
 */

public class MainApplication extends Application
{
	private static ApplicationComponent applicationComponent;
	private BonjourBrowserSubComponent bonjourBrowserSubComponent;
	private IPerfSubComponent perfSubComponent;
	private LanSubComponent lanSubComponent;
	private LookUpSubComponent lookUpSubComponent;
	private NetworkInfoSubComponent networkInfoSubComponent;
	private PingSubComponent pingSubComponent;
	private PortScannerSubComponent portScannerSubComponent;
	private TraceRouteSubComponent traceRouteSubComponent;
	private UPnPScanSubComponent uPnPScanSubComponent;
	private UsageStatsSubComponent usageStatsSubComponent;
	private WatcherSubComponent watcherSubComponent;
	private WhoIsSubComponent whoIsSubComponent;
	private WifiSubComponent wifiSubComponent;

	public MainApplication()
	{
	}

	@Override
	public void onCreate()
	{
		super.onCreate();

		applicationComponent = DaggerApplicationComponent.builder().applicationModule(new ApplicationModule(this)).build();
	}

	public static ApplicationComponent getApplicationComponent()
	{
		return applicationComponent;
	}

	public static MainApplication getApplication(Context context)
	{
		return (MainApplication) context.getApplicationContext();
	}

	public BonjourBrowserSubComponent getBonjourBrowserSubComponent()
	{
		if(bonjourBrowserSubComponent == null)
		{
			bonjourBrowserSubComponent = applicationComponent.plus(new BonjourBrowserModule());
		}

		return bonjourBrowserSubComponent;
	}

	public LookUpSubComponent getLookUpSubComponent()
	{
		if(lookUpSubComponent == null)
		{
			lookUpSubComponent = applicationComponent.plus(new LookUpModule());
		}

		return lookUpSubComponent;
	}

	public IPerfSubComponent getPerfSubComponent()
	{
		if(perfSubComponent == null)
		{
			perfSubComponent = applicationComponent.plus(new IPerfModule());
		}

		return perfSubComponent;
	}

	public LanSubComponent getLanSubComponent()
	{
		if(lanSubComponent == null)
		{
			lanSubComponent = applicationComponent.plus(new LanModule());
		}

		return lanSubComponent;
	}

	public NetworkInfoSubComponent getNetworkInfoSubComponent()
	{
		if(networkInfoSubComponent == null)
		{
			networkInfoSubComponent = applicationComponent.plus(new NetworkInfoModule());
		}

		return networkInfoSubComponent;
	}

	public PingSubComponent getPingSubComponent()
	{
		if(pingSubComponent == null)
		{
			pingSubComponent = applicationComponent.plus(new PingModule());
		}

		return pingSubComponent;
	}

	public PortScannerSubComponent getPortScannerSubComponent()
	{
		if(portScannerSubComponent == null)
		{
			portScannerSubComponent = applicationComponent.plus(new PortScannerModule());
		}

		return portScannerSubComponent;
	}

	public TraceRouteSubComponent getTraceRouteSubComponent()
	{
		if(traceRouteSubComponent == null)
		{
			traceRouteSubComponent = applicationComponent.plus(new TraceRouteModule());
		}

		return traceRouteSubComponent;
	}

	public UPnPScanSubComponent getUPnPScanSubComponent()
	{
		if(uPnPScanSubComponent == null)
		{
			uPnPScanSubComponent = applicationComponent.plus(new UPnPScanModule());
		}

		return uPnPScanSubComponent;
	}

	public UsageStatsSubComponent getUsageStatsSubComponent()
	{
		if(usageStatsSubComponent == null)
		{
			usageStatsSubComponent = applicationComponent.plus(new UsageStatsModule());
		}

		return usageStatsSubComponent;
	}

	public WatcherSubComponent getWatcherSubComponent()
	{
		if(watcherSubComponent == null)
		{
			watcherSubComponent	= applicationComponent.plus(new WatcherModule());
		}

		return watcherSubComponent;
	}

	public WhoIsSubComponent getWhoIsSubComponent()
	{
		if(whoIsSubComponent == null)
		{
			whoIsSubComponent = applicationComponent.plus(new WhoIsModule());
		}

		return whoIsSubComponent;
	}

	public WifiSubComponent getWifiSubComponent()
	{
		if(wifiSubComponent == null)
		{
			wifiSubComponent = applicationComponent.plus(new WifiModule());
		}

		return wifiSubComponent;
	}
}
