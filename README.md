# UNT
Features:
-Wifi Network information (ip, mac address, wifi SSID, RSSI, mask, dns, gateway and so on)
-Mobile Network information
-Ping (ICMP ping, HTTP Ping, HTTPS Ping, TCP Ping)
-TraceRoute
-Lan scan
-Network usage statistics (general and per app/process)
-WhoIs

TODO:
-Wifi scan (include showing hidden ssid networks)
-watcher (service monitoring online/offline of host)
-iperf
-ssh/telnet
-firewall
-port scan
-UPnP scan
-Bonjour browser
-DNS lookup
-sniffer
-ipv6 support for everything

Research TODO:
http://robolectric.org/
https://developer.android.com/training/testing/espresso/index.html
http://site.mockito.org/
Android Test Orchestrator
https://github.com/googlesamples

Coding standard:
- code formatting as in settings.jar
Naming conventions:
- class, function, variable names camelCase
- shared preference keys in format: feature_what_key for example ping_interval_key
- generic string resources: default_string
- string resources specyfic for feature: ping_default_string

